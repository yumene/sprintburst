#include "inc_Model.hlsli"

// カメラ定数バッファ
#include "../inc_CameraCB.hlsli"


//===============================================
// スタティックメッシュ用頂点シェーダー
//===============================================
VS_OUT main(float4 pos : POSITION, // 頂点座標(ローカル)
			float3 tangent : TANGENT, // 接線
			float3 binormal : BINORMAL, // 従法線
			float3 normal : NORMAL, // 法線
			float2 uv : TEXCOORD0 // UV
)
{
	VS_OUT Out;
	

	//-------------------------------------
	// 座標変換
	//-------------------------------------
	Out.Pos = mul(pos, g_mW);       // ワールド変換
	Out.wPos = Out.Pos.xyz;     // ワールド座標を憶えておく
	Out.Pos = mul(Out.Pos, g_mV);   // ビュー変換
	Out.wvPos = Out.Pos.xyz;    // ビュー座標を憶えておく
	Out.Pos = mul(Out.Pos, g_mP);   // 射影変換

	//-------------------------------------
	// UVはそのまま入れる
	//-------------------------------------
	Out.UV = uv;

	//-------------------------------------
	// ３種の法線
	//-------------------------------------
	Out.wT = normalize(mul(tangent, (float3x3)g_mW));
	Out.wB = normalize(mul(binormal, (float3x3)g_mW));
	Out.wN = normalize(mul(normal, (float3x3)g_mW));
	
	// 加工なしの法線
	Out.DefN = normal;

	return Out;
}
