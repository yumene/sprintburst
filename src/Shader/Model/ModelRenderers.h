#ifndef __MODEL_RENDERERS_H__
#define __MODEL_RENDERERS_H__

#include "../ContactBufferStructs.h"

using namespace EzLib;

class ModelRenderers
{
public:
	ModelRenderers()
	{
	}
	~ModelRenderers()
	{
		Release();
	}

	#pragma region 初期化・解放

	// 初期化
	bool Init();

	// 解放
	void Release();

	#pragma endregion


public:
	sptr<StaticMeshRenderer> m_SMeshRenderer;
	sptr<SkinMeshRenderer> m_SkinMeshRenderer;
	sptr<InstancingStaticMeshRenderer> m_InstSMeshRenderer;
	sptr<DebugColliderMeshRenderer> m_DebugColliderMeshRenderer;
};


#endif