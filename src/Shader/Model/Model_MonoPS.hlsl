#include "inc_Model.hlsli"


//===============================================
// ピクセルシェーダ
//===============================================

//ピクセルシェーダ出力データ
struct PS_Out
{
    float4 Color : SV_Target0;
};

// g_Diffuseを単色で出力するピクセルシェーダ
PS_Out main(VS_OUT In)
{
    PS_Out Out;
    Out.Color = g_Diffuse;
    return Out;
}
