#include "inc_Model.hlsli"

// ライト定数バッファ
#include "../inc_LightCB.hlsli"

// カメラ定数バッファ
#include "../inc_CameraCB.hlsli"


//===============================================
// Blinn-Phoneでスペキュラの強度を求める
//  計算軽め、ちょっと正確ではない感じだがするがゲームでよく使用される方法
//引数>
//  [in] lightDir	… ライトの方向
//  [in] normal		… 法線ベクトル(正規化したものを渡すこと)
//  [in] vCam		… ピクセルの座標からカメラへの方向(正規化したものを渡すこと)
//戻り値>スペキュラの強さ
//===============================================
float CalcSpecular_BlinnPhone(float3 lightDir, float3 normal, float3 vCam)
{
    float3 vHalf = normalize(vCam - lightDir);
    float SpePow = dot(normal, vHalf); // カメラの角度差(-1〜1)
    SpePow = saturate(SpePow); // マイナスは打ち消す
    SpePow = pow(SpePow, g_SpePower + 1);

    return SpePow;
}

//===============================================
// Phoneでスペキュラの強度を求める
//  正確な反射方向を求める方法
//===============================================
float CalcSpecular_Phone(float3 lightDir, float3 normal, float3 vCam)
{
//    float3 vRef = normalize(reflect(lightDir, normal));
    float3 vRef = normalize(vCam + normal);
    float SpePow = dot(vCam, vRef); // カメラの角度差(-1〜1)
    SpePow = saturate(SpePow); // マイナスは打ち消す
    SpePow = pow(SpePow, g_SpePower + 1);

	return SpePow;
}

void CalcLighting(float3 wPos, float3 normal, float3 vCam, inout float3 LightCol, inout float3 LightSpe);


//ピクセルシェーダ出力データ
struct PS_OUT{
	float4 Color : SV_Target0;  // 通常色
};

//======================================================================
//
// ピクセルシェーダ
// [主な機能]
// ・法線マップ(視差マップ)    … 凸凹表現
// ・平行光源(最大3個)         … 太陽光的な、無限距離ライト
// ・ポイント光源(最大100個)   … 電球的な、有限距離の全方向ライト
// ・スポット光源(最大20個)    … 懐中電灯的な、有限距離の角度制限付きライト
// ・アルファテスト            … 一定以下の透明なピクセルは捨て、なかったことにする
// ・加算スフィアマッピング    … MMD風のつやつやメタリック表現
// ・キューブ環境マッピング    … 疑似的な映り込み表現
// ・距離フォグ                … 霧の効果
//
// ※別途、RenderTargetの1番目に、発光色を出力しています
//
//======================================================================
PS_OUT main(VS_OUT In)
{
	// このピクセルからカメラへの方向
	float3 vCam = normalize(g_CamPos - In.wPos);

	//============================================================
	// 法線マップ
	//============================================================
	// 法線マップ用変換行列作成
	row_major float3x3 mTBN = { 
		normalize( In.wT ),     // X軸
		normalize( In.wB ),     // Y軸
		normalize( In.wN )      // Z軸
	};

	// 視差マップ用
	row_major float3x3 mInvTBN = transpose(mTBN);	// 転置行列(ある条件下の行列だと、逆行列と同じ意味になる)
	float3 vCamLocal = mul(vCam, mInvTBN);
	In.UV += g_ParallaxVal* NormalTex.Sample(WrapSmp, In.UV).a* vCamLocal.xy; // 射影(2D)座標系で、視差ぶんUVをずらす。g_ParallaxValは調整用


	// 法線マップ取得
	float4 normal = NormalTex.Sample(WrapSmp, In.UV);
	normal.xyz = normal.xyz*2.0 - 1.0;                     // 0〜1から-1〜1へ変換

	float3 w_normal = normalize(mul(normal.xyz, mTBN));    // ローカルな法線を行列で変換(面の向きを考慮したことになる)

	//w_normal = normalize(In.wN);	// 法線マップ未使用版

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//
	//
	// 材質の色関係
	//
	//
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	float4 MateCol = 1;		// 自分(材質)の色(ライトは含まない)　DiffuseとTextureと環境マップなど

	//============================================================
	// 材質の色
	//============================================================
	float4 texCol = MeshTex.Sample(WrapSmp, In.UV);		// テクスチャ取得
	MateCol = g_Diffuse* texCol* g_MulColor;			// 材質色* テクスチャ色* 強制合成色   の合成

	//============================================================
	// アルファテスト
	//  一定値以下の透明なピクセルは捨てる(Zバッファにも書き込まれなくなる)
	//============================================================
	if (MateCol.a < 0.01)discard;


	//============================================================
	// MMD風 加算スフィアマップ,MMD用乗算スフィアマップ
	// PMXEditorだと、ビュー座標系での法線をそのままUVとしてつかってるっぽい
	//============================================================
	float2 uv = normalize(mul(In.DefN, (float3x3)g_mV)).xy;	// 法線をビュー座標系に変換
	uv = uv * 0.5 + 0.5;		// 3DからUVへ変換
	MateCol.rgb *= MulSphereTex.Sample(WrapSmp, uv).rgb;			// そこの色を乗算
	MateCol.rgb += MaterialSphereTex.Sample(WrapSmp, uv).rgb;		// そこの色を加算


	//============================================================
	// キューブ環境マップ(映り込み)
	//============================================================
	{
		// リフレクションマップ(映り込み率テクスチャ)から取得
		float4 SphParam = SphereParamTex.Sample(WrapSmp, In.UV);

		float3 vRef = reflect(-vCam, w_normal);         // カメラからピクセルを見たときの反射ベクトル算出(-1〜1)
		float4 envCol = CubeTex.Sample(ClampSmp, vRef);

		// 合成(ブレンド)
		MateCol.rgb = lerp(MateCol.rgb, envCol.rgb, SphParam.r);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//
	//
	// ライトの色関係(ライティング)
	//
	//
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// スペキュラマップのデータを取得
    float4 SpeMap = SpecularTex.Sample(WrapSmp, In.UV);

	float3 LightCol = 0; // ライトのみの総拡散色(Diffuse) ここに複数のライトの色が加算されていく(照らされていく)
    float3 LightSpe = 0; // ライトのみの総反射色(Speculer) ここに複数のライトのスペキュラが加算されていく(照らされていく)

	// ライト有効時
	if (g_LightEnable)
	{


		//-------------------------------------------
		// 平行光源・ポイント光源・スポット光源の計算を行い、
        // ピクセルに当たる光の色(拡散色と反射色)を加算していく
		//-------------------------------------------
		CalcLighting(In.wPos, w_normal, vCam, LightCol, LightSpe);

		//-------------------------------------------
		// (おまけ)カメラライトスペキュラ
		// (法線マップを目立たたせる工夫　必要なければ消しても良い)
		//-------------------------------------------
		{
            float SpePow = CalcSpecular_BlinnPhone(-vCam, w_normal, vCam);
			LightSpe += 0.5* SpeMap.r* SpePow;			// 
		}

		//-------------------------------------------
		// 環境光
		//-------------------------------------------
		LightCol.rgb += g_AmbientLight.rgb;

	}
	// ライト無効時
	else{
        LightCol = 1;
        LightSpe = 0;
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//============================================================
    //
    // マテリアル色とライト色を合成
    //
	//============================================================

    float4 FinalCol = 0;

	// ライト拡散色(Diffuse)を、材質(マテリアル)に影響させる
    FinalCol.rgb = MateCol.rgb* LightCol.rgb;
    FinalCol.a = MateCol.a; // アルファ値はそのまま使用

	// ライト反射色(Specular)を、材質(マテリアル)の「反射色」に影響させ 、加算
    FinalCol.rgb += LightSpe.rgb* g_Specular.rgb* SpeMap.r;

	//============================================================
	// 自己発光色
	//============================================================
	// エミッシブマップから取得
    float4 emiCol = EmissiveTex.Sample(WrapSmp, In.UV);

    FinalCol.rgb += (g_Emissive.rgb + emiCol.rgb)* 4;

	//============================================================
	// 距離フォグ　最終的な色に対して霧効果を付ける
	//============================================================
	if(g_DistanceFogEnable){
		// フォグ 1(近い)〜0(遠い)
        float f = saturate(1.0 / exp(In.wvPos.z* g_DistanceFogDensity));
        // 適用
        FinalCol.rgb = lerp(g_DistanceFogColor.rgb, FinalCol.rgb, f);
    }

	//============================================================
	// 出力色
	//============================================================
	PS_OUT Out;
	// 最終的な出力色
	Out.Color = FinalCol;

	return Out;
}


//===============================================
// ライティング計算を行う
// 平行光源、点光源、スポット光源の拡散色、反射色を求める
//引数>
//    [in]  wPos     … ピクセルの3D座標
//    [in]  normal   … 法線
//    [in]  vCam     … ピクセルからカメラへの方向
//    [out] LightCol … 最終的なライトの拡散色(Diffuse)が加算される
//    [out] LightSpe … 最終的なライトの反射色(Specular)が加算される
//===============================================
void CalcLighting(float3 wPos, float3 normal, float3 vCam, inout float3 LightCol, inout float3 LightSpe)
{
	const float PI = 3.141592653589f;

	//============================================================
	// 平行光源ライティング
	//============================================================
	// ライト数ぶん処理
	for (int di = 0; di < g_DL_Cnt; di++)
	{
		//============================================================
		// ランバート拡散照明
		//============================================================
		float LPow = dot(-g_DL[di].Dir, normal);
		//LPow = max(0, LPow);
		LPow = LPow * 0.5f + 0.5; // ハーフランバート
		LPow = LPow * LPow;
		
		//============================================================
		// トゥーン
		//  ライティング結果の光の強さを元にトゥーンテクスチャから、カスタマイズされた光の強さを取得
		//============================================================
		float4 hToonCol = ToonTex.Sample(ClampSmp, float2(LPow, 0.5)); // 横
		float4 vToonCol = ToonTex.Sample(ClampSmp, float2(0.5, LPow)); // 縦

		float4 ToonCol = min(hToonCol, vToonCol); //ToonTex.Sample(ClampSmp, float2(LPow, LPow));


		//============================================================
		// シャドウマップでの光の遮断判定
		//============================================================











		//============================================================
		// ライト色を加算する
		//============================================================
		LightCol.rgb += g_DL[di].Color.rgb* ToonCol.rgb;

		//============================================================
		// スペキュラ(フォン鏡面反射)
		//============================================================
		// スペキュラ計算
		float SpePow = CalcSpecular_BlinnPhone(g_DL[di].Dir, normal, vCam);
		// ライトスペキュラ色を加算
		LightSpe += g_DL[di].Color.rgb* SpePow;
	}

	//============================================================
	// 点光源処理(ポイントライト)
	//  ※このライトはトゥーンを使用しません
	//============================================================
	// ライト数ぶん処理
	for (int pi = 0; pi < g_PL_Cnt; pi++)
	{
		// 色
		// このピクセルと点光源の距離を調べる
		float3 vDir = g_PL[pi].Pos - wPos; // 点光源への方向
		float dist = length(vDir); // 距離

		// 範囲内
		if (dist < g_PL[pi].Radius)
		{
			// 法線とライトの方向から内積で光の強さを求める
			// 点光源への方向と法線との角度の差が明るさとなる(ランバートと同じ)
	//		vDir = normalize(vDir);     //  正規化
			vDir /= dist; //  正規化
			float PLPow = dot(normal, vDir); // 法線とライトの方向との角度 (-1〜1)
			PLPow = saturate(PLPow); // 0〜1にする

			// 半径範囲による減衰を行う  ライトの座標になればなるほど明るく、半径ぎりぎりは暗い
			float attePow = 1 - saturate(dist / g_PL[pi].Radius); // 1(近い=最大の明るさ)　〜　0(遠い=完全に暗い)
			attePow *= attePow; // 光の強さは、距離の二乗に反比例する
			PLPow *= attePow; // 減衰適用

			// ライト色を加算する
			LightCol.rgb += g_PL[pi].Color.rgb* PLPow;

			// スペキュラ
			{
                float SpePow = CalcSpecular_BlinnPhone(-vDir, normal, vCam);
				SpePow *= attePow; // 範囲減衰
				// ライトスペキュラ色を加算
				LightSpe += g_PL[pi].Color.rgb* SpePow;
			}
		}
	}

	//============================================================
	// スポットライト
	//  ※このライトはトゥーンを使用しません
	//============================================================
	for (int si = 0; si < g_SL_Cnt; si++)
	{
		// このピクセルと光源の方向・距離を調べる
		float3 vDir = g_SL[si].Pos - wPos; // 光源への方向
		float dist = length(vDir); // 距離
//		vDir = normalize(vDir); // 正規化した光源への方向
		vDir /= dist; // 正規化した光源への方向

		// スポット光源の向きの角度差を求める
		float dt = acos(saturate(dot(-vDir, g_SL[si].Dir)));

		float innerAng = (g_SL[si].MinAngle* 0.5); // 内側コーン(100%光る部分)の角度の半分
		float outerAng = (g_SL[si].MaxAngle* 0.5); // 外側コーン(減衰する部分)の角度の半分

		// コーン内かどうかの角度判定
		if (dt <= outerAng)
		{
			// 法線と光の方向から内積で光の強さを求める
			float SLPow = dot(normal, vDir);
			SLPow = max(0, SLPow);

			// 距離による減衰を行う
			float attePow = 1 - saturate(dist / g_SL[si].Range); // 1(近い=最大の明るさ)　〜　0(遠い=完全に暗い)
			attePow *= attePow; // 光の強さは、距離の二乗に反比例する
			SLPow *= attePow; // 減衰適用

			// 外側コーン角度による減衰
			SLPow *= saturate((outerAng - dt) / (outerAng - innerAng));

			// ライト色を加算する
			LightCol.rgb += g_SL[si].Color.rgb* SLPow;

			// スペキュラ
			{
				
                float SpePow = CalcSpecular_BlinnPhone(-vDir, normal, vCam);
				SpePow *= attePow; // 範囲減衰

				// ライトスペキュラ色を加算
				LightSpe += g_SL[si].Color.rgb* SpePow;

			}
		}

	}

}
