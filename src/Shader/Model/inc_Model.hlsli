//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// SampleShader モデル描画用共通データ
//　他のHLSLからincludeして使う
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


//===========================================================
// コンスタント(定数)バッファの作成
// 定数バッファとはグローバル定数をグループ化した物。VC側からデータを渡せる場所。
// Direct3D11ではグローバル定数は、この定数バッファにする必要がある。
//
// register(b0)のb0が定数バッファのグループ番号
// 最大14個(b0〜b13)作れる。
// VC側からはこの番号を使って定数バッファをセットする。
// 
// 今回の授業では、b9〜b13を固定の内容で使用しますので使用しないでください。
// b0〜b8は、好きなように使ってください。
//
// 定数バッファにはパッキング規則というものがあり、きちんと意識していないと
// VC側から正常に値を持っていけないので注意
// [定数変数のパッキング規則 (DirectX HLSL)]
// https://msdn.microsoft.com/ja-jp/library/bb509632(v=vs.85).aspx
//===========================================================

//===================================
// [定数バッファ]オブジェクト単位データ
// packoffsetとは … この定数バッファ内のデータ位置を自分で指定したい場合に使用する。省略した場合は自動で決定される。
//===================================
cbuffer cbPerObject : register(b1)
{
	row_major float4x4	g_mW;       	// ワールド行列

	// 
	float4  g_MulColor;                 // 無条件に合成する色 強制的に色を変更したい場合に使用

	// ライト
	int		g_LightEnable;		        // ライティング有効/無効フラグ

	// フォグ
	int		g_DistanceFogEnable;		// フォグ有効/無効フラグ
	float3	g_DistanceFogColor;		    // フォグ色
	float	g_DistanceFogDensity;		// フォグ減衰率
};

//===================================
// [定数バッファ]マテリアル単位データ
//===================================
cbuffer cbPerMaterial : register(b2)
{
	float4	g_Diffuse;			// 拡散色(基本色)
	float4	g_Specular;			// スペキュラ(反射)
	float	g_SpePower;			// スペキュラの鋭さ
	float4	g_Emissive;			// エミッシブ(自己発光)

	float	g_ParallaxVal;		// 視差度
};

//===================================
// [定数バッファ]オブジェクト単位データ(スキンメッシュ用)
//===================================
cbuffer cbBones : register(b3)
{

//	row_major float4x4 g_mBones[1024]; // ボーン用行列配列

	// ※ボーン数により、セットされる定数バッファのサイズが変わるので、上記だと警告が出てうざい。
	// 下記のようにするとDebug時の警告を消せる。
	// 参考:http://zerogram.info/?p=860
	row_major float4x4 g_mBones[2] : packoffset(c0); // ボーン用行列配列
	row_major float4x4 g_mBoneEnd : packoffset(c1023);
};

//====================================================
// テクスチャ
//  VC側からセットするときに指定する番号は、t0〜t127の位置に対応している
//  最大128(t0〜t127)
//====================================================
Texture2D MeshTex			: register(t0);		// メッシュの通常テクスチャ(ディフューズテクスチャ)
Texture2D EmissiveTex		: register(t1);		// 発光テクスチャ
Texture2D ToonTex			: register(t2);		// トゥーンテクスチャ(陰影用)
Texture2D MaterialSphereTex : register(t3);		// 材質用加算スフィアマップ(MMDっぽい表現をするため)
Texture2D SphereParamTex	: register(t4);		// 映り込み(スフィア環境)マップ
Texture2D SpecularTex		: register(t5);		// スペキュラマップ
Texture2D MulSphereTex		: register(t6);		// 材質用乗算スフィアマップ(MMDモデル用)
Texture2D NormalTex			: register(t10);	// 法線マップテクスチャ

TextureCube CubeTex         : register(t20);    // キューブ環境マップ


//====================================================
// サンプラ
//  VC側からセットするときに指定する番号は、s0〜s15の位置に対応している
//  最大16(s0〜s15)
//====================================================
SamplerState WrapSmp : register(s0);	// Wrap用のサンプラ
SamplerState ClampSmp : register(s1);	// Clamp用のサンプラ


//===============================================
//
// 頂点シェーダー
//
//===============================================

// 頂点シェーダ出力データ
struct VS_OUT
{
	float4 Pos		: SV_Position;		// 2D座標(射影座標系)
	float2 UV		: TEXCOORD0;		// UV
	float3 wT		: TEXCOORD1;		// 接線(Tangent)
	float3 wB		: TEXCOORD2;		// 従法線(BiNormal)
	float3 wN		: TEXCOORD3;		// 法線(Normal)
	float3 wPos		: TEXCOORD4;		// 3D座標(ワールド座標系)
	float3 wvPos	: TEXCOORD5;		// 3D座標(ビュー座標系)
	float3 DefN		: TEXCOORD6;		// 加工なし法線 (追加)
};
