#include "inc_Model.hlsli"

// カメラ定数バッファ
#include "../inc_CameraCB.hlsli"

//===============================================
// スキンメッシュ用頂点シェーダー
//===============================================
VS_OUT main(float4 pos : POSITION,
			float3 tangent : TANGENT,
			float3 binormal : BINORMAL,
			float3 normal : NORMAL,
			float2 uv : TEXCOORD0,
			float4 blendWeight : BLENDWEIGHT,	// ボーンウェイトx4
			uint4  blendIndex : BLENDINDICES		// ボーン番号x4
)
{
	//----------------------------------------------------------------------	
	// スキニング処理　最大4つのボーン行列をブレンドし、座標や法線などを変換する。
	//----------------------------------------------------------------------	
	row_major float4x4 mBones = 0; // 最終的なワールド行列
	[unroll]
	for (int i = 0; i<4; i++)
		mBones += g_mBones[blendIndex[i]]* blendWeight[i];

	// ボーン行列で変換(まだローカル座標系)
	pos = mul(pos, mBones);
	tangent = mul(tangent, (float3x3) mBones);
	binormal = mul(binormal, (float3x3) mBones);
	normal = mul(normal, (float3x3) mBones);
	//----------------------------------------------------------------------	

	VS_OUT Out;

	//-------------------------------------
	// 座標変換
	//-------------------------------------
	Out.Pos = mul(pos, g_mW);   // ワールド変換
	Out.wPos = Out.Pos.xyz;     // ワールド座標を憶えておく
	Out.Pos = mul(Out.Pos, g_mV);   // ビュー変換
	Out.wvPos = Out.Pos.xyz;    // ビュー座標を憶えておく
	Out.Pos = mul(Out.Pos, g_mP);   // 射影変換

	//-------------------------------------
	// UV
	//-------------------------------------
	Out.UV = uv;

	//-------------------------------------
	// ３種の法線
	//-------------------------------------
	Out.wT = normalize(mul(tangent, (float3x3)mBones));
	Out.wB = normalize(mul(binormal, (float3x3)mBones));
	Out.wN = normalize(mul(normal, (float3x3)mBones));
	
	// 加工なしの法線
	Out.DefN = normal;

	return Out;
}
