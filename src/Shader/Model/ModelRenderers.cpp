#include "MainFrame/ZMainFrame.h"

bool ModelRenderers::Init()
{
	Release();

	bool successed;

	m_SMeshRenderer = std::make_shared<StaticMeshRenderer>();
	m_SkinMeshRenderer = std::make_shared<SkinMeshRenderer>();
	m_InstSMeshRenderer = std::make_shared<InstancingStaticMeshRenderer>();
	m_DebugColliderMeshRenderer = std::make_shared<DebugColliderMeshRenderer>();
	successed = m_SMeshRenderer->Init("Shader/Model_VS.cso", "Shader/Model_PS.cso");
	successed &= m_SkinMeshRenderer->Init("Shader/Model_SkinVS.cso", "Shader/Model_PS.cso");
	successed &= m_InstSMeshRenderer->Init("Shader/Model_InstancingVS.cso", "Shader/Model_PS.cso");
	successed &= m_DebugColliderMeshRenderer->Init("Shader/Model_VS.cso", "Shader/Model_MonoPS.cso");

	return successed;
}

void ModelRenderers::Release()
{
	if(m_SMeshRenderer)
		m_SMeshRenderer->Release();
	if(m_SkinMeshRenderer)
		m_SkinMeshRenderer->Release();
	if(m_InstSMeshRenderer)
		m_InstSMeshRenderer->Release();
	if (m_DebugColliderMeshRenderer)
		m_DebugColliderMeshRenderer->Release();
}
