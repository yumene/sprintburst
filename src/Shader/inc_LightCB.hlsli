
#include "LightDefines.h"

//===================================
// ライトデータ
//===================================
cbuffer cbDirLight : register(b12)
{
	//------------------------------
	// 環境光
	//------------------------------
	float4 g_AmbientLight;


	//------------------------------
	// 平行光源データ 最大3個
	//------------------------------
	int g_DL_Cnt; // 平行光源数

	struct DLData {
		float4	Color;		// 色
		float3	Dir;		// 方向
            float gomi;
    };
	DLData g_DL[MAX_DIRLIGHT];


	//------------------------------
	// ポイントライトデータ 最大100個
	//------------------------------
	int g_PL_Cnt; // ポイントライト数

	struct PLData
	{
		float4 Color; // 色
		float3 Pos; // 座標
		float Radius; // 減衰値 大きい=減衰が強い
	};
	PLData g_PL[MAX_POINTLIGHT];

	//------------------------------
	// スポットライトデータ 最大20個
	//------------------------------
	int g_SL_Cnt; // スポットライト数

	struct SLData
	{
		float4 Color; // ライトの色
		float3 Pos; // ライトの位置
		float Range; // 照射範囲

		float3 Dir; // ライトの方向
		float MinAngle; // 中心〜minまでは、最大の強さになり、
		float MaxAngle; // min〜maxにかけて、強さが減っていく

		float3 gomi; // 構造体はできるだけ16の倍数で作成した方がバグが減るので、ゴミをねじ込む
	};

	SLData g_SL[MAX_SPOTLIGHT];
};
