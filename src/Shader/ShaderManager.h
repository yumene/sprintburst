#ifndef __SHADER_MANAGER_H__
#define __SHADER_MANAGER_H__

#include "LightManager.h"

#include "Model/ModelRenderers.h"
#include "Game/System/Renderer/LineRenderer/LineRenderer.h"

// シェーダー管理構造体
//　各シェーダ描画関係のもの管理をするクラス
// 
// 定数バッファ
//  cbuffer(b0〜b9)		… 自由
//
//  cbuffer(b10)		… (固定)シャドウマップで使用予定
//  cbuffer(b11)		… (固定)カメラデータで使用
//  cbuffer(b12)		… (固定)ライトデータで使用
//  cbuffer(b13)		… (固定)デバッグ用で使用予定
//  ※このあたりの仕様は自分でやりやすいようにカスタマイズするように
//
// サンプラステート
//  s0					… Linear補間のWrapサンプラ
//  s1					… Linear補間のClampサンプラ
//  s2					… Point補間のWrapサンプラ
//  s3					… Point補間のClampサンプラ
//  s4〜9				… 固定用の予約
//  s10					… ShadowMap用 Comparisonサンプラとして使用する予定
//
//  s11〜s15			… 自由
//  ※このあたりの仕様は自分でやりやすいようにカスタマイズするように
struct ShaderManager
{
public:
	// カメラ用定数バッファ
	struct cbCamera
	{
		ZMatrix	mV;		// ビュー行列
		ZMatrix	mP;		// 射影行列
		ZVec3 CamPos;	// カメラの座標
		float tmp;		// ※パッキング規則のため
	};

	// デバッグ用定数バッファ
	struct cbDebug
	{
	public:
		cbDebug()
		{
			showCascadeDist = 0;
		}

	public:
		int showCascadeDist;
		float tmp[3];

	};

public:
	ShaderManager() : m_Ls(20000)
	{
	}

	void Init()
	{
		// シェーダー初期化
		m_Ms.Init();
		m_Ls.Init("Shader/Line_VS.cso", "Shader/Line_PS.cso");

		// よく使うブレンドステートを作成
		// 半透明合成用ステート
		m_bsAlpha.Set_Alpha(-1);
		m_bsAlpha.Create();
		// 加算合成用ステート
		m_bsAdd.Set_Add(-1);
		m_bsAdd.Create();
		// アルファ値無効モード
		m_bsNoAlpha.Set_NoAlpha(-1, true);
		m_bsNoAlpha.Create();
		// 半透明合成用ステート(AlphaToCoverage付き)
		m_bsAlpha_AtoC.Set_Alpha(-1);
		m_bsAlpha_AtoC.Set_AlphaToCoverageEnable(true);
		m_bsAlpha_AtoC.Create();

		// よく使うサンプラ作成& セット
		m_smp0_Linear_Wrap.SetAll_Standard();
		m_smp0_Linear_Wrap.Set_Wrap();
		m_smp0_Linear_Wrap.Set_Filter_Anisotropy(4);	// 高品質な異方性フィルタリングを使用(動作を重視したい場合はコメントにしてください)
		m_smp0_Linear_Wrap.Create();
		m_smp0_Linear_Wrap.SetStateVS(0);
		m_smp0_Linear_Wrap.SetStatePS(0);

		m_smp1_Linear_Clamp.SetAll_Standard();
		m_smp1_Linear_Clamp.Set_Clamp();
		m_smp1_Linear_Clamp.Set_Filter_Anisotropy(4);	// 高品質な異方性フィルタリングを使用(動作を重視したい場合はコメントにしてください)
		m_smp1_Linear_Clamp.Create();
		m_smp1_Linear_Clamp.SetStateVS(1);
		m_smp1_Linear_Clamp.SetStatePS(1);

		m_smp2_Point_Wrap.SetAll_Standard();
		m_smp2_Point_Wrap.Set_Filter_Point();
		m_smp2_Point_Wrap.Set_Wrap();
		m_smp2_Point_Wrap.Create();
		m_smp2_Point_Wrap.SetStateVS(2);
		m_smp2_Point_Wrap.SetStatePS(2);

		m_smp3_Point_Clamp.SetAll_Standard();
		m_smp3_Point_Clamp.Set_Filter_Point();
		m_smp3_Point_Clamp.Set_Clamp();
		m_smp3_Point_Clamp.Create();
		m_smp3_Point_Clamp.SetStateVS(3);
		m_smp3_Point_Clamp.SetStatePS(3);

		// ライト管理クラス
		m_LightMgr.Init();

		// カメラ定数バッファ
		m_cb11_Camera.Create(11);
		m_cb11_Camera.SetVS();		// 頂点シェーダパイプラインへセット
		m_cb11_Camera.SetPS();		// ピクセルシェーダパイプラインへセット
		m_cb11_Camera.SetGS();		// ジオメトリシェーダパイプラインへセット

		// デバッグ用定数バッファ
		m_cb13_Debug.Create(13);
		m_cb13_Debug.SetVS();
		m_cb13_Debug.SetPS();
		m_cb13_Debug.SetGS();

		// キューブマップ
		m_texCube.LoadTexture("data/Texture/CubeMap.dds");
		m_texCube.SetTexturePS(20);
	}

	// シェーダ(定数バッファ)のカメラのデータを更新
	void UpdateCamera(const ZCamera* camera = &ZCamera::LastCam)
	{
		if (camera == nullptr)
			return;

		// カメラ情報
		m_cb11_Camera.m_Data.mV = camera->mView;				// ビュー行列
		m_cb11_Camera.m_Data.mP = camera->mProj;				// 射影行列
		m_cb11_Camera.m_Data.CamPos = camera->mCam.GetPos();	// カメラ座標
		
		m_cb11_Camera.WriteData();		// 定数バッファへ書き込む

		m_cb11_Camera.SetVS();		// 頂点シェーダパイプラインへセット
		m_cb11_Camera.SetPS();		// ピクセルシェーダパイプラインへセット
		m_cb11_Camera.SetGS();		// ジオメトリシェーダパイプラインへセット
	}

	void Release()
	{
		// シェーダクラス解放
		m_Ms.Release();
		m_Ls.Release();

		// ステート解放
		m_bsAdd.Release();
		m_bsAlpha.Release();

		// サンプラ解放
		m_smp0_Linear_Wrap.Release();
		m_smp1_Linear_Clamp.Release();
		m_smp2_Point_Wrap.Release();
		m_smp3_Point_Clamp.Release();
		
		// カメラ用定数バッファー解放
		m_cb11_Camera.Release();

		m_LightMgr.Release();
	}

public:
	// ライト
	LightManager m_LightMgr;
	
	// ステート
	ZBlendState m_bsAlpha;			// 半透明合成モード
	ZBlendState m_bsAdd;			// 加算合成モード
	ZBlendState m_bsNoAlpha;		// アルファ無効モード
	ZBlendState	 m_bsAlpha_AtoC;	// 半透明合成モード(AlphaToCoverage付き)

	// サンプラ
	ZSamplerState m_smp0_Linear_Wrap;	// s0 Linear補間& Wrapモード用サンプラ
	ZSamplerState m_smp1_Linear_Clamp;	// s1 Linear補間& Clampモード用サンプラ
	ZSamplerState m_smp2_Point_Wrap;	// s2 Point補間& Wrapモード用サンプラ
	ZSamplerState m_smp3_Point_Clamp;	// s3 Point補間& Clampモード用サンプラ
	
	// カメラ用定数バッファ
	ZConstantBuffer<cbCamera> m_cb11_Camera;

	// デバッグ用定数バッファ
	ZConstantBuffer<cbDebug> m_cb13_Debug;

	// シェーダクラス
	ModelRenderers m_Ms;	// モデルレンダラー
	LineRenderer m_Ls;		// ラインレンダラー



	// その他
	ZTexture m_texCube;	// キューブマップテクスチャ(環境マッピング用)


};


#endif