#include "MainFrame/ZMainFrame.h"
#include "LightManager.h"

void LightManager::Init()
{
	// シェーダ用の定数バッファ作成
	m_cb12_Light.Create(12);
}

void LightManager::Release()
{
	m_cb12_Light.Release();
}

void LightManager::Update()
{
	//-----------------------------------
	// 環境光
	//-----------------------------------
	m_cb12_Light.m_Data.AmbientLight.Set(m_AmbientLight, 0);

	//-----------------------------------
	// 平行光源
	//-----------------------------------
	//  無効になってるライトは削除する
	{
		auto it = m_DirLightList.begin();
		while (it != m_DirLightList.end())
		{
			if ((*it).expired())
				it = m_DirLightList.erase(it);
			else
				++it;
		}
	}

	// 定数バッファに書き込み
	m_cb12_Light.m_Data.DL_Cnt = 0;
	for (auto& light : ShMgr.m_LightMgr.m_DirLightList)
	{
		int idx = m_cb12_Light.m_Data.DL_Cnt;
		if (idx >= ShMgr.m_LightMgr.MAX_DIRLIGHT)
			break;	// 限度数以上なら終了

		m_cb12_Light.m_Data.DL[idx].Color = light.lock()->Diffuse* light.lock()->Intensity;
		m_cb12_Light.m_Data.DL[idx].Dir = light.lock()->Direction;

		m_cb12_Light.m_Data.DL_Cnt++;

	}

	// ポイントライト
	//  無効になってるライトは削除する
	{
		auto it = m_PointLightList.begin();
		while (it != m_PointLightList.end())
		{
			if ((*it).expired())
				it = m_PointLightList.erase(it);
			else
				++it;
		}
	}

	// 定数バッファに書き込み
	m_cb12_Light.m_Data.PL_Cnt = 0;
	for (auto& light : ShMgr.m_LightMgr.m_PointLightList)
	{
		int idx = m_cb12_Light.m_Data.PL_Cnt;
		if (idx >= ShMgr.m_LightMgr.MAX_POINTLIGHT)
			break;	// 限度数以上なら終了

		m_cb12_Light.m_Data.PL[idx].Color = light.lock()->Diffuse* light.lock()->Intensity;
		m_cb12_Light.m_Data.PL[idx].Pos = light.lock()->Position;
		m_cb12_Light.m_Data.PL[idx].Radius = light.lock()->Radius;

		m_cb12_Light.m_Data.PL_Cnt++;

	}

	// スポットライト
	//  無効になってるライトは削除する
	{
		auto it = m_SpotLightList.begin();
		while (it != m_SpotLightList.end())
		{
			if ((*it).expired())
				it = m_SpotLightList.erase(it);
			else
				++it;
		}
	}

	// 定数バッファに書き込み
	m_cb12_Light.m_Data.SL_Cnt = 0;
	for (auto& light : ShMgr.m_LightMgr.m_SpotLightList)
	{
		int idx = m_cb12_Light.m_Data.SL_Cnt;
		
		// 限度数以上なら終了
		if (idx >= ShMgr.m_LightMgr.MAX_POINTLIGHT)
			break;

		m_cb12_Light.m_Data.SL[idx].Color = light.lock()->Diffuse* light.lock()->Intensity;
		m_cb12_Light.m_Data.SL[idx].Pos = light.lock()->Position;
		m_cb12_Light.m_Data.SL[idx].Range = light.lock()->Range;
		m_cb12_Light.m_Data.SL[idx].Dir = light.lock()->Direction;
		m_cb12_Light.m_Data.SL[idx].Dir.Normalize();
		m_cb12_Light.m_Data.SL[idx].MinAng = ToRadian(light.lock()->MinAngle);
		m_cb12_Light.m_Data.SL[idx].MaxAng = ToRadian(light.lock()->MaxAngle);

		m_cb12_Light.m_Data.SL_Cnt++;
	}

	// 実際に定数バッファへ書き込み
	m_cb12_Light.WriteData();

	m_cb12_Light.SetVS();		// 頂点シェーダパイプラインへセット
	m_cb12_Light.SetPS();		// ピクセルシェーダパイプラインへセット
	m_cb12_Light.SetGS();		// ジオメトリシェーダパイプラインへセット

}
