#include "inc_Effect.hlsli"

//ピクセルシェーダ出力データ
struct PS_OUT {
	float4 Color : SV_Target0;
};

// ピクセルシェーダ
PS_OUT main(VS_OUT In)
{
	// 色
	float4 Col = InputTex.Sample(ClampSmp, In.UV) * g_Color * In.Color;

	// エミッシブマップから取得
	float4 emiCol = EmissiveTex.Sample(ClampSmp, In.UV);

	// 
	PS_OUT Out;
	Out.Color = Col;
	Out.Color.rgb += emiCol.rgb * 10;
	return Out;
}
