//===================================
// カメラデータ
//===================================
cbuffer cbCamera : register(b11)
{
    row_major float4x4 g_mV; // ビュー行列
    row_major float4x4 g_mP; // 射影行列

	// カメラ
    float3 g_CamPos; // カメラ座標

};
