#pragma region ColliderBase

inline int ColliderBase::GetShape()const
{
	return m_Shape;
}

inline void ColliderBase::AddIgnoreID(size_t id)
{
	m_IgnoreIDs.push_back(id);
}

template<typename T>
inline T* ColliderBase::GetUserMap(const std::string& name)const
{
	auto it = m_UserMap.find(name);
	if (it == m_UserMap.end())
		return nullptr;
	return (T*)((*it).second);
}

template<typename T>
inline T* ColliderBase::Cast()const
{
	if (T::sShape() == m_Shape)
		return static_cast<T*>(this);
	return nullptr;
}

#pragma endregion

#pragma region Collider_Sphere

inline void Collider_Sphere::Set(const ZVec3& pos, float rad)
{
	m_Sphere.Center = pos;
	m_Sphere.Radius = rad;
}

inline void Collider_Sphere::SetPos(const ZVec3& pos)
{
	m_Sphere.Center = pos;
}

inline void Collider_Sphere::SetRadius(float rad)
{
	m_Sphere.Radius = rad;
}

#pragma endregion

#pragma region Collider_Box

inline const DirectX::BoundingOrientedBox& Collider_Box::GetBox()const
{
	return m_Box;
}

inline const ZVec3& Collider_Box::GetDir(int index)const
{
	return m_Dir[index];
}

inline void Collider_Box::Set(const ZVec3& vLocalCenterPos, const ZVec3& vHalfSize, const ZMatrix& mat)
{
	// 行列拡大成分取得
	ZVec3 scale = mat.GetScale();

	// ローカル中心座標を行列でワールド変換
	ZVec3 vCenter;
	vLocalCenterPos.Transform(vCenter, mat);

	// 回転をクォータニオンに変換
	ZQuat rotate;
	mat.ToQuaternion(rotate, true);

	m_Box.Center = vCenter;
	m_Box.Extents = vHalfSize * scale;
	m_Box.Orientation = rotate;
	m_Dir[0] = mat.GetXAxis().Normalized();
	m_Dir[1] = mat.GetYAxis().Normalized();
	m_Dir[2] = mat.GetZAxis().Normalized();
}

inline void Collider_Box::Set_MinMax(const ZVec3& vlocalMin, const ZVec3& vlocalMax, const ZMatrix& mat)
{
	ZQuat rotate;
	ZVec3 scale = mat.GetScale();
	ZVec3 vExtents = (vlocalMax - vlocalMin) / 2;
	ZVec3 vCenter = vlocalMin + vExtents;
	vCenter.Transform(mat);
	vExtents *= scale;

	mat.ToQuaternion(rotate, true);
	m_Box.Center = vCenter;
	m_Box.Extents = vExtents;
	m_Box.Orientation = rotate;

	m_Dir[0] = mat.GetXAxis().Normalized();
	m_Dir[1] = mat.GetYAxis().Normalized();
	m_Dir[2] = mat.GetZAxis().Normalized();
}

#pragma endregion

#pragma region Collider_Ray

inline void Collider_Ray::Set(const ZVec3& pos1, const ZVec3& pos2)
{
	m_vPos1 = pos1;
	m_vPos2 = pos2;
	m_vDir = pos2 - pos1;
	m_RayLen = m_vDir.Length();
	m_vDir.Normalize();
}

inline const ZVec3& Collider_Ray::GetPos1()const
{
	return m_vPos1;
}

inline const ZVec3& Collider_Ray::GetPos2()const
{
	return m_vPos2;
}

inline const ZVec3& Collider_Ray::GetDir()const
{
	return m_vDir;
}

inline float Collider_Ray::GetRayLen()const
{
	return m_RayLen;
}

#pragma endregion

#pragma region Collider_Mesh

inline void Collider_Mesh::Set(const ZMatrix& mat)
{
	m_Mat = mat;
}

inline void Collider_Mesh::AddMesh(sptr<ZGameModel> pModel)
{
	if (pModel == nullptr)
		return;
	for (auto& model : pModel->GetModelTbl())
		m_MeshTbl.push_back(model->GetMesh());
}

inline void Collider_Mesh::ClearMesh()
{
	m_MeshTbl.clear();
}

#pragma endregion

#pragma region Collider_Compound

inline void Collider_Compound::AddHitObj(ColliderBase* hitObj)
{
	m_Colliders.push_back(hitObj);
}

inline void Collider_Compound::ClearHitObj()
{
	m_Colliders.clear();
}

#pragma endregion

#pragma CollisionEngine

inline void CollisionEngine::AddAtk(ColliderBase* obj, size_t lineNo)
{
	if (obj == nullptr)
		return;
	if (lineNo >= NumAtkLines)
		lineNo = NumAtkLines - 1;

	m_AtkLists[lineNo].push_back(obj);
}

inline void CollisionEngine::AddDef(ColliderBase* obj)
{
	m_DefList.push_back(obj);
}

inline void CollisionEngine::ClearList()
{
	for (auto& node : m_AtkLists)
		node.clear();
	m_DefList.clear();
}

inline void CollisionEngine::EnableMultiThread(bool flg)
{
	m_IsMultiThread = flg;
}

#pragma endregion