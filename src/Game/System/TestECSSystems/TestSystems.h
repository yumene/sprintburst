#ifndef __TEST_SYSTEMS_H__
#define __TEST_SYSTEMS_H__

class MoveUpdateSystem : public ECSSystemBase
{
public:
	MoveUpdateSystem();
	 
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

private:
	void verlet(ZVec3& pos, ZVec3& velocity, const ZVec3& acceleration, float delta);

	void forestRuth(ZVec3& pos, ZVec3& velocity, const ZVec3& acceleration, float delta);

};

class AnimationUpdateSystem : public ECSSystemBase
{
public:
	AnimationUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;
};

class StaticMeshDrawSystem : public ECSSystemBase
{
public:
	StaticMeshDrawSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

private:
	bool m_IsInstancingDraw;

};

class SkinMeshDrawSystem : public ECSSystemBase
{
public:
	SkinMeshDrawSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;
};

class BoxSpawnerSystem : public ECSSystemBase
{
public:
	BoxSpawnerSystem(std::vector<sptr<ECSEntity>>& entityList);

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

private:
	std::vector<sptr<ECSEntity>>* m_pEntityList;
	sptr<ZGameModel> m_BoxModel;
	size_t m_NumSpawnEnitites;

};

class PhysicsSystem : public ECSSystemBase
{
public:
	PhysicsSystem(sptr<ZPhysicsWorld>& world) : m_pPhysicsWorld(world)
	{
	}

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

private:
	sptr<ZPhysicsWorld> m_pPhysicsWorld;
};

class CharaDebugSystem : public ECSSystemBase
{
public:
	CharaDebugSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

};

//VRコントローラを出すためだけのSystem
class VRControllSystem : public ECSSystemBase
{
public:
	VRControllSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

#endif