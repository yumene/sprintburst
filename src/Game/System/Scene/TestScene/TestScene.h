#ifndef __TITLE_SCENE_H__
#define __TITLE_SCENE_H__

#include "Game/GameCamera.h"

class TestScene : public SceneBase
{
public:
	virtual ~TestScene()
	{
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();

public:
	// その他
	std::vector<sptr<ECSEntity>> m_Entities; // エンティティハンドル

	// 平行光源
	sptr<DirLight> m_DirLight;

	// カメラ
	GameCamera m_Cam;

	// テクスチャ
	sptr<ZTexture> m_texBack;
	
	sptr<ZPhysicsWorld> m_PhysicsWorld;

};

#endif