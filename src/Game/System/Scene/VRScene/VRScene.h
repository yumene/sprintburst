#ifndef __VR_SCENE_H__
#define __VR_SCENE_H__

#include "Game/GameCamera.h"
#include "Game/VR/VR.h"

class VRScene : public SceneBase
{
public:
	virtual ~VRScene()
	{
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();

public:
	// その他
	std::vector<sptr<ECSEntity>> m_Entities; // エンティティハンドル

	// 平行光源
	sptr<DirLight> m_DirLight;

	// カメラ
	GameCamera m_Cam;
	// VR用カメラ
	CVRCamera m_VRCam;
	// テクスチャ
	sptr<ZTexture> m_texBack;

	sptr<ZPhysicsWorld> m_PhysicsWorld;
};

#endif#pragma once
