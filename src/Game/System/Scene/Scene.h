#ifndef __SCENE_H__
#define __SCENE_H__

// シーン基本クラス
class SceneBase
{
public:
	SceneBase()
	{
	}

	virtual void Init() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;

public:
	// 更新システム
	ECSSystemList m_UpdateSystems;
	// 描画システム
	ECSSystemList m_DrawSystems;

};

// シーンファクトリ
class SceneManager
{
public:
	// 初期化
	void Init();

	// 更新
	void Update();

	// 描画
	void Draw();

	// 解放
	void Release()
	{
		m_NowScene = nullptr;
		m_NextChangeSceneName = "";
		m_SceneGenerateMap.clear();
	}

	// シーンを変更
	//  sceneName	... あらかじめInit()で登録しておいたシーン名
	//  ※即座には変更されず次のUpdateの前に変更が実行される
	void ChangeScene(const std::string& sceneName)
	{
		m_NextChangeSceneName = sceneName;
	}

	// 現在のシーンを取得
	sptr<SceneBase> GetNowScene()
	{
		return m_NowScene;
	}

private:
	// 現在のシーン
	sptr<SceneBase> m_NowScene;

	// シーン生成マップ
	std::map<std::string, std::function<sptr<SceneBase>()>>	m_SceneGenerateMap;

	std::string m_NextChangeSceneName;

};


#endif