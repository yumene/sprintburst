#include "MainFrame/ZMainFrame.h"
#include "Scene.h"

// シーンクラスヘッダ
#include "./TestScene/TestScene.h"
#include "./VRScene/VRScene.h"

void SceneManager::Init()
{
	// シーン登録
	m_SceneGenerateMap["Test"] =
		[]()
	{
		return std::make_shared<TestScene>();
	};

	m_SceneGenerateMap["VR"] =
		[]()
	{
		return std::make_shared<VRScene>();
	};

	// 初期シーン作成
	m_NowScene = m_SceneGenerateMap["Test"]();

	// 初期シーン初期化
	m_NowScene->Init();

}

void SceneManager::Update()
{
	if (m_NextChangeSceneName.size() > 0)
	{
		m_NowScene = nullptr;

		// 存在するシーン名か
		if (m_SceneGenerateMap.end() == m_SceneGenerateMap.find(m_NextChangeSceneName))
			DW_SCROLL(0, "存在シーンです : %s\n", m_NextChangeSceneName.c_str());
		else
		{
			// 存在するならシーン作成
			m_NowScene = m_SceneGenerateMap[m_NextChangeSceneName]();
			// 初期化
			m_NowScene->Init();
		}

		// シーン切り替え用文字列クリア
		m_NextChangeSceneName = "";
	}

	if (m_NowScene)
		m_NowScene->Update();
}

void SceneManager::Draw()
{
	if (m_NowScene)
		m_NowScene->Draw();
}
