#include "MainFrame/ZMainFrame.h"
#include "SkinMeshRenderer.h"

bool SkinMeshRenderer::Init(const std::string& vsPath, const std::string& psPath)
{
	Release();

	bool successed;

	// 頂点シェーダ作成
	successed = m_VS.LoadShader(vsPath, ZVertex_Pos_UV_TBN_Skin::GetVertexTypeData());
	// ピクセルシェーダ作成
	successed &= m_PS.LoadShader(psPath);

	// コンスタントバッファー作成
	m_cb1_PerObject.Create(1);
	m_cb2_PerMaterial.Create(2);
	
	return successed;
}

void SkinMeshRenderer::Release()
{
	m_VS.Release();
	m_PS.Release();

	m_cb1_PerObject.Release();
	m_cb2_PerMaterial.Release();
}

void SkinMeshRenderer::Submit(ZMatrix* mat, ZGameModel* model, ZBoneController* bc)
{
	m_RenderBuffer[std::make_tuple(model, bc)].push_back(mat);
}

void SkinMeshRenderer::Flash()
{
	m_VS.SetShader();
	m_PS.SetShader();

	SetContactBuffers();

	for (auto& param : m_RenderBuffer)
	{
		ZGameModel* gModel = std::get<0>(param.first);
		ZBoneController* bc = std::get<1>(param.first);
		if (bc == nullptr)
			continue;

		// ボーン行列セット
		bc->GetBoneConstantBuffer().SetVS(3);

		for (sptr<ZSingleModel> pModel : gModel->GetModelTbl_Skin())
		{
			if (pModel == nullptr)
				continue;
			
			// メッシュ情報セット(頂点バッファ,インデックスバッファ,プリミティブ・トポロジーなどデバイスへセット)
			pModel->GetMesh()->SetDrawData();
			
			auto& materials = pModel->GetMaterials();
			for (UINT i = 0; i < materials.size(); i++)
			{
				if (pModel->GetMesh()->GetSubset(i)->FaceCount == 0)
					continue;
			
				ZMaterial& mate = materials[i];
				SetMaterial(mate);

				m_cb2_PerMaterial.WriteData();

				SetTextures(mate);

				for (auto mat : param.second)
				{
					if (mat == nullptr)
						continue;
				
					// 行列セット
					SetMatrix(*mat);
					m_cb1_PerObject.WriteData();
					pModel->GetMesh()->DrawSubset(i);
					
				}
			}
			
		}

	}

	m_RenderBuffer.clear();

}

void SkinMeshRenderer::SetContactBuffers()
{
	// オブジェクト単位のデータ
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();
	
	// マテリアル単位のデータ
	m_cb2_PerMaterial.SetPS();
}

void SkinMeshRenderer::SetMaterial(ZMaterial& mate)
{
	// マテリアル,テクスチャセット
	// 拡散色(Diffuse)
	m_cb2_PerMaterial.m_Data.Diffuse = mate.Diffuse;
	// 反射色(Specular)
	m_cb2_PerMaterial.m_Data.Specular = mate.Specular;
	// 反射の強さ(Power)
	m_cb2_PerMaterial.m_Data.SpePower = mate.Power;
	// 発光色(Emissive)
	m_cb2_PerMaterial.m_Data.Emissive = mate.Emissive;
}

void SkinMeshRenderer::SetTextures(ZMaterial& mate)
{
	// テクスチャをセット
	if (mate.TexSet->GetTex(0))
		mate.TexSet->GetTex(0)->SetTexturePS(0);
	else
		ZDx.GetWhiteTex()->SetTexturePS(0);
	// エミッシブマップ
	if (mate.TexSet->GetTex(1))
		mate.TexSet->GetTex(1)->SetTexturePS(1);
	else
		ZDx.RemoveTexturePS(1);
	// トゥーンテクスチャ
	if (mate.TexSet->GetTex(2))
		mate.TexSet->GetTex(2)->SetTexturePS(2);
	else
		ZDx.GetToonTex()->SetTexturePS(2);
	// 材質加算スフィアマップ
	if (mate.TexSet->GetTex(3))
		mate.TexSet->GetTex(3)->SetTexturePS(3);
	else
		ZDx.RemoveTexturePS(3);
	// リフレクションマップ
	if (mate.TexSet->GetTex(4))
		mate.TexSet->GetTex(4)->SetTexturePS(4);
	else
		ZDx.RemoveTexturePS(4);
	// スペキュラマップ
	if (mate.TexSet->GetTex(5))
		mate.TexSet->GetTex(5)->SetTexturePS(5);
	else
		ZDx.GetWhiteTex()->SetTexturePS(5);
	// 材質乗算スフィアマップ(MMD用に拡張)
	if (mate.TexSet->GetTex(6))
		mate.TexSet->GetTex(6)->SetTexturePS(6);
	else
		ZDx.GetWhiteTex()->SetTexturePS(6);
	// 法線マップ
	if (mate.TexSet->GetTex(9))
		mate.TexSet->GetTex(9)->SetTexturePS(10);
	else
		ZDx.GetNormalTex()->SetTexturePS(10);
}
