#ifndef __SKIN_MESH_RENDERER_H__
#define __SKIN_MESH_RENDERER_H__

#include "ModelRendererBase.h"

class SkinMeshRenderer : public ModelRendererBase<ZGameModel,ZBoneController>
{
public:
	~SkinMeshRenderer()
	{
		Release();
	}

	// 初期化
	virtual bool Init(const std::string& vsPath, const std::string& psPath) override;
	// 解放
	virtual void Release() override;
	
	// 描画(m_RenderBufferに追加)
	virtual void Submit(ZMatrix* mat, ZGameModel* model,ZBoneController* bc) override;
	// 本描画(m_RenderBufferに溜め込まれた描画情報を元に描画)
	virtual void Flash() override;


private:
	// コンタクトバッファをシェーダーにセット
	virtual void SetContactBuffers() override;


	void SetMatrix(const ZMatrix& mat)
	{
		m_cb1_PerObject.m_Data.mW = mat;
	}

	void SetMaterial(ZMaterial& mate);

	void SetTextures(ZMaterial& mate);

private:
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;
	ZConstantBuffer<cbPerMaterial> m_cb2_PerMaterial;
};

#endif

