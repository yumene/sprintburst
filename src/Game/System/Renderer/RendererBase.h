#ifndef __RENDERER_BASE_H__
#define __RENDERER_BASE_H__

class RendererBase
{
public:
	RendererBase()
	{
	}

	virtual ~RendererBase()
	{
	}

	// 初期化
	virtual bool Init(const std::string& vsPath, const std::string& psPath) = 0;

	// 解放
	virtual void Release() = 0;

protected:
	// コンタクトバッファをシェーダーにセット
	virtual void SetContactBuffers() = 0;

protected:
	ZVertexShader m_VS;
	ZPixelShader m_PS;

};

template<typename ...Args>
class BatchRenderer : public RendererBase
{
public:
	BatchRenderer()
	{
	}
	virtual ~BatchRenderer()
	{
	}

	// 初期化
	virtual bool Init(const std::string& vsPath, const std::string& psPath) = 0;

	// 解放
	virtual void Release() = 0;

	// 描画(バッファに保存)
	virtual void Submit(Args ...param) = 0;
	// 本描画(バッファに溜め込まれた描画情報を元に描画)
	virtual void Flash() = 0;

protected:
	// コンタクトバッファをシェーダーにセット
	virtual void SetContactBuffers() = 0;

};

#endif