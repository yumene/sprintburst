#ifndef __COMMON_COMPONENTS_H__
#define __COMMON_COMPONENTS_H__

#include "ZECS/ECSComponent.h"

// 移動量コンポーネント
DefComponent(VelocityComponent)
{
	ZVec3 Velocity;
};

// ゲーム用モデルコンポーネント
DefComponent(GameModelComponent)
{
	sptr<ZGameModel> Model;
};

// 単一メッシュコンポーネント
DefComponent(SingleModelComponent)
{
	sptr<ZSingleModel> Model;
};

// ゲーム用モデルのボーンコントローラーコンポーネント
DefComponent(ModelBoneControllerConponent)
{
	sptr<ZBoneController> BoneController;
};

// アニメーターコンポーネント
DefComponent(AnimatorComponent)
{
	sptr<ZAnimator> Animator;
};

#endif