#ifndef __VR_h__
#define __VR_h__

#include <openvr.h>

//============================================================
//このソースを含んだプログラムをビルドするときは
//"Lib/opnevr"内の"openvr_api.dll"と"openvr_api.pdb"を
//"C:\Windows\SysWOW64"フォルダに入れてください
//
//ビルドした実行ファイルを起動する場合はexeファイルと
//同じ階層に、上記の"openvr_api.dll"を置いて起動してください
//============================================================

#pragma region VRController

//========================================
//Viveコントローラーを使用する際に使うクラス
//========================================

class CVRController {
public:

	// キーフラグ
	enum KEYFLAG {
		KEY_TRIGGER = 1,		// 押した瞬間
		KEY_BUTTON = 2,		// 押してる状態
		KEY_RELEASE = 4			// 離した瞬間
	};

	// コントローラの行列を取得(ワールド)
	ZMatrix GetMatrix();

	// ボタン判定
	bool  CheckButton(vr::EVRButtonId button, KEYFLAG flag = KEY_BUTTON) {
		return m_ButtonFlag[button] & flag ? true : false;
	}
	bool  Script_CheckButton(int button, int flag) {
		return m_ButtonFlag[button] & flag ? true : false;
	}

	// バイブ
	//  power … 強さ 100〜3999っぽい？
	void TriggerHapticPulse(unsigned short power);

	// 
	CVRController() {
		memset(m_ButtonFlag, 0, sizeof(m_ButtonFlag));
	}

	// 初期設定
	void Init(int no) {
		m_ControllerNo = no;
		if (no == 0)m_LocalMat.CreateMove(0.3f, -0.1f, 0.4f);
		if (no == 1)m_LocalMat.CreateMove(-0.3f, -0.1f, 0.4f);
		m_DeviceNo = -1;
	}

	// 更新処理
	void Update();

	vr::VRControllerState_t&	GetNowState() { return m_NowState; }

	ZVec2 GetAxis(int idx) {
		return (ZVec2&)m_NowState.rAxis[idx];
	}

private:
	vr::VRControllerState_t		m_NowState;
	DWORD						m_ButtonFlag[vr::EVRButtonId::k_EButton_Max];

	int							m_ControllerNo = 0;
	int							m_DeviceNo = -1;			// デバイスの番号 状況によって変わるかも？

	ZMatrix						m_LocalMat;	// コントローラ無し時の仮の行列
};
#pragma endregion

#pragma region VRCamera

//========================================
//HMDを使用する際に使うCameraのクラス
//通常のカメラの代わりに使用する
//========================================

class CVRCamera : public ZCamera {
public:
	float		m_CamRx;		// カメラのX軸回転角度
	float		m_CamRy;		// カメラのY軸回転角度
								//	float		m_CamZoom;		// カメラのズーム値
	ZMatrix		m_CamBaseMat;


	//	static CVRCamera	LastCamVR;

	static const float m_CamMoveSpeed;

	//
	CVRCamera() {
		// カメラデータ初期化
		m_CamRx = 0;
		m_CamRy = 0;
		//		m_CamZoom = 15;
		m_CamBaseMat.CreateIdentity();
	}

	void Init() {
		m_CamRx = 0;
		m_CamRy = 0;
		//		m_CamZoom = CamZoom;
		m_CamBaseMat.CreateIdentity();
	}

	/*void Init(float rx, float ry, float camZoom, bool isFpsMode = false)
	{
	m_CamBaseMat.CreateRotateX(rx);
	m_CamBaseMat.RotateY(ry);
	/*m_CamBaseMat.Move_Local(0, 0, camZoom);
	m_CamBaseMat.CreateMove(0, 0, camZoom);
	//m_IsFPSMode = isFpsMode;
	}*/

	// 処理
	void Update();

	ZMatrix GetMatrix();

	// セット
	void SetCamera();

};
#pragma endregion

#pragma region openVR class

//========================================
//HMDを使用する場合に必要なクラス
//========================================

class CVR {
public:

	//----------------------------------------------------------
	// 初期化
	//----------------------------------------------------------
	bool Init();

	//----------------------------------------------------------
	// 解放
	//----------------------------------------------------------
	void Release();

	//----------------------------------------------------------
	// 右目、左目用のテクスチャをHMDへ送信する
	//----------------------------------------------------------
	void Submit(vr::EVREye eye);

	// 
	void WaitGetPoses();
public:

	// 更新処理
	void Update();

	// カメラ取得
//	CVRCamera& GetCamera(){ return m_Cam; }

	//----------------------------------------------------------
	// 描画モード
	//----------------------------------------------------------
	enum RenderMode {
		RM_Left = 0,		// 左目モード
		RM_Right = 1,		// 右目モード
		RM_Center = 2,		// 通常モード
		RM_None = 999,
	};

	enum ControllerNum {
		R = 0,
		L = 1,
	};

	//----------------------------------------------------------
	// 右目、左目、センター描画モードを変更する
	//  RM_Left		… 左目描画モード中
	//  RM_Right	… 右目描画モード中
	//  RM_Center	… センター描画モード中
	//----------------------------------------------------------
	void BeginRender(RenderMode mode);

	//----------------------------------------------------------
	// 描画モードを終了する
	//  RM_None		… 描画モード中ではない
	//----------------------------------------------------------
	void EndRender();

	//----------------------------------------------------------
	// 現在のレンダーモードを取得(RenderMode)
	//  RM_Left		… 左目描画モード中
	//  RM_Right	… 右目描画モード中
	//  RM_Center	… センター描画モード中
	//  RM_None		… 描画モード中ではない
	//----------------------------------------------------------
	int GetNowRenderMode(){
		return m_NowRenderMode;
	}

	//----------------------------------------------------------
	// 現在のHMDのワールド行列を取得(※右目、左目モードも含む)　ここでいうワールド行列は、カメラを考慮したもの
	//----------------------------------------------------------
	ZMatrix	GetNowHMDPoseWithEye_World();

	//----------------------------------------------------------
	// 現在のHMDのローカル行列を取得(※右目、左目モードも含む)
	// ワールド行列はCVRCameraにより変わるので、ワールド行列を取得したい場合は、
	// m_LastCam.mCamを使うとよい
	//  ※未接続時は、マウスモード行列を返す
	//----------------------------------------------------------
	ZMatrix	GetNowHMDPoseWithEye();

	// 
	ZMatrix GetNowHDMPose_World();

	// 現在のHMDのローカル行列を取得
	ZMatrix GetNowHDMPose();


	// 現在のHDM用射影行列を取得(※右目、左目モードも含む)
	ZMatrix GetNowProjectionMatrix(){
		if(m_NowRenderMode == RM_Left){
			return m_mat4ProjectionLeft;
		}
		else if(m_NowRenderMode == RM_Right){
			return m_mat4ProjectionRight;
		}
		else{
			ZMatrix proj;
			proj.CreatePerspectiveFovLH(60, (float)ZDx.GetRezoW() / ZDx.GetRezoH(), 0.01f, 1000);
			return proj;
		}
	}

	ZMatrix GetEyeProj_Left(){ return m_mat4ProjectionLeft; }
	ZMatrix GetEyeProj_Right(){ return m_mat4ProjectionRight; }
	ZMatrix GetEyePos_Left(){ return m_mEyePosLeft; }
	ZMatrix GetEyePos_Right(){ return m_mEyePosRight; }

	int GetEyeTextureWidth(){ return (int)m_RT_Left.GetInfo().Width; }
	int GetEyeTextureHeight(){ return (int)m_RT_Left.GetInfo().Height; }

	void CopyDrawEyeTexture(int RLFlag, ZTexture* srcTex);

	// コントローラの行列を返す
	//  controllerNo … コントローラの番号(0 or 1)
	const ZMatrix*	Controller_GetMatrix(int controllerNo);

	// コントローラのボタン入力の状態を判定
	//  isCheckPoseValid	… 識別可能状態のときのみ取得する？
	//  isTouched			… true:Touchイベントを取得(アナログ系のタッチやトリガー) false:Pressイベントを取得(押し込んだとき)
//	bool Controller_CheckButton(int controllerNo, vr::EVRButtonId buttonID, bool isCheckPoseValid = false, bool isTouched = false, ZVec2 outAxisVal[vr::k_unControllerStateAxisCount] = nullptr);

	bool Controller_GetState(int controllerNo, vr::VRControllerState_t& outState, int* outDeviceNo = nullptr);

	// コントローラデータを取得
	CVRController& Controller_GetData(int controllerID){ return m_ControllerData[controllerID]; }

	// ベースステーション
//	const ZMatrix*	GetTrackingRefMatrix(int controllerNo);

	// 初期化されているか
	bool IsInit(){ return m_pHMD != nullptr; }
	// ワークテクスチャ取得
	ZTexture&	GetLeftTex(){ return m_RT_Left; }
	ZTexture&	GetRightTex(){ return m_RT_Right; }
	ZTexture&	GetCenterTex(){ return m_RT_Center; }

	// 現在の目モードのレンダーターゲットを取得
	ZTexture&	GetNowRT(){
		if(m_NowRenderMode == RM_Left)return m_RT_Left;
		else if(m_NowRenderMode == RM_Right)return m_RT_Right;
		else return m_RT_Center;
	}
	// 現在の目モードの深度バッファを取得
	ZTexture&	GetNowDepth(){
		if(m_NowRenderMode == RM_Left)return m_Depth_Eye;
		else if(m_NowRenderMode == RM_Right)return m_Depth_Eye;
		else return m_Depth_Center;
	}

	vr::IVRSystem*	GetVRSystem(){ return m_pHMD; }

	CVRCamera	m_LastCamVR;

private:
//public:


	vr::IVRSystem*				m_pHMD = nullptr;				// Main interface for display, distortion, tracking, controller, and event access.
	vr::IVRRenderModels*		m_pRenderModels = nullptr;
	vr::TrackedDevicePose_t		m_rTrackedDevicePose[vr::k_unMaxTrackedDeviceCount];
	int							m_iValidPoseCount;
	int							m_iValidPoseCount_Last;
	std::string					m_strPoseClasses;

	// 全デバイス情報
	ZMatrix						m_rmat4DevicePose[vr::k_unMaxTrackedDeviceCount];	// 接続されている全デバイスの行列
	char						m_rDevClassChar[vr::k_unMaxTrackedDeviceCount];

	// 現在の描画モード
	int							m_NowRenderMode = RM_None;
	ZRenderTargets				m_backupRTs;

	// 左右の作業テクスチャ
	ZTexture					m_RT_Left;
	ZTexture					m_RT_Right;

	private:
	ZTexture					m_Depth_Eye;

	// センター用作業テクスチャ
	ZTexture					m_RT_Center;
	ZTexture					m_Depth_Center;

	// 
	float m_fNearClip;
	float m_fFarClip;

	ZMatrix m_mHMDPose;			// ヘッドマウントディスプレイの行列
	ZMatrix m_mEyePosLeft;		// 左目のローカル位置
	ZMatrix m_mEyePosRight;		// 右目のローカル位置

	ZMatrix m_mat4ProjectionLeft;	// 左目の射影行列
	ZMatrix	m_mat4ProjectionRight;	// 右目の射影行列



	ZMatrix GetHMDMatrixPoseEye(vr::Hmd_Eye nEye);
	ZMatrix GetHMDMatrixProjectionEye(vr::Hmd_Eye nEye);
	ZMatrix ConvertSteamVRMatrixToMatrix(const vr::HmdMatrix34_t &matPose);


	CVRController		m_ControllerData[2];

//	CVRCamera			m_Cam;

	// 
	void SetupCameras();

//================================================
// シングルトン
//================================================
private:
	CVR(){
		m_iValidPoseCount_Last = -1;
		memset(m_rDevClassChar, 0, sizeof(m_rDevClassChar));
		int i = 0;
		for(auto& ctrl : m_ControllerData){
			ctrl.Init(i);
			i++;
		}
	}

	static CVR* s_pInstance;

public:
	/*
	static CVR& GetInstance(){
		static CVR instance;
		return instance;
	}
	*/
	static CVR& GetInstance() {
		return *s_pInstance;
	}
	static void CreateInstance(){
		DeleteInstance();
		if(s_pInstance == nullptr){
			s_pInstance = new CVR();
		}
	}

	static void DeleteInstance(){
		if(s_pInstance){
			//delete s_pInstance;
			s_pInstance = nullptr;
		}

	}

	static void SetInstance(CVR* inst){
		s_pInstance = inst;
	}
};

#define cvr CVR::GetInstance()

#pragma endregion

#endif

// IVRSystem … Main interface for display, distortion, tracking, controller, and event access.
// IVRCompositor … 3DコンテンツをVRに流す
// IVROverlay … 2DコンテンツをVRに流す

