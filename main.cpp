#include <iostream>
#include <stdio.h>
#include "EzLib/MainFrame/ZMainFrame.h"
#include "EzLib/ZECS/ZECS.h"


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpszArgs, int nWinMode)
{
	// メモリリーク検知
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	// COM初期化
	CoInitializeEx(nullptr, COINIT_MULTITHREADED);

	// mbstorwsc_s関数で日本語対応にするため
	setlocale(LC_ALL, "japan");
	
	ZWindowProperties properties{hInstance,1280,720,false,false};

	if (IDYES == MessageBox(NULL, "フルスクリーンにしますか?", "確認", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2))
		properties.UseFullScreen = true;

	ZMainFrame mainframe("DX11 Test", properties);
	mainframe.Start();

	// COM開放
	CoUninitialize();

	return 0;
}



