#ifndef __PATHUTIL_H__
#define __PATHUTIL_H__

#include <string>
#include <vector>

namespace EzLib
{
	class ZPathUtil
	{
	public:
		// インスタンス化禁止
		ZPathUtil() = delete;
		ZPathUtil(const ZPathUtil& pathUtil) = delete;
		ZPathUtil& operator=(const ZPathUtil& pathUtil) = delete;
	
		// カレントディレクトリ取得
		static std::string GetCurrentDir();
		
		// 実行可能ファイル取得
		static std::string GetExecutablePath();
		
		// 結合
		static std::string Combine(const std::vector<std::string>& parts);
		static std::string Combine(const std::string& a,const std::string& b);
	
		// パスからディレクトリ名取得
		static std::string GetDirName(const std::string& path);
	
		// パスからファイル名を取得
		static std::string GetFileName(const std::string& path);
	
		// ファイル名から拡張子以外を取得
		static std::string GetFileName_NonExt(const std::string& path);
	
		// ファイル名から拡張子を取得
		static std::string GetExt(const std::string& path);
	
		// 区切り文字(\\)取得
		static std::string GetDelimiter();
	
		// 区切り文字の正規化
		static std::string Normalize(const std::string& path);
	
	};

}
#endif