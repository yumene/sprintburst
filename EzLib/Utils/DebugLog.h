#ifndef __DEBUG_LOG_H__
#define __DEBUG_LOG_H__

#include "EzLib.h"

namespace EzLib
{
	static void DebugLog(const std::string& log)
	{
		OutputDebugString(log.c_str());
	}

	static void DebugLog_fmt(const char* fmt, ...)
	{
		va_list argList;

		va_start(argList, fmt);

		const size_t MAX_CHARS = 1024;
		static char strBuf[MAX_CHARS];
		int charsWritten = vsnprintf(strBuf, MAX_CHARS, fmt, argList);

		DebugLog(strBuf);

		va_end(argList);
	}

}


#endif