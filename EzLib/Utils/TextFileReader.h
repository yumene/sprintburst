#ifndef __TEXTFILE_READER_H__
#define __TEXTFILE_READER_H__

#include <string>
#include <vector>
#include "ZFile.h"

namespace EzLib
{
	class TextFileReader
	{
	public:
		TextFileReader() = default;
		TextFileReader(const char* path);
		TextFileReader(const std::string& path);
	
		bool Open(const char* path);
		bool Open(const std::string& path);
		
		void Close();
		bool IsOpen();
	
		std::string ReadLine();
		void ReadAllLine(std::vector<std::string>& lines);
		std::string ReadAll();
		bool IsEOF();
	
	private:
		ZFile m_File;
	};
}

#endif