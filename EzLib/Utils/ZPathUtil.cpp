#include <Windows.h>
#include <algorithm>
#include "ZPathUtil.h"

namespace EzLib
{

namespace
{
	const char PathDelimiter = '\\';
	const char* PathDelimiters = "\\/";
}

	std::string ZPathUtil::GetCurrentDir()
	{
		std::string cDir;
		DWORD sz = GetCurrentDirectory(0, nullptr);
		cDir.resize(sz);
		GetCurrentDirectory(sz, &cDir[0]);
		return std::move(cDir);
	}

	std::string ZPathUtil::GetExecutablePath()
	{
		std::string modulePath;
		modulePath.resize(MAX_PATH);
		if (GetModuleFileName(NULL,&modulePath[0], (DWORD)modulePath.size()) == 0)
			return std::move(std::string());
	
		return std::move(modulePath);
	}

	std::string ZPathUtil::Combine(const std::vector<std::string>& parts)
	{
		std::string result;

		for(const auto& part : parts)
		{
			if(part.empty() == false)
			{
				auto pos = part.find_last_not_of(PathDelimiters);
				if (pos != std::string::npos)
				{
					if (result.empty() == false)
						result.append(&PathDelimiter, 1);
					result.append(part.c_str(), pos + 1);
				}
			}
		}

		return std::move(result);
	}

	std::string ZPathUtil::Combine(const std::string& a, const std::string& b)
	{
		return std::move(Combine({a,b}));
	}

	std::string ZPathUtil::GetDirName(const std::string& path)
	{
		auto pos = path.find_last_of(PathDelimiters);
		if(pos == std::string::npos)
			return std::move(std::string());

		return std::move(path.substr(0, pos + 1));
	}

	std::string ZPathUtil::GetFileName(const std::string& path)
	{
		auto pos = path.find_last_of(PathDelimiters);
		if (pos == std::string::npos)
			return std::move(path);

		return std::move(path.substr(pos + 1,path.size()-pos));
	}

	std::string ZPathUtil::GetFileName_NonExt(const std::string& path)
	{
		const std::string fname = GetFileName(path);
		auto pos = fname.find_last_of('.');
		if (pos == std::string::npos)
			return std::move(fname);

		return std::move(fname.substr(0,pos));
	}

	std::string ZPathUtil::GetExt(const std::string& path)
	{
		auto pos = path.find_last_of('.');
		if (pos == std::string::npos)
			return std::move(std::string());

		std::string ext = path.substr(pos + 1, path.size() - pos);
		for (auto& c : ext)
			c = (char)tolower(c);

		return std::move(ext);
	}

	std::string ZPathUtil::GetDelimiter()
	{
		return std::move(std::string("\\"));
	}

	std::string ZPathUtil::Normalize(const std::string& path)
	{
		std::string result = path;
		std::replace(result.begin(), result.end(), '/', '\\');

		return std::move(result);
	}


}
