#include "EzLib.h"

namespace EzLib
{
	ZThreadPool::ZThreadPool(size_t threadCount)
		: m_IsTerminationRequested(false)
	{
		m_ThreadLoopFunc = [this]()
		{
			while (true)
			{
				Task task;
				{
					std::unique_lock<std::mutex> ul(m_Mtx);
					
					m_CV.wait(ul, [=] { return m_IsTerminationRequested || !m_Tasks.empty(); });

					if (m_IsTerminationRequested)
						break;

					task = std::move(m_Tasks.front());
					m_Tasks.pop();
				}
				task();
			}
		};

		for (size_t i = 0; i < threadCount; i++)
			m_Threads.emplace_back(std::thread(m_ThreadLoopFunc));
	}

	ZThreadPool::~ZThreadPool()
	{
		{
			std::unique_lock<std::mutex> ul(m_Mtx);
			m_IsTerminationRequested = true;
		}
		m_CV.notify_all();
		
		const size_t size = m_Threads.size();
		for (auto& thread : m_Threads)
			thread.join();
	}

}