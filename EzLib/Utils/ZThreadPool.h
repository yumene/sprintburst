#ifndef __ZTHREAD_POOL_H__
#define __ZTHREAD_POOL_H__

namespace EzLib
{
	class ZThreadPool
	{
	private:
		using Task = std::function<void()>;

	public:
		ZThreadPool(size_t threadCount);
		~ZThreadPool();

		// タスク追加
		template<class T>
		auto AddTask(T runnable)->std::future<decltype(runnable())>
		{
			auto wrapper = std::make_shared<std::packaged_task<decltype(runnable())()>>(std::move(runnable));
			{
				std::unique_lock<std::mutex> ul(m_Mtx);
				m_Tasks.push([=] { (*wrapper)(); });
			}

			// スレッド起動
			m_CV.notify_one();

			return wrapper->get_future();
		}

		size_t GetNumThreads() const
		{
			return m_Threads.size();
		}

	private:
		std::function<void()> m_ThreadLoopFunc;
		bool m_IsTerminationRequested;			// 終了要求があったか
		std::queue<Task> m_Tasks;
		std::mutex m_Mtx;
		std::condition_variable m_CV;
		std::vector<std::thread> m_Threads;

	};

}

#endif