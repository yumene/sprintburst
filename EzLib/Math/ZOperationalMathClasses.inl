//=========================================================================
//
//
// ZOperationalVec3
//
//
//=========================================================================

namespace EzLib
{

	inline ZOperationalVec3& ZOperationalVec3::operator+= (const ZOperationalVec3& V)
	{
		v += V;
		return *this;
	}

	inline ZOperationalVec3& ZOperationalVec3::operator-= (const ZOperationalVec3& V)
	{
		v -= V;
		return *this;
	}

	inline float ZOperationalVec3::Dot(const DirectX::XMVECTOR& v1, const DirectX::XMVECTOR& v2)
	{
		DirectX::XMVECTOR X = DirectX::XMVector3Dot(v1, v2);
		return DirectX::XMVectorGetX(X);
	}

	inline float ZOperationalVec3::Dot(const DirectX::XMVECTOR &v1)const
	{
		DirectX::XMVECTOR X = DirectX::XMVector3Dot(v, v1);
		return DirectX::XMVectorGetX(X);
	}

	inline void ZOperationalVec3::Cross(ZOperationalVec3 &vOut, const ZOperationalVec3& v1, ZOperationalVec3& v2)
	{
		vOut = DirectX::XMVector3Cross(v1.v, v2.v);
	}

	inline float ZOperationalVec3::Length()const
	{
		DirectX::XMVECTOR X = DirectX::XMVector3Length(v);
		return DirectX::XMVectorGetX(X);
	}

	inline DirectX::XMVECTOR ZOperationalVec3::Length2()const
	{
		return DirectX::XMVector3Length(v);
	}

	inline float ZOperationalVec3::LengthSq()const
	{
		DirectX::XMVECTOR X = DirectX::XMVector3LengthSq(v);
		return DirectX::XMVectorGetX(X);
	}

	inline void ZOperationalVec3::CrossBack(DirectX::XMVECTOR& vOut, const DirectX::XMVECTOR &v1)const
	{
		vOut = DirectX::XMVector3Cross(v, v1);
	}

	inline void ZOperationalVec3::CrossFront(DirectX::XMVECTOR& vOut, const DirectX::XMVECTOR &v1)const
	{
		vOut = DirectX::XMVector3Cross(v1, v);
	}

	inline void ZOperationalVec3::Transform(DirectX::XMVECTOR& vOut, const DirectX::XMVECTOR& v1, const DirectX::XMMATRIX& mat)
	{
		vOut = DirectX::XMVector3TransformCoord(v1, mat);
	}

	inline void ZOperationalVec3::TransformNormal(DirectX::XMVECTOR& vOut, const DirectX::XMVECTOR& v1, const DirectX::XMMATRIX& mat)
	{
		vOut = DirectX::XMVector3TransformNormal(v1, mat);
	}

	inline void ZOperationalVec3::Store(DirectX::XMFLOAT3 &out) const
	{
		DirectX::XMStoreFloat3(&out, v);
	}

	inline DirectX::XMFLOAT3 ZOperationalVec3::Store() const
	{
		DirectX::XMFLOAT3 vOut;
		DirectX::XMStoreFloat3(&vOut, v);
		return vOut;
	}


	inline void ZOperationalVec3::Normalize()
	{
		v = DirectX::XMVector3Normalize(v);
	}

	inline ZOperationalVec3 operator+ (const ZOperationalVec3& V1, const ZOperationalVec3& V2)
	{
		return V1.v + V2.v;
	}

	inline ZOperationalVec3 operator- (const ZOperationalVec3& V1, const ZOperationalVec3& V2)
	{
		//	return DirectX::XMVectorSubtract(V1, V2);
		return V1.v - V2.v;
	}

	inline ZOperationalVec3 operator* (const ZOperationalVec3& V1, float S)
	{
		return DirectX::XMVectorScale(V1.v, S);// V1.v* S;
	}

	inline ZOperationalVec3 operator/ (const ZOperationalVec3& V1, float S)
	{
		return DirectX::XMVectorScale(V1.v, 1.0f / S);
	}

	//=========================================================================
	//
	//
	// ZOperationalMatrix
	//
	//
	//=========================================================================


	inline void ZOperationalMatrix::CreateMove(float x, float y, float z)
	{
		*this = DirectX::XMMatrixTranslation(x, y, z);
	}

	inline void ZOperationalMatrix::CreateRotateX(float deg)
	{
		*this = DirectX::XMMatrixRotationX(DirectX::XMConvertToRadians(deg));
	}

	inline void ZOperationalMatrix::CreateRotateY(float deg)
	{
		*this = DirectX::XMMatrixRotationY(DirectX::XMConvertToRadians(deg));
	}

	inline void ZOperationalMatrix::CreateRotateZ(float deg)
	{
		*this = DirectX::XMMatrixRotationZ(DirectX::XMConvertToRadians(deg));
	}

	inline void ZOperationalMatrix::CreateRotateAxis(const DirectX::XMVECTOR& vAxis, float deg)
	{
		*this = DirectX::XMMatrixRotationAxis(vAxis, DirectX::XMConvertToRadians(deg));
	}

	inline void ZOperationalMatrix::CreateScale(float x, float y, float z)
	{
		*this = DirectX::XMMatrixScaling(x, y, z);
	}


	inline void ZOperationalMatrix::Inverse()
	{
		*this = DirectX::XMMatrixInverse(nullptr, *this);
	}

	inline void ZOperationalMatrix::Inverse(DirectX::XMMATRIX& mOut) const
	{
		mOut = DirectX::XMMatrixInverse(nullptr, *this);
	}

	inline void EzLib::ZOperationalMatrix::Move(float x, float y, float z)
	{
		ZOperationalMatrix m;
		m.CreateMove(x, y, z);
		Multiply(m);
	}

	inline void ZOperationalMatrix::RotateX(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateX(deg);
		Multiply(m);
	}

	inline void EzLib::ZOperationalMatrix::RotateY(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateY(deg);
		Multiply(m);
	}

	inline void EzLib::ZOperationalMatrix::RotateZ(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateZ(deg);
		Multiply(m);
	}

	inline void ZOperationalMatrix::Multiply(const ZOperationalMatrix& m)
	{
		*this = DirectX::XMMatrixMultiply(*this, m);
	}

	inline void ZOperationalMatrix::Multiply(const DirectX::XMFLOAT4X4& m)
	{
		*this = DirectX::XMMatrixMultiply(*this, DirectX::XMLoadFloat4x4(&m));
	}

	inline void ZOperationalMatrix::Multiply_Local(const DirectX::XMMATRIX& m)
	{
		*this = DirectX::XMMatrixMultiply(m, *this);
	}

	inline void ZOperationalMatrix::Multiply_Local(const DirectX::XMFLOAT4X4& m)
	{
		*this = DirectX::XMMatrixMultiply(DirectX::XMLoadFloat4x4(&m), *this);
	}

	inline void ZOperationalMatrix::NormalizeScale()
	{
		r[0] = DirectX::XMVector3Normalize(r[0]);
		r[1] = DirectX::XMVector3Normalize(r[1]);
		r[2] = DirectX::XMVector3Normalize(r[2]);
	}

	inline void ZOperationalMatrix::Store(DirectX::XMFLOAT4X4& out)
	{
		DirectX::XMStoreFloat4x4(&out, *this);
	}



}