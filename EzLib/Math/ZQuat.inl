
namespace EzLib
{

	//------------------------------------------------------------------------------
	// ��r���Z�q
	//------------------------------------------------------------------------------

	inline bool ZQuat::operator == (const ZQuat& q) const
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(this);
		XMVECTOR q2 = XMLoadFloat4(&q);
		return XMQuaternionEqual(q1, q2);
	}

	inline bool ZQuat::operator != (const ZQuat& q) const
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(this);
		XMVECTOR q2 = XMLoadFloat4(&q);
		return XMQuaternionNotEqual(q1, q2);
	}

	//------------------------------------------------------------------------------
	// ������Z�q
	//------------------------------------------------------------------------------

	inline ZQuat& ZQuat::operator+= (const ZQuat& q)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(this);
		XMVECTOR q2 = XMLoadFloat4(&q);
		XMStoreFloat4(this, XMVectorAdd(q1, q2));
		return *this;
	}

	inline ZQuat& ZQuat::operator-= (const ZQuat& q)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(this);
		XMVECTOR q2 = XMLoadFloat4(&q);
		XMStoreFloat4(this, XMVectorSubtract(q1, q2));
		return *this;
	}

	inline ZQuat& ZQuat::operator*= (const ZQuat& q)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(this);
		XMVECTOR q2 = XMLoadFloat4(&q);
		XMStoreFloat4(this, XMQuaternionMultiply(q1, q2));
		return *this;
	}

	inline ZQuat& ZQuat::operator*= (float S)
	{
		using namespace DirectX;
		XMVECTOR q = XMLoadFloat4(this);
		XMStoreFloat4(this, XMVectorScale(q, S));
		return *this;
	}

	inline ZQuat& ZQuat::operator/= (const ZQuat& q)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(this);
		XMVECTOR q2 = XMLoadFloat4(&q);
		q2 = XMQuaternionInverse(q2);
		XMStoreFloat4(this, XMQuaternionMultiply(q1, q2));
		return *this;
	}

	//------------------------------------------------------------------------------
	// Urnary operators
	//------------------------------------------------------------------------------

	inline ZQuat ZQuat::operator- () const
	{
		using namespace DirectX;
		XMVECTOR q = XMLoadFloat4(this);

		ZQuat R;
		XMStoreFloat4(&R, XMVectorNegate(q));
		return R;
	}


	//------------------------------------------------------------------------------
	// Binary operators
	//------------------------------------------------------------------------------

	inline ZQuat operator+ (const ZQuat& Q1, const ZQuat& Q2)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(&Q1);
		XMVECTOR q2 = XMLoadFloat4(&Q2);

		ZQuat R;
		XMStoreFloat4(&R, XMVectorAdd(q1, q2));
		return R;
	}

	inline ZQuat operator- (const ZQuat& Q1, const ZQuat& Q2)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(&Q1);
		XMVECTOR q2 = XMLoadFloat4(&Q2);

		ZQuat R;
		XMStoreFloat4(&R, XMVectorSubtract(q1, q2));
		return R;
	}

	inline ZQuat operator* (const ZQuat& Q1, const ZQuat& Q2)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(&Q1);
		XMVECTOR q2 = XMLoadFloat4(&Q2);

		ZQuat R;
		XMStoreFloat4(&R, XMQuaternionMultiply(q1, q2));
		return R;
	}

	inline ZQuat operator* (const ZQuat& Q, float S)
	{
		using namespace DirectX;
		XMVECTOR q = XMLoadFloat4(&Q);

		ZQuat R;
		XMStoreFloat4(&R, XMVectorScale(q, S));
		return R;
	}

	inline ZQuat operator/ (const ZQuat& Q1, const ZQuat& Q2)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(&Q1);
		XMVECTOR q2 = XMLoadFloat4(&Q2);
		q2 = XMQuaternionInverse(q2);

		ZQuat R;
		XMStoreFloat4(&R, XMQuaternionMultiply(q1, q2));
		return R;
	}

	inline ZQuat operator* (float S, const ZQuat& Q)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(&Q);

		ZQuat R;
		XMStoreFloat4(&R, XMVectorScale(q1, S));
		return R;
	}

	//
	inline float ZQuat::Dot(const ZQuat &q) const
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(this);
		XMVECTOR q2 = XMLoadFloat4(&q);
		return XMVectorGetX(XMQuaternionDot(q1, q2));
	}

	inline float ZQuat::Dot(const ZQuat& q1, const ZQuat& q2)
	{
		using namespace DirectX;
		XMVECTOR Q1 = XMLoadFloat4(&q1);
		XMVECTOR Q2 = XMLoadFloat4(&q2);
		return XMVectorGetX(XMQuaternionDot(Q1, Q2));
	}

	inline void ZQuat::Conjugate()
	{
		using namespace DirectX;
		XMVECTOR q = XMLoadFloat4(this);
		XMStoreFloat4(this, XMQuaternionConjugate(q));
	}

	inline void ZQuat::Inverse(ZQuat& qOut, const ZQuat& qIn)
	{
		using namespace DirectX;
		XMVECTOR q = XMLoadFloat4(&qIn);
		XMStoreFloat4(&qOut, XMQuaternionInverse(q));
	}

	inline void ZQuat::Normalize(ZQuat& out, const ZQuat& qIn)
	{
		using namespace DirectX;
		XMVECTOR q = XMLoadFloat4(&qIn);
		XMStoreFloat4(&out, XMQuaternionNormalize(q));
	}

	inline float ZQuat::Length(const ZQuat& qIn)
	{
		using namespace DirectX;
		XMVECTOR q = XMLoadFloat4(&qIn);
		return XMVectorGetX(XMQuaternionLength(q));
	}

	inline void ZQuat::Multiply(const ZQuat& qIn)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(this);
		XMVECTOR q2 = XMLoadFloat4(&qIn);
		XMStoreFloat4(this, XMQuaternionMultiply(q1, q2));
	}

	inline void ZQuat::Multiply_Local(const ZQuat& qIn)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(&qIn);
		XMVECTOR q2 = XMLoadFloat4(this);
		XMStoreFloat4(this, XMQuaternionMultiply(q1, q2));
	}

	inline void ZQuat::Multiply(ZQuat& qOut, const ZQuat& qIn1, const ZQuat& qIn2)
	{
		using namespace DirectX;
		XMVECTOR q1 = XMLoadFloat4(&qIn1);
		XMVECTOR q2 = XMLoadFloat4(&qIn2);
		XMStoreFloat4(&qOut, XMQuaternionMultiply(q1, q2));
	}


	inline void ZQuat::ToMatrix(ZMatrix &out) const
	{
		out = DirectX::XMMatrixRotationQuaternion(*this);
	}

	inline void ZQuat::ToMatrix(ZMatrix &out, ZQuat &qIn)
	{
		out = DirectX::XMMatrixRotationQuaternion(qIn);
	}

	inline ZMatrix ZQuat::ToMatrix()const
	{
		ZMatrix mat;
		ToMatrix(mat);
		return mat;
	}

	inline void ZQuat::Slerp(ZQuat& out, const ZQuat& q1, const ZQuat& q2, float f)
	{
		using namespace DirectX;
		XMVECTOR Q0 = XMLoadFloat4(&q1);
		XMVECTOR Q1 = XMLoadFloat4(&q2);
		XMStoreFloat4(&out, XMQuaternionSlerp(Q0, Q1, f));
	}


}
