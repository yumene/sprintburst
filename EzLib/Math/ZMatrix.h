//===============================================================
//  @file ZMatrix.h
//   行列クラス
// 
//  @author 鎌田
//===============================================================
#ifndef ZMatrix_h
#define ZMatrix_h

namespace EzLib
{

	class ZQuat;
	class ZOperationalMatrix;

	//==========================================================================================================
	//   行列操作クラス
	// 
	//  @ingroup Math
	//==========================================================================================================
	class ZMatrix : public DirectX::XMFLOAT4X4
	{
	public:

		//============================================================
		// メンバ関数
		//============================================================
		//
		ZMatrix() : DirectX::XMFLOAT4X4(1.0f, 0, 0, 0,
										0, 1.0f, 0, 0,
										0, 0, 1.0f, 0,
										0, 0, 0, 1.0f)
		{
		}

		// casting
		operator float* () { return &_11; }

		//   演算用行列から、コピーしてくる
		ZMatrix(const DirectX::XMMATRIX& M) { DirectX::XMStoreFloat4x4(this, M); }
		// 
		operator DirectX::XMMATRIX() const { return XMLoadFloat4x4(this); }

		// 比較演算子
		bool operator == (const ZMatrix& M) const;
		bool operator != (const ZMatrix& M) const;

		// 代入演算子
		ZMatrix& operator= (const ZMatrix& M) { memcpy_s(this, sizeof(float)* 16, &M, sizeof(float)* 16); return *this; }
		ZMatrix& operator+= (const ZMatrix& M);
		ZMatrix& operator-= (const ZMatrix& M);
		ZMatrix& operator*= (const ZMatrix& M);
		ZMatrix& operator*= (float S);
		ZMatrix& operator/= (float S);

		ZMatrix& operator/= (const ZMatrix& M);
		// Element-wise divide

		ZMatrix& operator=(const DirectX::XMFLOAT4X4& m)
		{
			*(DirectX::XMFLOAT4X4*)this = m;
			return *this;
		}

		ZMatrix& operator=(const DirectX::XMMATRIX& M)
		{
			DirectX::XMStoreFloat4x4(this, M);
			return *this;
		}


		// Urnary operators
		ZMatrix operator+ () const { return *this; }
		ZMatrix operator- () const;

		//========================================================================
		//
		//
		//
		// 取得系
		//
		//
		//
		//========================================================================

		//   座標をベクトル型で取得
		ZVec3& GetPos() { return (ZVec3&)_41; }
		const ZVec3& GetPos() const { return (ZVec3&)_41; }

		//   X軸をベクトル型で取得
		ZVec3& GetXAxis() { return (ZVec3&)_11; }
		const ZVec3& GetXAxis() const { return (ZVec3&)_11; }

		//   Y軸をベクトル型で取得
		ZVec3& GetYAxis() { return (ZVec3&)_21; }
		const ZVec3& GetYAxis() const { return (ZVec3&)_21; }

		//   Z軸をベクトル型で取得
		ZVec3& GetZAxis() { return (ZVec3&)_31; }
		const ZVec3& GetZAxis() const { return (ZVec3&)_31; }

		//   X拡大率を取得
		float GetXScale() const
		{
			return ZVec3::Length((ZVec3&)_11);
		}
		//   Y拡大率を取得
		float GetYScale() const
		{
			return ZVec3::Length((ZVec3&)_21);
		}
		//   Z拡大率を取得
		float GetZScale() const
		{
			return ZVec3::Length((ZVec3&)_31);
		}
		//   拡大率(x,y,z)をベクトルで取得
		ZVec3 GetScale() const
		{
			return ZVec3(GetXScale(), GetYScale(), GetZScale());
		}


		// 行列からオイラー角を求める
		// ただし、XYZの順番で合成されたときの角度となる。
		//  参考：http://mikaduki2007.blog.shinobi.jp/Entry/279/
		void ComputeAngle_XYZ(float& angX, float& angY, float& angZ)const;

		void ComputeAngle_XYZ(ZVec3& ang)const
		{
			ComputeAngle_XYZ(ang.x, ang.y, ang.z);
		}

		ZVec3 ComputeAngle_XYZ()const
		{
			ZVec3 ang;
			ComputeAngle_XYZ(ang);
			return std::move(ang);
		}

		//   射影行列から、視野角・最近接距離・最遠方距離を求める
		//  @param[out]	outAng		… 視野角が入ってくる
		//  @param[out] outAspect	… アスペクト比が入ってくる
		//  @param[out]	outNerar	… 最近接距離が入ってくる
		//  @param[out]	outFar		… 最遠方距離が入ってくる
		void ComputePerspectiveInfo(float& outAng, float& outAspect, float& outNear, float& outFar) const;

		//	bool Decompose(ZVec3& scale, Quaternion& rotation, ZVec3& translation);

			//========================================================================
			//
			//
			//
			// 行列「合成」系関数･･･行列を作成し、合成していく(相対的変化)
			//
			//
			//
			//========================================================================

			//   移動をワールド合成。
			//  	x … x座標
			//  	y … y座標
			//  	z … z座標
		void Move(float x, float y, float z);

		//   移動をワールド合成。
		//  	v … ベクトル
		void Move(const ZVec3 &v);

		//   移動をローカル合成。
		//  	x … x座標
		//  	y … y座標
		//  	z … z座標
		void Move_Local(float x, float y, float z);

		//   移動をローカル合成。
		//  	v … ベクトル
		void Move_Local(const ZVec3 &v);

		//   X回転をワールド合成。
		//  	deg … 回転角度(度)
		void RotateX(float deg);

		//   Y回転をワールド合成。
		//  	deg … 回転角度(度)
		void RotateY(float deg);

		//   Z回転をワールド合成。
		//  	deg … 回転角度(度)
		void RotateZ(float deg);

		//   指定軸回転をワールド合成。
		//  	vAxis	… 回転軸
		//  	deg		… 回転角度(度)
		void RotateAxis(const ZVec3 &vAxis, float deg);

		//   X回転をワールド合成(座標は変化させない)。
		//  	deg		… 回転角度(度)
		void RotateX_Normal(float deg);

		//   Y回転をワールド合成(座標は変化させない)。
		//  	deg		… 回転角度(度)
		void RotateY_Normal(float deg);

		//   Z回転をワールド合成(座標は変化させない)。
		//  	Ang		… 回転角度(度)
		void RotateZ_Normal(float deg);

		//   指定軸回転をワールド合成(座標は変化させない)。
		//  	vAxis	… 回転軸
		//  	deg		… 回転角度(度)
		void RotateAxis_Normal(const ZVec3 &vAxis, float deg);

		//   X回転をローカル合成。
		//  	deg		… 回転角度(度)
		void RotateX_Local(float deg);

		//   Y回転をローカル合成。
		//  	deg		… 回転角度(度)
		void RotateY_Local(float deg);

		//   Z回転をローカル合成。
		//  	deg		… 回転角度(度)
		void RotateZ_Local(float deg);

		//   指定軸回転をローカル合成。
		//  	vAxis	… 回転軸
		//  	deg		… 回転角度(度)
		void RotateAxis_Local(const ZVec3 &vAxis, float deg);

		//   拡大をワールド合成。
		//  	x		… x拡大率
		//  	y		… y拡大率
		//  	z		… z拡大率
		void Scale(float x, float y, float z);

		//   拡大をワールド合成。
		//  	v		… 拡大率(x,y,z)
		void Scale(const ZVec3 &v);

		//   拡大をワールド合成。
		//		scale	… 拡大率
		void Scale(const float scale);

		//   拡大をローカル合成。
		//  	x		… x拡大率
		//  	y		… y拡大率
		//  	z		… z拡大率
		void Scale_Local(float x, float y, float z);

		//   拡大をローカル合成。
		//  	v		… 拡大率(x,y,z)
		void Scale_Local(const ZVec3 &v);

		//   拡大をワールド合成(座標は変化しません)。
		//  	x		… x拡大率
		//  	y		… y拡大率
		//  	z		… z拡大率
		void Scale_Normal(float x, float y, float z);

		//   拡大をワールド合成(座標は変化しません)。
		//  	v		… 拡大率(x,y,z)
		void Scale_Normal(const ZVec3 &v);

		//   Lookの方向に瞬時に向ける(相対的)。※合成系だけど、原点中心でなく、その場で回転します
		//  	Look	… 向きたい方向
		void RotateLookAtRelative(const ZVec3 &Look);

		//   Lookの方向にMaxAngの角度だけ向ける(相対的)。※合成系だけど、原点中心でなく、その場で回転します
		//  	Look	… 向きたい方向
		//  	MaxAng	… 最大角度
		void RotateLookAtRelativeAngle(const ZVec3 &Look,
									   float MaxAng);

		//   指定した行列をワールド合成(後ろに合成)
		//  	mat		… 合成する行列
		void Multiply(const ZMatrix &mat);
		void Multiply(const ZOperationalMatrix& mat);
		static void Multiply(ZMatrix &outMat, const ZMatrix &mat1, const ZMatrix &mat2);

		//   指定した行列をローカル合成(手前に合成)
		//  	mat		… 合成する行列
		void Multiply_Local(const ZMatrix &mat);
		void Multiply_Local(const ZOperationalMatrix& mat);

		//  指定した行列同士をワールド合成(ただしその場で回転する)
		void Multiply_NoMove(const ZMatrix &mat);
		static void Multiply_NoMove(ZMatrix &outMat, const ZMatrix &mat1, const ZMatrix &mat2);

		//========================================================================
		//
		//
		//
		// 行列「作成・操作」系関数･･･行列の内容を直接操作する関数(絶対的変化)
		//
		//
		//
		//========================================================================

		//   単位行列にする。
		void CreateIdentity();

		//   回転のみ単位行列にする。
		void CreateIdentityRotate();

		//   この行列を逆行列にする
		void Inverse();

		//   この行列を逆行列にしたものを、mOutに入れる
		//  @param[out]	mOut	… 逆行列にしたものを格納する行列
		void Inverse(ZMatrix &mOut) const;

		ZMatrix Inversed()const;

		//   指定行列を逆行列にしたものを、mOutに入れる
		//  @param[out]	mOut			… 逆行列にしたものを格納する変数
		//  @param[out]	pDeterminant	… 行列式をうけとる変数
		//  	mIn				… 逆行列にする行列
		static void Inverse(ZMatrix &mOut, const ZMatrix &mIn);

		//   この行列を転置する
		void Transpose();

		//   移動行列を作成
		//  	x	… x座標
		//  	y	… y座標
		//  	z	… z座標
		void CreateMove(float x, float y, float z);

		//   移動行列を作成
		//  	v	… 座標(x,y,z)
		void CreateMove(const ZVec3 &v);

		//   移動行列を作成しmOutに入れる
		//  @param[out]	mOut	… 受け取る行列
		//  	x		… x座標
		//  	y		… y座標
		//  	z		… z座標
		static void CreateMove(ZMatrix &mOut, float x, float y, float z);

		//   移動行列を作成しmOutに入れる
		//  @param[out]	mOut	… 受け取る行列
		//  	v		… 座標(x,y,z)
		static void CreateMove(ZMatrix &mOut, const ZVec3& v);

		static ZMatrix CreateMoveMat(const ZVec3& v);

		//   X回転行列を作成
		//  	Ang		… 回転角度(度)
		void CreateRotateX(float deg);

		//   X回転行列を作成を作成し、mOutに入れる
		//  @param[out]	mOut	… 受け取る行列
		//  	deg		… 回転角度(度)
		static void CreateRotateX(ZMatrix &mOut, float deg);

		//   Y回転行列を作成
		//  	deg		… 回転角度(度)
		void CreateRotateY(float deg);

		//   Y回転行列を作成を作成し、mOutに入れる
		//  @param[out]	mOut	… 受け取る行列
		//  	deg		… 回転角度(度)
		static void CreateRotateY(ZMatrix &mOut, float deg);

		//   Z回転行列を作成
		//  	deg		… 回転角度(度)
		void CreateRotateZ(float deg);

		//   Z回転行列を作成を作成し、mOutに入れる
		//  @param[out]	mOut	… 受け取る行列
		//  	deg		… 回転角度(度)
		static void CreateRotateZ(ZMatrix &mOut, float deg);

		//   任意軸回転行列を作成
		//  	vAxis	… 回転軸
		//  	deg		… 回転角度(度)
		void CreateRotateAxis(const ZVec3 &vAxis, float deg);

		//   任意軸回転行列を作成し、mOutに入れる
		//  @param[out]	mOut	… 受け取る行列
		//  	vAxis	… 回転軸
		//  	deg		… 回転角度(度)
		static void CreateRotateAxis(ZMatrix &mOut, const ZVec3 &vAxis, float deg);

		//   スケーリング行列を作成
		//  	x	… x拡大率
		//  	y	… y拡大率
		//  	z	… z拡大率
		void CreateScale(float x, float y, float z);

		//   スケーリング行列を作成
		//  	v	… 拡大率(x,y,z)
		void CreateScale(const ZVec3 &v);

		//   スケーリング行列を作成
		//  @param[out]	mOut	… 受け取る行列
		//  	x		… x拡大率
		//  	y		… y拡大率
		//  	z		… z拡大率
		static void CreateScale(ZMatrix &mOut, float x, float y, float z);

		//   スケーリング行列を作成
		//  @param[out]	mOut	… 受け取る行列
		//  	v		… 拡大率(x,y,z)
		static void CreateScale(ZMatrix &mOut, const ZVec3 &v);

		static ZMatrix CreateScaleMat(const ZVec3 &v);

		//   指定平面でミラー反転させるための行列を作成
		//   planePos		… 平面上の座標
		//   planeDir		… 平面の方向
		void CreateReflect(const ZVec3& planePos, const ZVec3& planeDir);

		//   拡大成分を正規化
		void NormalizeScale();

		//   指定方向にZ軸を向けた行列を作成。座標は現在のものが使われる
		//  	LookDir	… 向きたい方向
		//  	Up		… 上方向となるベクトル
		void SetLookTo(const ZVec3 &LookDir, const ZVec3 &Up);

		//   指定位置にZ軸を向けた行列を作成。座標も指定する。
		//  	MyPos	… 座標
		//  	LookDir	… 向きたい方向
		//  	Up		… 上方向となるベクトル
		void SetLookTo(const ZVec3 &MyPos, const ZVec3 &LookDir, const ZVec3 &Up)
		{
			SetPos(MyPos);
			SetLookTo(LookDir, Up);
		}

		//   現在の位置から指定位置にZ軸を向けた行列を作成。座標は変化せず、現在のものが使われる
		//  	TargetPos	… 向きたい位置
		//  	Up			… 上方向となるベクトル
		void SetLookAt(const ZVec3 &TargetPos, const ZVec3 &Up);

		//   現在の位置から指定位置にZ軸を向けた行列を作成。座標は変化せず、現在のものが使われる
		//  	MyPos		… 座標
		//  	TargetPos	… 向きたい位置
		//  	Up			… 上方向となるベクトル
		void SetLookAt(const ZVec3 &MyPos, const ZVec3 &TargetPos, const ZVec3 &Up)
		{
			SetPos(MyPos);
			SetLookAt(TargetPos, Up);
		}

		//   カメラの向きと同じ向きにする。座標はそのまま。拡大も維持
		//  	lpCamMat	… カメラ行列(ビュー行列ではない)
		void SetBillBoard(const ZMatrix &lpCamMat);

		//   Y軸は指定し、Z軸をカメラの方向に極力向けるような行列にする。レーザーのようなビルボード時に使う。
		//  	vCamPos	… カメラ座標
		//  	vY		… Y軸となるベクトル
		void SetBillBoardAxisY(const ZVec3 &vCamPos, const ZVec3 &vY);

		//   線形補間 m1〜m2の中間位置fの行列を求める。fは0〜1。
		//  単純な線形補間なので、キャラの回転とかに使用すると歪むので注意。
		//  @param[out]	mOut	… 補間結果が入る行列
		//  	m1		… 開始点の行列
		//  	m2		… 終了点の行列
		//  	f		… 補間位置(0〜1)
		static void Lerp(ZMatrix& mOut, const ZMatrix& m1, const ZMatrix& m2, float f);

		//   回転は球面線形補間、座標などは線形補間 m1〜m2の中間位置fの行列を求める。fは0〜1。
		//  @param[out]	mOut	… 補間結果が入る行列
		//  	m1		… 開始点の行列
		//  	m2		… 終了点の行列
		//  	f		… 補間位置(0〜1)
		static void Slerp(ZMatrix& mOut, const ZMatrix& m1, const ZMatrix& m2, float f);

		//   座標のみをセットする
		//  	fx	… x座標
		//  	fy	… y座標
		//  	fz	… z座標
		void SetPos(float fx, float fy, float fz)
		{
			_41 = fx;
			_42 = fy;
			_43 = fz;
		}

		//   座標のみをセットする
		//  	v	… 座標(x,y,z)
		void SetPos(const ZVec3 &v)
		{
			_41 = v.x;
			_42 = v.y;
			_43 = v.z;
		}

		//   座標のみをセットする
		//  	lpSrcMat	… 行列(座標のみ使用される)
		void SetPos(const ZMatrix &lpSrcMat)
		{
			_41 = lpSrcMat._41;
			_42 = lpSrcMat._42;
			_43 = lpSrcMat._43;
		}

		//   X軸のみセットする
		//  	fx	… ベクトルのx成分
		//  	fy	… ベクトルのy成分
		//  	fz	… ベクトルのz成分
		void SetXAxis(float fx, float fy, float fz)
		{
			_11 = fx;
			_12 = fy;
			_13 = fz;
		}
		//   X軸のみセットする
		//  	v	… X軸となるベクトル
		void SetXAxis(const ZVec3 &v)
		{
			_11 = v.x;
			_12 = v.y;
			_13 = v.z;
		}

		//   Y軸のみセットする
		//  	fx	… ベクトルのx成分
		//  	fy	… ベクトルのy成分
		//  	fz	… ベクトルのz成分
		void SetYAxis(float fx, float fy, float fz)
		{
			_21 = fx;
			_22 = fy;
			_23 = fz;
		}
		//   Y軸のみセットする
		//  	v	… Y軸となるベクトル
		void SetYAxis(const ZVec3 &v)
		{
			_21 = v.x;
			_22 = v.y;
			_23 = v.z;
		}

		//   Z軸のみセットする
		//  	fx	… ベクトルのx成分
		//  	fy	… ベクトルのy成分
		//  	fz	… ベクトルのz成分
		void SetZAxis(float fx, float fy, float fz)
		{
			_31 = fx;
			_32 = fy;
			_33 = fz;
		}
		//   Z軸のみセットする
		//  	v	… Z軸となるベクトル
		void SetZAxis(const ZVec3 &v)
		{
			_31 = v.x;
			_32 = v.y;
			_33 = v.z;
		}

		//   回転部分だけ、指定行列からコピーする
		//  	lpSrcMat	… コピー元の行列
		void SetRotation(const ZMatrix &lpSrcMat)
		{
			_11 = lpSrcMat._11;
			_12 = lpSrcMat._12;
			_13 = lpSrcMat._13;
			_21 = lpSrcMat._21;
			_22 = lpSrcMat._22;
			_23 = lpSrcMat._23;
			_31 = lpSrcMat._31;
			_32 = lpSrcMat._32;
			_33 = lpSrcMat._33;
		}

		//   回転部分だけ、指定クオータニオンからコピーする
		//  	lpSrcQuat	… コピー元のクオータニオン
		void SetRotation(const ZQuat &lpSrcQuat)
		{
			ZMatrix m;
			lpSrcQuat.ToMatrix(m);
			SetRotation(m);
		}

		//   拡大率を直接操作する
		//  	x	… x拡大率
		//  	y	… y拡大率
		//  	z	… z拡大率
		void SetScale(float x, float y, float z)
		{
			NormalizeScale();	// 一度、拡大成分正規化

			_11 *= x;
			_12 *= x;
			_13 *= x;
			_21 *= y;
			_22 *= y;
			_23 *= y;
			_31 *= z;
			_32 *= z;
			_33 *= z;
		}

		// 定数行列
		static const ZMatrix Identity;	// 単位行列

		//   拡大率を直接操作する
		//  	size	… 拡大率
		void SetScale(const ZVec3 &pSize)
		{
			SetScale(pSize.x, pSize.y, pSize.z);
		}

		//   拡大率を直接操作する
		//  	scale	… 拡大率
		void SetScale(const float scale)
		{
			SetScale(scale , scale, scale);
		}

		//   透視投影行列(射影行列)として作成
		void CreatePerspectiveFovLH(float angle, float aspect, float fNear, float fFar)
		{
			// 射影行列
			*this = DirectX::XMMatrixPerspectiveFovLH(
				ToRadian(angle),		// 視野角
				aspect,					// 画面のアスペクト比
				fNear,					// 最近接距離
				fFar);					// 最遠方距離
		}

		//   正射影行列(射影行列)として作成
		void CreateOrthoLH(float w, float h, float fNear, float fFar)
		{
			*this = DirectX::XMMatrixOrthographicLH(w, h, fNear, fFar);
		}

		//   ビュー行列として作成　注視点を指定するVer
		//   pos			… カメラ座標
		//   ratgetPos	… 注視点となる座標
		//   up			… 上(Y)となる方向
		void CreateViewMatrixPos(const ZVec3 &pos, const ZVec3 &targetPos, const ZVec3 &up = ZVec3(0, 1, 0))
		{
			*this = DirectX::XMMatrixLookAtLH(pos, targetPos, up);
		}

		//   ビュー行列として作成　カメラ方向を指定するVer
		//   pos	… カメラ座標
		//   dir	… カメラ方向
		//   up	… 上(Y)となる方向
		void CreateViewTransform(const ZVec3 &pos, const ZVec3 &dir, const ZVec3 &up = ZVec3(0, 1, 0))
		{
			*this = DirectX::XMMatrixLookToLH(pos, dir, up);
		}

		//========================================================================
		// 変換
		//========================================================================
		//   自分の行列の回転部分をクォータニオンへ変換
		//  ※行列に拡大成分が入っていると、変換がおかしくなる？
		//  @param[out]	out				… 変換されたクォータにオンを受け取る変数
		//   bNormalizeScale	… 行列を正規化するか。拡大成分が入った行列だと回転補間の時に影響が出る。
		void ToQuaternion(const ZQuat &out, bool bNormalizeScale = false) const;

		ZQuat ToQuaternion()const;

		//   指定行列から回転部分をクォータニオンへ変換
		//  ※行列に拡大成分が入っていると、変換がおかしくなる？
		//  @param[out]	out	… 変換されたクォータにオンを受け取る変数
		//  	mIn	… 元となる行列
		//   bNormalizeScale	… 行列を正規化する？
		static void ToQuaternion(const ZQuat &out, const ZMatrix &mIn, bool bNormalizeScale = false);

		//左手座標系と右手座標系の変換
		void MirrorZ();

		//   Json11データからセット [ _11, _12, _13, _14, _21, _22, _23, _24, _31, _32, _33, _34, _41, _42, _43, _44 ]の配列形式
		void Set(const json11::Json& jsonAry)
		{
			auto ary = jsonAry.array_items();
			if (ary.size() != 16)return;
			_11 = (float)ary[0].number_value();
			_12 = (float)ary[1].number_value();
			_13 = (float)ary[2].number_value();
			_14 = (float)ary[3].number_value();
			_21 = (float)ary[4].number_value();
			_22 = (float)ary[5].number_value();
			_23 = (float)ary[6].number_value();
			_24 = (float)ary[7].number_value();
			_31 = (float)ary[8].number_value();
			_32 = (float)ary[9].number_value();
			_33 = (float)ary[10].number_value();
			_34 = (float)ary[11].number_value();
			_41 = (float)ary[12].number_value();
			_42 = (float)ary[13].number_value();
			_43 = (float)ary[14].number_value();
			_44 = (float)ary[15].number_value();
		}


	};

	// Binary operators
	/*
	ZMatrix operator+ (const ZMatrix& M1, const ZMatrix& M2);
	ZMatrix operator- (const ZMatrix& M1, const ZMatrix& M2);
	ZMatrix operator* (const ZMatrix& M1, const ZMatrix& M2);
	ZMatrix operator* (const ZMatrix& M, float S);
	ZMatrix operator/ (const ZMatrix& M, float S);
	ZMatrix operator/ (const ZMatrix& M1, const ZMatrix& M2);
	// Element-wise divide
	ZMatrix operator* (float S, const ZMatrix& M);
	*/

}

#endif
