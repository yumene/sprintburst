//===============================================================
//
//  @file ZFont.h
//   高速フォントテクスチャ作成
// 
//  @author 鎌田
//
//===============================================================
#ifndef ZFont_h
#define ZFont_h

namespace EzLib
{

	class ZFontSpriteManager;

	//=====================================================================
	//   文字のデータ
	// 
	//  1文字のデータ
	// 
	//  @ingroup Graphics
	//=====================================================================
	struct ZFontData
	{
		sptr<ZTexture>		Tex;				// 文字テクスチャ

		char				Bytes = 1;			// 1:1バイト文字 2:2バイト文字
		uint16_t			Code = 0;			// 文字コード

		// @ brief テクスチャの使用容量取得
		UINT DEBUG_GetBytes()
		{
			if (Tex)
			{
				return Tex->GetInfo().Width* Tex->GetInfo().Height* 4;
			}
			return 0;
		}
	};

	//=====================================================================
	//   フォントスプライトクラス
	// 
	//  文字列をテクスチャとして生成することができる
	// 
	//  @ingroup Graphics_Important
	//=====================================================================
	class ZFontSprite
	{
		struct TexData;
	public:

		//   文字リスト取得
		std::vector<sptr<ZFontData>>&	GetTexList() { return m_TexList; }
		//   文字列の総幅を取得
		int								GetTotalWidth() { return m_TotalWidth; }
		//   文字列取得
		std::string						GetString() { return m_String; }

		//------------------------------------------------------------
		//   解放
		//------------------------------------------------------------
		void Release()
		{
			m_TexList.clear();

			m_TotalWidth = 0;
			m_String = "";
		}

		//------------------------------------------------------------
		//   文字列からフォントテクスチャリストを作成
		//   hdc			… fontが設定されているDCを指定。このfontが使用される。
		//   antiAliasing	… アンチエイリアシングフラグ 0:なし 1:2x 2:4x 3:8x
		//   pFontDataMap	… 作成したフォントデータの記憶/取得を行うMap。nullptrだと使用しない(必ずテクスチャ作成となる)
		//   bAdd			… データを追加する？
		//------------------------------------------------------------
		void CreateFontTexture(HDC hdc, const std::string& text, int antiAliasing = 0, std::array<sptr<ZFontData>, 65536>* pFontDataArray = nullptr, bool bAdd = false);

		// 
		ZFontSprite();
		// 
		~ZFontSprite()
		{
			Release();
		}


	private:

		std::vector<sptr<ZFontData>>	m_TexList;			// テクスチャリスト

		int								m_TotalWidth = 0;	// 文字列の総幅
		std::string						m_String;			// 文字列
		
	};

	//=====================================================================
	//   フォント管理クラス
	// 
	//  フォントの読み込みや、ZFontSpriteの生成を行う	\n
	//  フォントの作成を毎回行うとかなり処理が遅くなるので、一度作成したテクスチャを使い回せるようにしています
	// 
	//  @ingroup Graphics_Important
	//=====================================================================
	class ZFontSpriteManager
	{
	public:
		//------------------------------------------------------------
		//   初期設定
		//   hWnd		… ウィンドウのハンドル
		//------------------------------------------------------------
		void Init(HWND hWnd);

		//------------------------------------------------------------
		//   解放
		//------------------------------------------------------------
		void Release();

		//------------------------------------------------------------
		//   指定名のフォントを、指定番号へ登録する
		//   fontNo		… 登録するIndex番号
		//   fontName		… 追加するフォント名
		//   h			… フォントの大きさ(高さ)
		//------------------------------------------------------------
		void AddFont(int fontNo, const std::string& fontName, int h);

		//------------------------------------------------------------
		//   指定番号のフォントを使用し、文字列をフォントテクスチャとして生成する
		//   fontNo			… 使用する登録フォントのIndex番号
		//   text				… 文字列
		//   antiAliasingFlag	… アンチエイリアシングを行うか？ 0:なし 1:2x 2:4x 3:8x
		//  @return 生成されたテクスチャ情報
		//------------------------------------------------------------
		sptr<ZFontSprite> CreateFontTexture(int fontNo, const std::string& text, int antiAliasingFlag);

		//------------------------------------------------------------
		//   ttfファイルを読み込み、リソースとして登録する
		//------------------------------------------------------------
		static void sAddFontResource(const std::string& ttfFileName);

		//------------------------------------------------------------
		//   登録したttfフォントを全て解除する
		//------------------------------------------------------------
		static void sRemoveAllFontResource();

		// 
		~ZFontSpriteManager()
		{
			Release();
		}

		//   指定番号のフォントが使用している、総バイト数取得
		UINT DEBUG_GetFontBytes(int fontNo)
		{
			UINT totalByes = 0;
			auto& node = m_FontTbl[fontNo].CreatedFontDataTbl;
			for (auto& n : node)
			{
				totalByes += n->DEBUG_GetBytes();
			}
			return totalByes;
		}

	private:

		//   フォント登録データ
		struct FontData
		{
			HFONT hFont;												// フォントハンドル
			std::array<sptr<ZFontData>, 65536>			CreatedFontDataTbl;	// 作成済みフォントデータ配列
		};
		std::array<FontData, 10>	m_FontTbl;					// 登録されたフォントの配列

		HWND						m_hWnd = 0;					// ウィンドウハンドル
		HDC							m_hDC = 0;					// デバイスコンテキスト


		static std::map<std::string, int>	s_LoadedFontMap;	// 追加したttfファイルのリスト

	//==============================
	// シングルトン
	//==============================

	private:
		// 
		ZFontSpriteManager()
		{
		}

		static uptr<ZFontSpriteManager>	s_pInstance;
	public:
		// インスタンス作成
		static void CreateInstance()
		{
			DeleteInstance();
			s_pInstance = uptr<ZFontSpriteManager>(new ZFontSpriteManager());
		}
		// インスタンス削除
		static void DeleteInstance()
		{
			s_pInstance = nullptr;
		}
		// インスタンス取得
		static ZFontSpriteManager& GetInstance()
		{
			return *s_pInstance;
		}
	};

	//   フォント管理クラスのインスタンス取得
	#define ZFontMgr ZFontSpriteManager::GetInstance()

}


#endif
