#ifndef __MAIN_FRAME_H__
#define __MAIN_FRAME_H__

#include "EzLib.h"
#include "Utils/DebugLog.h"
#include "DebugWindow/DebugWindow.h"

using namespace EzLib;

#include "Game/System/Renderer/RendererBase.h"
#include "Game/System/Renderer/ModelRenderer/StaticMeshRenderer.h"
#include "Game/System/Renderer/ModelRenderer/SkinMeshRenderer.h"
#include "Game/System/Renderer/ModelRenderer/InstancingStaticMeshRenderer.h"
#include "Game/System/Renderer/ModelRenderer/DebugColliderMeshRenderer.h"

#include "Shader/ShaderManager.h"

#include "Input/ZInput.h"
#include "Game/System/Scene/Scene.h"

#include "Game/VR/VR.h"
#include "Window/ZWindow.h"

namespace EzLib
{
#ifdef SHOW_DEBUG_WINDOW
	static void DebugCommandProc(string cmtText);
#endif

	class ZMainFrame
	{
	public:
		ZMainFrame(const char* wndTitle, const ZWindowProperties& properties);
		virtual ~ZMainFrame();

		// コピー禁止
		ZMainFrame(ZMainFrame&) = delete;
		ZMainFrame& operator=(ZMainFrame&) = delete;

		void Start();

		void ExitGameLoop()
		{
			m_IsExitGameLoop = true;
		}

		// フレームレート設定
		void constexpr SetFrameRate(float rate)
		{
			m_FrameRate = rate;
			m_oneFrameParMilli = (1000.0ms / m_FrameRate);
			m_DeltaTime = 1.0f / m_FrameRate;
		}
		
		// フレームレート取得
		const float& GetFrameRate()const
		{
			return m_FrameRate;
		}

	protected:
		virtual bool Init();
		virtual void Run();	// メインループ開始
		virtual void FpsControll(ZTimer& stimer, chrono::duration<double>& sleepDurationTime);
		virtual bool ShowFps();
		virtual void Release();

	#pragma region Singleton

	public:
		inline static ZMainFrame& GetInstance()
		{
			return *m_sInstance;
		}

		static void DeleteInstance()
		{
			m_sInstance = nullptr;
		}

	#pragma endregion

	public:
		// 前フレームからの経過時間
		float m_DeltaTime;
		
		uptr<ZWindow>	m_Window;		// ウィンドウクラス

		ZMTRand m_Rand;					// 乱数生成
		
		ShaderManager m_ShaderMgr;		// シェーダーマネージャー

		SceneManager m_SceneMgr;		// シーン管理

		ZResourceStorage	m_ResStg;	// リソース管理庫

		// 物理ワールド(仮置き)
		ZPhysicsWorld m_PhysicsWorld;

		// その他

	protected:

		std::string			m_WndTitle;
		bool				m_IsFullScreen;
		float				m_FrameRate;
		bool				m_IsExitGameLoop;
		
		// FSP制御用
		chrono::duration<double> m_oneFrameParMilli;
		chrono::duration<double> m_DulationTime;
	
	private:
		ZWindowProperties m_WndProperties;

		static ZMainFrame* m_sInstance;

	};

}

#define APP EzLib::ZMainFrame::GetInstance()
#define ShMgr APP.m_ShaderMgr

#endif // MAIN_FRAME_H