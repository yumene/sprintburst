#include "MainFrame/ZMainFrame.h"
#include <iostream>
#include <thread>

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

namespace EzLib
{
	ZMainFrame* ZMainFrame::m_sInstance = nullptr;

	ZMainFrame::ZMainFrame(const char * wndTitle, const ZWindowProperties& properties)
		: m_FrameRate(60), m_IsExitGameLoop(false), m_WndTitle(wndTitle), m_WndProperties(properties)
	{
		m_oneFrameParMilli = (1000.0ms / m_FrameRate);
		m_DeltaTime = 1.0f / m_FrameRate;
		m_Rand.InitSeed(timeGetTime());
		m_sInstance = this;
	}

	ZMainFrame::~ZMainFrame()
	{
		if (m_IsFullScreen)
			ChangeDisplaySettings(NULL, 0);
		DeleteInstance();
	}

	void ZMainFrame::Start()
	{
		if (Init() == false)
			return;

		Run();
	}

	#ifdef SHOW_DEBUG_WINDOW

	void DebugCommandProc(string cmdText)
	{

	}

	#endif

	bool ZMainFrame::Init()
	{
		m_Window = std::make_unique<ZWindow>(m_WndTitle.c_str(), m_WndProperties);
		if (m_Window->IsInitialized() == false)
			return false;

		// DirectX初期化
		ZDirectXGraphics::CreateInstance();
		string errorMsg;
		if (ZDx.Init(m_Window->GetWindowHandle(), m_WndProperties.Width, m_WndProperties.Height, &m_ResStg, ZDirectXGraphics::MSAA::MSAA_NONE, &errorMsg) == false)
		{
			errorMsg = "DirectX初期化失敗 : " + errorMsg;
			MessageBox(m_Window->GetWindowHandle(), errorMsg.c_str(), "Error", MB_OK);
			return false;
		}
		ZDx.SetVSync(m_WndProperties.UseVSync);

		// フォント管理設定
		ZFontSpriteManager::CreateInstance();
		ZFontMgr.Init(m_Window->GetWindowHandle());
		// フォント追加
		ZFontMgr.AddFont(0, "ＭＳ ゴシック", 12);
		ZFontMgr.AddFont(1, "ＭＳ ゴシック", 24);
		ZFontMgr.AddFont(2, "ＭＳ ゴシック", 36);

		// シェーダー初期化
		ShMgr.Init();

		// サウンド初期化
		ZSndMgr.Init();

		//VRデバイス設定
		CVR::CreateInstance();
		cvr.Init();
		
		if (m_WndProperties.UseFullScreen)
			ZDx.SetFullScreen(true);

	#ifdef SHOW_DEBUG_WINDOW
		DEBUG_WINDOW.Init(m_Window->GetWindowHandle(), 4, DebugCommandProc);
	#endif

		// シーンマネージャー初期化
		m_SceneMgr.Init();

		// 物理ワールド初期化
		m_PhysicsWorld.Init();
		
		return true;
	}

	void ZMainFrame::Run()
	{
		// スリープ時間計測用
		chrono::duration<double> sleepDurationTime;

		//ZECS::Example();
	
		ZTimer timer(m_DulationTime);
		ZTimer stimer(sleepDurationTime); // スリープ時間計測

		// ケームループ
		while (m_IsExitGameLoop == false)
		{
			// ウィンドウメッセージ処理,入力情報取得
			m_Window->Update();
			// ゲーム処理
			timer.Start();
			// サウンド処理
			ZSndMgr.Update();

			// デバッグウィンドウ表示切り替え
			if (INPUT.KeyStay(VK_CONTROL) && INPUT.KeyEnter('D'))
				DEBUG_WINDOW.ToggleWindow();

			// Update~
			{
				m_SceneMgr.Update();
				
	#ifdef SHOW_DEBUG_WINDOW
				DEBUG_WINDOW.Update();
	#endif
			}

			// Draw~
			{
				// 描画前準備(バックバッファ& Zバッファクリア)
				// バックバッファをクリアする。
				ZDx.Begin(ZVec4(0.3f, 0.3f, 0.8f, 1.0f), true, true);

				m_SceneMgr.Draw();
				
	#ifdef SHOW_DEBUG_WINDOW
				DEBUG_WINDOW.Draw();
	#endif
				ZDx.End();
			}

			INPUT.SetMouseWheelValue(0);

			timer.Stop();

			#pragma region FPS Control
			
			FpsControll(stimer, sleepDurationTime);

			// 経過時間保持
			m_DeltaTime = (float)(m_DulationTime.count());

			#pragma endregion

			// FPS描画
			ShowFps();

			if (m_Window->IsClosed())
				ExitGameLoop();
		}

		Release();
	}

	void ZMainFrame::Release()
	{
		// フォント解放
		ZFontSpriteManager::DeleteInstance();
		ZFontSpriteManager::sRemoveAllFontResource(); // 読み込んだttfファイル削除

		// シーン解放
		m_SceneMgr.Release();
		// シェーダー解放
		ShMgr.Release();
		// リソース解放
		m_ResStg.Release();
		// サウンド解放
		ZSndMgr.Release();
		
		m_PhysicsWorld.Release();

		// デバッグ用GUI解放処理
		DEBUG_WINDOW.Release();

		//VRデバイス解放
		cvr.Release();
		CVR::DeleteInstance();

		// インスタンス削除
		ZDirectXGraphics::DeleteInstance();

		// デバッグウィンドウ終了
		#ifdef SHOW_DEBUG_WINDOW
		DEBUG_WINDOW.Close();
		#endif

	}

	void ZMainFrame::FpsControll(ZTimer& stimer, chrono::duration<double>& sleepDurationTime)
	{
		if (m_DulationTime >= m_oneFrameParMilli)
			return;

		// 処理時間が指定フレームレートの1フレームあたりの時間より小さければ
		
		// ミリ秒単位のスリープ
		stimer.Start();
		timeBeginPeriod(1);
		DWORD t = (DWORD)((m_oneFrameParMilli - m_DulationTime).count() * 1000);
		Sleep(t);
		timeEndPeriod(1);
		stimer.Stop();
		m_DulationTime += sleepDurationTime;

		// ↑のマイクロ秒単位の端数の時間分Sleep(0)をはさんでの待機ループ
		while (m_DulationTime < m_oneFrameParMilli)
		{
			stimer.Start();
			Sleep(0);
			stimer.Stop();
			m_DulationTime += sleepDurationTime;
		}
		
	}

	bool ZMainFrame::ShowFps()
	{
		static chrono::duration<double> fpsTimeCounter = 0ms;
		static float frameCnt = 0;

		frameCnt += 1;
		fpsTimeCounter += m_DulationTime;

		if (fpsTimeCounter >= 1000ms)
		{
			#ifdef SHOW_DEBUG_WINDOW
			DW_STATIC(2, "%.3f\n", 1.0f / (fpsTimeCounter.count() / frameCnt));
			DW_STATIC(3, "%3.0f Frame/s\n", frameCnt);

			#else
			DebugLog_fmt("%.3f\n", 1.0f / (fpsTimeCounter.count() / frameCnt));
			#endif

			fpsTimeCounter = 0ms;
			frameCnt = 0;
			return true;
		}

		return false;

	}


}