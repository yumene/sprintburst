//===============================================================
//  @file ZResourceStorage.h
//   リソース管理関係
// 
//  @author 鎌田
//===============================================================
#ifndef ZResourceStorage_h
#define ZResourceStorage_h

#include <map>

namespace EzLib
{

	class ZTexture;
	class ZTextureSet;
	class ZGameModel;
	class ZSoundData;

	//===================================================================================
	//   リソース倉庫
	// 
	// 	ZDataWeakStorage<ZIResource>を継承(FlyWeightパターン的な感じ)						\n
	// 		※これはZIResourceクラスを継承したクラスインスタンスを作成・保管できるクラス。	\n
	// 		　内部ではmapで登録しているので、重複読み込みが防げる。							\n
	// 		※戻り値のデータはsharedポインタとして返るので、sharedポインタの特性上、		\n
	// 		　解放処理は書かなくてよい。持ち主がいなくなった時点で自動で解放される。		\n
	// 
	//  @ingroup Etc
	//===================================================================================
	class ZResourceStorage : public ZDataWeakStorage<ZIResource, ZLock>
	{
	public:
		//====================================================================================================
		//   テスクチャを読み込みそれを返す。既に登録済みなら読み込みは行わず、読み込み済みのものを返す。
		//  詳細な読み込みの設定は、変数m_LoadTextureInfoを使用してるので、この変数を変更すると読み込みの細かな設定を変更できます
		//  	filename	… ファイル名
		//  	forceLoad	… trueだと既に読み込まれているいても無条件で再読み込みする
		//  @return テクスチャ
		//====================================================================================================
		sptr<ZTexture> LoadTexture(const std::string& filename, bool forceLoad = false);
		ZIMAGE_LOAD_INFO	m_LoadTextureInfo;		// LoadTexture関数で画像読み込み時の詳細設定　読み込みの細かな設定はこいつを変更すればOK

		//====================================================================================================
		//   テスクチャを読み込みそれを返す。既に登録済みなら読み込みは行わず、読み込み済みのものを返す。
		//  ZtextureSetバージョン
		//====================================================================================================
		sptr<ZTextureSet> LoadTextureSet(const std::string& filename, bool forceLoad = false)
		{
			auto ts = std::make_shared<ZTextureSet>();
			ts->LoadTextureSet(filename, forceLoad);
			return ts;
		}

		//====================================================================================================
		//   メッシュを読み込みそれを返す。既に登録済みなら読み込みは行わず、それを返す。
		// 	XファイルとXEDファイルに対応(スキンメッシュ対応)
		//  	filename	… ファイル名
		//  	bakeCurve	… 曲線補間系のアニメキーは、全て線形補間として変換・追加する(XED用 処理が軽くなります)
		//  	forceLoad	… trueだと既に読み込まれているいても無条件で再読み込みする
		//  @return ZGameModelモデル
		//====================================================================================================
		sptr<ZGameModel> LoadMesh(const std::string& filename, bool bakeCurve = true, bool forceLoad = false);

		//====================================================================================================
		//   音声データのを読み込みそれを返す。既に登録済みなら読み込みは行わず、それを返す。
		//  	filename	… ファイル名
		//  	forceLoad	… trueだと既に読み込まれているいても無条件で再読み込みする
		//  @return サウンドデータ
		//====================================================================================================
		sptr<ZSoundData> LoadSound(const std::string& filename, bool forceLoad = false);




		//====================================================================================================
		// 全リソース名を取得
		//====================================================================================================
		void DEBUG_GetNowResources(std::vector<std::string>& out)
		{
			auto dataMap = GetDataMap();
			auto it = dataMap.begin();
			while (it != dataMap.end())
			{
				if ((*it).second.expired() == false)
				{
					out.push_back((*it).second.lock()->GetTypeName());
				}

				++it;
			}
		}

		// 
		ZResourceStorage();
	};

}

#endif
