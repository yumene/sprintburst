//===============================================================
//  @file ZXEDLoader.h
//   XEDファイル読み込む
// 
//  XEDファイルは、KAMADA SkinMesh Editorで作成できるモデル形式です
// 
//  @author 鎌田
//===============================================================

#ifndef ZXEDLoader_h
#define ZXEDLoader_h

namespace EzLib
{

	//=================================================================================
	//   XEDファイル読み込みクラス
	// 
	//   fileName	… ファイルパス
	//  @param[out] outModelTbl	… 結果をいれる、メッシュ配列を指定
	//  @param[out] outBoneTree	… 結果をいれる、ボーン配列を指定
	//  @param[out] outAnimeTbl	… 結果をいれる、アニメデータ配列を指定
	//    bakeCurve	… 曲線補間系のアニメキーは、全て線形補間として変換・追加する(処理が軽くなります)
	// 
	//  @ingroup Graphics_Model
	//=================================================================================
	bool ZLoadXEDFile(
		const std::string& fileName,
		std::vector<sptr<ZSingleModel>>& outModelTbl,
		std::vector<sptr<ZGameModel::BoneNode>>& outBoneTree,
		std::vector<sptr<ZAnimationSet>>& outAnimeTbl,
		std::vector< ZGM_PhysicsDataSet >& outPhysicsTbl,
		//	std::vector< std::vector<ZXEDRigidBody> >&	outRigidBodyTbl,
		bool bakeCurve
	);


}

#endif
