//===================================================================
//
//
// ZDataWeakStorageテンプレートクラスのメンバ関数の実装部分
//
//
//===================================================================

template<typename T,typename LockType>
const int ZDataWeakStorage<T, LockType>::m_OptimizeCycle = 100; // 数値は適当

// データ追加
template<typename T, typename LockType>
template<typename X>
sptr<X> ZDataWeakStorage<T, LockType>::AddData_Type(const std::string& Name, bool* pbCreate)			// 生成
{

	// 定期的に不要ノードを消す
	m_Cnt++;
	if (m_Cnt % m_OptimizeCycle == 0)
		Optimize();

	sptr<X> lpData;
	std::string sKey = Name;

	// 排他制御
	LockGuard lock(m_Lock);

	//===============================
	// すでに登録されているかをCheck
	//===============================
	auto p = m_DataMap.find(sKey);
	if (m_DataMap.end() != p)
	{	// 存在
// 解放済みのものなら、１度mapから消す
		if (p->second.expired())
		{
			m_DataMap.erase(p);
		}
		// 存在するなら、それを返す
		else
		{
			if (pbCreate)*pbCreate = false;
			return sptr_dynamic_cast<X>(p->second.lock());
		}
	}

	//===========================
	// データを作成
	//===========================
	lpData = sptr<X>(new X());

	m_DataMap[sKey] = lpData;

	if (pbCreate)*pbCreate = true;

	return lpData;
}

// 指定名のデータ取得
template<typename T, typename LockType>
sptr<T> ZDataWeakStorage<T, LockType>::GetData(const std::string& Name)
{
	// 定期的に不要ノードを消す
	m_Cnt++;
	if (m_Cnt % m_OptimizeCycle == 0)
		Optimize();
	
	std::string sKey;
	if (Name == nullptr)
	{
		sKey = "";
	}
	else
	{
		sKey = Name;
	}

	// 排他制御
	LockGuard lock(m_Lock);

	// 存在確認
	auto p = m_DataMap.find(sKey);
	if (p == m_DataMap.end())return nullptr;

	// 破壊確認
	if (p->second.expired())
	{
		m_DataMap.erase(p);
		return nullptr;
	}

	// 存在するならreturn
	return p->second.lock();
}

// 指定キーのデータを管理マップから削除
template<typename T, typename LockType>
void ZDataWeakStorage<T, LockType>::DelData(const std::string& Name)
{
	// 排他制御
	{
		LockGuard lock(m_Lock);

		// 存在確認
		auto p= m_DataMap.find(Name);
		if (p == m_DataMap.end())return;	// 存在しないなら終了

											// 削除
		m_DataMap.erase(p);
	}

	Optimize();
}

// 解放
template<typename T, typename LockType>
void ZDataWeakStorage<T, LockType>::Release()
{
	// 排他制御
	LockGuard lock(m_Lock);

	auto p = m_DataMap.begin();
	while (p != m_DataMap.end())
	{
		p->second.reset();
		p = m_DataMap.erase(p);
	}
}

// 最適化
template<typename T, typename LockType>
void ZDataWeakStorage<T, LockType>::Optimize()
{
	// 排他制御
	LockGuard lock(m_Lock);

	auto p = m_DataMap.begin();
	while (p != m_DataMap.end())
	{
		if (p->second.expired())
			p = m_DataMap.erase(p);
		else
			++p;
	}
}
