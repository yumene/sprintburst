#include "EzLib.h"
#include "../EzLib/DebugWindow/DebugWindow.h"

using namespace EzLib;

ZResourceStorage::ZResourceStorage()
{
	// LoadTexture関数時の、読み込み詳細設定
	m_LoadTextureInfo.ShaderResource = true;
	m_LoadTextureInfo.RenderTarget = false;
	m_LoadTextureInfo.DepthStencil = false;
	m_LoadTextureInfo.MipLevels = 0;

}


sptr<ZTexture> ZResourceStorage::LoadTexture(const std::string& filename, bool forceLoad)
{
	// データ追加
	bool bCreate;
	sptr<ZTexture> pAdd = AddData_Type<ZTexture>(filename, &bCreate);

	// 新規生成時は読み込み　又は 強制指示時はは読み込み
	if (bCreate || forceLoad)
	{
		if (pAdd->LoadTextureEx(filename, &m_LoadTextureInfo) == false)
		{
			// 失敗時は空を返す
			pAdd = nullptr;
			Optimize();
		}
	}

	return pAdd;
}

sptr<ZGameModel> ZResourceStorage::LoadMesh(const std::string& filename, bool bakeCurve, bool forceLoad)
{
	// データ追加
	bool bCreate;
	sptr<ZGameModel> pAdd = AddData_Type<ZGameModel>(filename, &bCreate);

	// 新規生成時は読み込み　又は 強制指示時はは読み込み
	if (bCreate || forceLoad)
	{
		// 読み込み
		if (pAdd->LoadMesh(filename, bakeCurve) == false)
		{
			// 失敗時は空を返す
			pAdd = nullptr;
			Optimize();
		}
	}

	return pAdd;

}

sptr<ZSoundData> ZResourceStorage::LoadSound(const std::string& filename, bool forceLoad)
{
	// データ追加
	bool bCreate;
	sptr<ZSoundData> pAdd = AddData_Type<ZSoundData>(filename, &bCreate);

	// 新規生成時は読み込み　又は 強制指示時はは読み込み
	if (bCreate || forceLoad)
	{
		if (pAdd->LoadWaveFile(filename) == false)
		{
			// 失敗時は空を返す
			pAdd = nullptr;
			Optimize();
		}
	}

	return pAdd;
}
