#ifndef __ECS_H__
#define __ECS_H__

#include "EzLib.h"
#include "ECSCommon.h"
#include "ZComponentRefrection.h"

namespace EzLib
{
namespace ZECS
{
	class ECSEntity;

	class EntityComponentSystem
	{
	private:
		struct ECSThreadData
		{
			size_t size;
			std::atomic<size_t> nowIndex;
		};

		using UpdateComponentPackageList = std::vector<UpdateComponentPackage>;

	public:
		// コピー禁止
		EntityComponentSystem(EntityComponentSystem&) = delete;
		EntityComponentSystem& operator=(EntityComponentSystem&) = delete;

		void AddListener(ECSListener* listener);
		void RemoveListener(ECSListener* listener);
		void RemoveAllListener();

		#pragma region Entity Functions

		sptr<ECSEntity> MakeEntity(__ECSComponentBase** components, const uint32* componentIDs, size_t numComponents);
		template<typename ...Args>
		sptr<ECSEntity> MakeEntity(Args* ...args);

		void RemoveEntity(ECSEntity& handle);
		void RemoveAllEntity();
		
		uint32 HandleToEntityIndex(const EntityHandle handle)const;

		uint32 GetNumEntities()const;

		#pragma endregion

		#pragma region Component Functoins

		template<class Component>
		void AddComponent(const EntityHandle entity, const Component* component);

		template<class Component>
		bool RemoveComponent(const EntityHandle entity);

		template<class Component>
		Component* GetComponent(const EntityHandle entity);

		__ECSComponentBase* GetComponent(const EntityHandle entity, uint32 compID);

		template<class Component>
		Component* MakeComponent();

		template<class Class>
		void RegisterClassRefrection(const std::string& className);

		__ECSComponentBase* InstantiateComponent(const std::string& className);

		#pragma endregion

		#pragma region System Function

		void UpdateSystems(ECSSystemList& systems,const float delta,bool notUseLateUpdate = false);

		#pragma endregion

		void EnableUseMultiThread();

		void DisableUseMultiThread();

	#pragma region Singleton

	public:
		static EntityComponentSystem& GetInstance()
		{
			static EntityComponentSystem m_Instance;
			return m_Instance;
		}

	private:
		EntityComponentSystem();
		~EntityComponentSystem();

	#pragma endregion

	private:
		EntityCompsPair* HandleToRawType(const EntityHandle handle)const;
		
		EntityComps& HandleToEntityComps(const EntityHandle handle)const;

		void _RemoveEntityComps(const EntityHandle handle);
		void DeleteComponent(uint32 componentID, uint32 index);
		bool _RemoveComponent(EntityHandle componentID, uint32 index);
		void _AddComponent(sptr<ECSEntity> handle, EntityComps& entity, uint32 componentID,__ECSComponentBase* component);
		__ECSComponentBase* _GetComponent(EntityComps& entityComponents, ComponentMemory& compArray, uint32 componentID);

		UpdateComponentPackageList CollectComponentPackage(sptr<ECSSystemBase> system);
		void _UpdateSystem(sptr<ECSSystemBase> updateSystem, float delta, UpdateComponentPackageList& pacageList, bool isLateUpdate);

		uint32 FindLeastCommonComponent(const std::vector<uint32>& compTypes, const std::vector<uint32>& compFlags);

	private:
		ComponentMemoriesByID m_CompMemories; // id & components raw data
		std::vector<EntityCompsPair*> m_EntitieCompsPairList;
		std::vector<ECSListener*> m_ECSListeners;
		ZComponentRefrection m_CompRefrection; // コンポーネント用クラスリフレクション
		bool m_UseMultiThread;
		int m_NumCpu;

		std::unordered_map<sptr<ECSSystemBase>, UpdateComponentPackageList> m_SystemsUpdateParams;
		std::vector<std::future<void>> m_Futures;
		std::vector<std::future<UpdateComponentPackageList>> m_CollectSystemParamFutures;
		uptr<ZThreadPool> m_ThreadPool;

		std::mutex m_EntityMtx;
		std::mutex m_CompMtx;
		std::mutex m_ECSMtx;

	};

}
}

#include "ECSEntity.h"
#include "ECSComponent.h"
#include "ECSSystem.h"
#include "ECSListener.h"

namespace EzLib
{
namespace ZECS
{
#include "ECS.inl"
}
}

#endif