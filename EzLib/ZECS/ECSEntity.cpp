#include "ECSEntity.h"

namespace EzLib
{
namespace ZECS
{
	ECSEntity::~ECSEntity()
	{
		if (m_EntityHandle != NULL_ENTITY_HANDLE)
			ECS.RemoveEntity(*this);
	}

	void EzLib::ZECS::ECSEntity::RemoveAllEntity(std::vector<sptr<ECSEntity>>& entityList)
	{
		std::vector<sptr<ECSEntity>> removeTargetEntities;
		removeTargetEntities.reserve(entityList.size() / 2);
	
		// Handle��nullptr�̗v�f���폜
		for (size_t i = 0; i < entityList.size(); i++)
		{
			if (entityList[i]->m_EntityHandle != nullptr)
				removeTargetEntities.push_back(entityList[i]);
		}
	
		entityList.clear();
		entityList.shrink_to_fit();
	
		// �����\�[�g
		std::sort(removeTargetEntities.begin(), removeTargetEntities.end(),
			[](sptr<ECSEntity>& l, sptr<ECSEntity>& r)
		{
			const EntityHandle lHandle = l->GetEntityHandle();
			const EntityHandle rHandle = r->GetEntityHandle();
			return ECS.HandleToEntityIndex(lHandle) < ECS.HandleToEntityIndex(rHandle);
		});
	
		for (uint32 i = 0; i < removeTargetEntities.size(); i++)
			ECS.RemoveEntity(*removeTargetEntities[i].get());
	
		removeTargetEntities.clear();
		removeTargetEntities.shrink_to_fit();
	
	}

	void ECSEntity::Remove()
	{
		if (m_EntityHandle)
			ECS.RemoveEntity(*this);
	}

	void ECSEntity::SetEntityHandle(const EntityHandle handle)
	{
		m_EntityHandle = handle;
	}

	const EntityHandle ECSEntity::GetEntityHandle()const
	{
		return m_EntityHandle;
	}

}
}
