inline const std::vector<uint32>& ECSSystemBase::GetComponentTypes()const
{
	return m_ComponentTypes;
}

inline const std::vector<uint32>& ECSSystemBase::GetComponentFlags()const
{
	return m_ComponentFlags;
}

inline bool ECSSystemBase::IsActive()const
{
	if (m_IsEnable == false)
		return false;

	// コンポーネントを使用しないシステムなら無条件でtrue
	if (m_ComponentTypes.size() == 0)
		return true;

	for (uint32 i = 0; i < m_ComponentFlags.size(); i++)
	{
		if ((m_ComponentFlags[i] & ECSSystemBase::FLAG_OPTIONAL) == 0)
			return true;
	}

	return false;
}

inline bool ECSSystemBase::UseMultiThread()const
{
	return m_UseMultiThread;
}

inline std::string ECSSystemBase::GetSystemName()const
{
	return m_DebugSystemName;
}

template<class Component>
inline Component* ECSSystemBase::GetCompFromUpdateParam(UpdateCompParams comp)const
{
	if (comp == nullptr)
		return nullptr;

	__ECSComponentBase* component = nullptr;

	try
	{
		component = comp[m_UpdateCompIndexMap.at(Component::ID)];
	}
	catch (std::out_of_range&) // 例外(コンポーネントが登録されていなければ)
	{
		return nullptr;
	}

	return static_cast<Component*>(component);
}

inline bool ECSSystemList::AddSystem(sptr<ECSSystemBase> system)
{
	if (system->IsActive() == false)
		return false;

	m_Systems.push_back(system);
	return true;
}

template<typename T>
inline sptr<T> ECSSystemList::AddSystem()
{
	static_assert(is_base_of<ECSSystemBase, T>(), "this class is not system");
	sptr<T> system = make_shared<T>();
	m_Systems.push_back(system);
	return system;
}
