inline bool ECSListener::NotifyOnAllComponentOperations()
{
	return m_IsNotifyOnAllComponentOperations;
}

inline bool ECSListener::NotifyOnAllEntityOperations()
{
	return m_IsNotifyOnAllEntityOperations;
}