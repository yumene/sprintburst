#ifndef __ZECS_H__
#define __ZECS_H__

#include "ECS.h"

namespace EzLib
{
namespace ZECS
{
	void Example();
}
}

#ifndef ECS
#define ECS EzLib::ZECS::EntityComponentSystem::GetInstance()
#endif
#define DefComponent(x) struct x : public ECSComponentBase<x>

using namespace EzLib::ZECS;

// ※
// コンポーネントは自前でnewなどで作成せず、"ECS.MakeComponent<任意のコンポーネント>()" で作成すること
// (戻り値は指定したコンポーネントのポインタが返る)

#endif