inline void EntityComponentSystem::AddListener(ECSListener* listener)
{
	m_ECSListeners.push_back(listener);
}

inline void EntityComponentSystem::RemoveListener(ECSListener* listener)
{
	auto it = std::find(m_ECSListeners.begin(), m_ECSListeners.end(), listener);
	if (it == m_ECSListeners.end())
		return;

	m_ECSListeners.erase(it);
}

inline void EntityComponentSystem::RemoveAllListener()
{
	m_ECSListeners.clear();
}

inline sptr<ECSEntity> EntityComponentSystem::MakeEntity(__ECSComponentBase** components, const uint32* componentIDs, size_t numComponents)
{
	std::unique_lock<std::mutex> ul(m_EntityMtx);
	EntityCompsPair* entityHandle = new EntityCompsPair();
	sptr<ECSEntity> entity = std::make_shared<ECSEntity>((EntityHandle)entityHandle);

	for (uint32 i = 0; i < numComponents; i++)
	{
		if (__ECSComponentBase::IsTypeValid(componentIDs[i]) == false)
		{
			std::string errorMsg = "'" + std::to_string(componentIDs[i]) + "'" + " is not a valid component type";
			MessageBox(nullptr, errorMsg.c_str(), "ECS Error", MB_OK);
			delete entityHandle;
			return std::make_shared<ECSEntity>();
		}

		_AddComponent(entity, entityHandle->second, componentIDs[i], components[i]);
	}

	// 行列コンポーネントが追加されていなければ無条件で追加
	if (GetComponent<TransformComponent>(entity->GetEntityHandle()) == nullptr)
	{
		if (__ECSComponentBase::IsTypeValid(TransformComponent::ID) == false)
		{
			std::string errorMsg = "'" + std::to_string(TransformComponent::ID) + "'" + " is not a valid component type";
			MessageBox(nullptr, errorMsg.c_str(), "ECS Error", MB_OK);
			delete entityHandle;
			return std::make_shared<ECSEntity>();
		}

		_AddComponent(entity, entityHandle->second, TransformComponent::ID, MakeComponent<TransformComponent>());
		numComponents++;
	}

	entityHandle->first = m_EntitieCompsPairList.size();
	m_EntitieCompsPairList.push_back(entityHandle);

	// リスナーに通知
	for (auto& listener : m_ECSListeners)
	{
		bool isValid = true;
		if (listener->NotifyOnAllEntityOperations())
		{
			listener->OnMakeEntity(entityHandle);
			continue;
		}

		for (auto componentID : listener->GetComponentIDs())
		{
			bool hasComponent = false;
			for (uint32 i = 0; i < numComponents; i++)
			{
				if (listener->GetComponentIDs()[i] != componentIDs[i])
					continue;
				
				hasComponent = true;
				break;
			}
			if (hasComponent == false)
			{
				isValid = false;
				break;
			}
		}
		if (isValid)
			listener->OnMakeEntity(entityHandle);
	}

	return std::move(entity);
}

template<typename ...Args>
inline sptr<ECSEntity> EntityComponentSystem::MakeEntity(Args* ...args)
{
	// 引数で受け取ったパラメータ数を保持
	static constexpr size_t size = sizeof...(Args);
	__ECSComponentBase* Components[size] = { args... };
	const uint32 IDs[size] = { args->ID ... };

	return std::move(MakeEntity(Components, &IDs[0], size));
}

inline EntityCompsPair* EntityComponentSystem::HandleToRawType(const EntityHandle handle)const
{
	return (EntityCompsPair*)(handle);
}

inline uint32 EntityComponentSystem::HandleToEntityIndex(const EntityHandle handle)const
{
	return HandleToRawType(handle)->first;
}

inline uint32 EntityComponentSystem::GetNumEntities()const
{
	return m_EntitieCompsPairList.size();
}

inline EntityComps& EntityComponentSystem::HandleToEntityComps(const EntityHandle handle)const
{
	return HandleToRawType(handle)->second;
}

template<class Component>
inline void EntityComponentSystem::AddComponent(const EntityHandle entity,const Component* component)
{
	static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
	std::unique_lock<std::mutex> ul(m_CompMtx);

	_AddComponent(entity, HandleToEntityComps(entity), Component::ID, component);
	
	// リスナーに通知
	for (auto& listener : m_ECSListeners)
	{
		const std::vector<uint32>& componentIDs = listener->GetComponentIDs();
		for (auto compID : componentIDs)
		{
			if (listener->NotifyOnAllComponentOperations())
			{
				listener->OnAddComponent(entity, Component::ID);
				continue;
			}
			
			if (compID != Component::ID)
				continue;
			listener->OnAddComponent(entity, Component::ID);
			break;
			
		}
	}

}

inline void EntityComponentSystem::_AddComponent(sptr<ECSEntity> handle, EntityComps& entity, uint32 componentID, __ECSComponentBase* component)
{
	// 同じコンポーネントIDをもつコンポーネントをすでに持っているか
	auto* comp = GetComponent(handle->GetEntityHandle(), componentID);

	// 持っていなければ新規追加
	if (comp == nullptr)
		component->m_Entity = handle;
	// すでに持っていれば削除し、新規で受け取ったコンポーネントを追加
	else
	{
		_RemoveComponent(handle->GetEntityHandle(), componentID);
		component->m_Entity = handle;
	}

	entity[componentID] = component->m_MemoryIndex;

}

template<class Component>
inline bool EntityComponentSystem::RemoveComponent(const EntityHandle entity)
{
	static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
	std::unique_lock<std::mutex> ul(m_CompMtx);

	// リスナーに通知
	for (auto& listener : m_ECSListeners)
	{
		const std::vector<uint32>& componentIDs = listener->GetComponentIDs();
		for (auto compID : componentIDs)
		{
			if (listener->NotifyOnAllComponentOperations())
			{
				listener->OnRemoveComponent(entity, Component::ID);
				continue;
			}
			
			if (compID != Component::ID)
				continue;
			listener->OnRemoveComponent(entity, Component::ID);
			break;
		}
	}

	return _RemoveComponent(entity, Component::ID);
}

inline bool EntityComponentSystem::_RemoveComponent(EntityHandle handle, uint32 componentID)
{
	auto& entityComponents = HandleToEntityComps(handle);

	auto& it = entityComponents.find(componentID);

	if (it == entityComponents.end())
		return false;
	
	DeleteComponent((*it).first, (*it).second);
	entityComponents.erase(it);
	
	return true;
}

template<class Component>
inline Component* EntityComponentSystem::GetComponent(const EntityHandle entity)
{
	static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
	return (Component*)(_GetComponent(HandleToEntityComps(entity), m_CompMemories[Component::ID], Component::ID));
}

inline __ECSComponentBase* EntityComponentSystem::GetComponent(const EntityHandle entity, uint32 compID)
{
	return _GetComponent(HandleToEntityComps(entity), m_CompMemories[compID], compID);
}

inline __ECSComponentBase* EntityComponentSystem::_GetComponent(EntityComps& entityComponents, ComponentMemory& compArray, uint32 componentID)
{
	if (entityComponents.size() == 0)
		return nullptr;

	for(auto& it : entityComponents)
	{
		if(it.first == componentID)
			return reinterpret_cast<__ECSComponentBase*>(&compArray[it.second]);
	}

	return nullptr;
}

template<class Component>
inline Component* EntityComponentSystem::MakeComponent()
{
	static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
	std::unique_lock<std::mutex> ul(m_CompMtx);
	ECSComponentCreateFunction createFunc = __ECSComponentBase::GetTypeCreateFunction(Component::ID);
	Component* comp = static_cast<Component*>(createFunc(m_CompMemories[Component::ID], nullptr));
	return comp;
}

template<class Component>
inline void EntityComponentSystem::RegisterClassRefrection(const std::string& className)
{
	static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
	std::unique_lock<std::mutex> ul(m_CompMtx);
	m_CompRefrection.Register<Component>(className);
}

inline __ECSComponentBase* EntityComponentSystem::InstantiateComponent(const std::string& className)
{
	std::unique_lock<std::mutex> ul(m_CompMtx);
	__ECSComponentBase* comp = m_CompRefrection.New(className);
	return comp;
}