#ifndef __ECS_COMMON_H__
#define __ECS_COMMON_H__

namespace EzLib
{
namespace ZECS
{
	class ECSListener;
	class ECSEntity;
	class ECSSystemBase;
	class ECSSystemList;
	struct __ECSComponentBase;

	#pragma region EntityComponentSystemDefines

	using ComponentMemory = std::vector<uint8>;							// 1バイトの動的メモリ配列
	using ComponentMemoriesByID = std::map<uint32, ComponentMemory>;	// ID別のコンポーネントメモリ
	using EntityComps =  std::map<uint32, uint32>;						// コンポーネントID& コンポーネント用メモリのIndex
	using EntityCompsPair = std::pair<uint32, EntityComps>;				// index& Entity
	using ComponentArray = std::vector<ComponentMemory*>;				// 各コンポーネントごとの専用メモリ(混同させると管理が面倒)
	using UpdateComponentPackage = std::vector<__ECSComponentBase*>;	// Systemの渡す前のコンポーネント収集用コンテナ

	#pragma endregion

	#pragma region ComponentDefines

	using EntityHandle = void*;
	using ECSComponentCreateFunction = __ECSComponentBase*(*)(ComponentMemory&,sptr<ECSEntity>);
	using ECSComponentFreeFunction = void(*)(__ECSComponentBase*);
	
	#ifndef NULL_ENTITY_HANDLE
	#define NULL_ENTITY_HANDLE nullptr
	#endif

	#pragma endregion

	#pragma region SystemDefines

	using UpdateCompParams = __ECSComponentBase**;

	#pragma endregion
}
}


#endif