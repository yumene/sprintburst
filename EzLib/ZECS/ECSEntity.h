#ifndef __ECS_ENTITY_H__
#define __ECS_ENTITY_H__

#ifndef ECS
#define ECS EzLib::ZECS::EntityComponentSystem::GetInstance()
#endif

#include "ECSCommon.h"
#include "Utils/DebugLog.h"

namespace EzLib
{
namespace ZECS
{
	class ECSEntity
	{
	public:
		ECSEntity():m_EntityHandle(NULL_ENTITY_HANDLE)
		{
		}
		ECSEntity(EntityHandle handle) :m_EntityHandle(handle)
		{
		}
		ECSEntity(ECSEntity&) = delete;

		virtual ~ECSEntity();
		
		// ※ ECSではエンティティは生成順にvectorに追加され、
		//   エンティティ削除時はvectorの最後尾に移動させた後にpop_backしているので、
		//   vectorのclear関数で一括削除すると存在しないインデックスを参照してしまい、
		//   エラー落ちするので一括でエンティティを削除する際はECSEntityクラスに用意した
		//   専用の関数を使って削除する
		static void RemoveAllEntity(std::vector<sptr<ECSEntity>>& entityList);

		void Remove();

		// コンポーネント追加
		template<typename Component>
		void AddComponent();

		// コンポーネント取得
		template<typename Component>
		Component* GetComponent();
		
		// コンポーネント削除
		template<typename Component>
		void RemoveComponent();

		void SetEntityHandle(const EntityHandle handle);

		const EntityHandle GetEntityHandle()const;
		
	private:
		EntityHandle m_EntityHandle;
	};

}
}

namespace EzLib
{
namespace ZECS
{
#include "ECSEntity.inl"
}
}

#endif