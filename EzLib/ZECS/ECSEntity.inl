template<typename Component>
inline void ECSEntity::AddComponent()
{
	ECS.AddComponent(m_EntityHandle, ECS.MakeComponent<Component>());
}

template<typename Component>
inline Component* ECSEntity::GetComponent()
{
	return ECS.GetComponent<Component>(m_EntityHandle);
}

template<typename Component>
inline void ECSEntity::RemoveComponent()
{
	ECS.RemoveComponent<Component>(m_EntityHandle);
}