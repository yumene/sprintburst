#ifndef __ZCOMPONENT_REFRECTION_H__
#define __ZCOMPONENT_REFRECTION_H__

#include "ECSCommon.h"

namespace EzLib
{
namespace ZECS
{
	// クラスリフレクション
	// 指定した名前(文字列)から特定のクラスを生成する
	// ClassBase ... ベースの型 このクラスを継承したクラスのみを生成出来る
	class ZComponentRefrection
	{
	public:
		ZComponentRefrection(ComponentMemoriesByID* pCompMemories)
		{
			m_pCompMemories = pCompMemories;
		}

		// クラス登録
		template<class Component>
		void Register(const std::string& name)
		{
			static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");

			if (m_pCompMemories == nullptr)
				return;
			if (m_pCompPairMap == nullptr)
				return;

			// 生成関数作成
			auto instansiateFunc =
				[this]()
			{
				ECSComponentCreateFunction createFunc = __ECSComponentBase::GetTypeCreateFunction(Component::ID);
				return static_cast<Component*>(createFunc((*m_pCompMemories)[Component::ID], nullptr));
			};

			// 登録
			m_ReflectionMap.insert(std::pair<std::string, std::function<BaseClass*()>>(name, instansiateFunc));

		}

		// インスタンス生成
		__ECSComponentBase* New(const std::string& name)const
		{
			if (m_pCompMemories == nullptr)
				return nullptr;

			__ECSComponentBase* instance = nullptr;
			
			try
			{
				auto& instansiateFunc = m_ReflectionMap.at(name);
				instance = instansiateFunc();
			}
			catch (std::out_of_range&)
			{
				return instance;
			}

			// インスタンス生成関数実行
			return instance;
		}

		// 解放
		void Release()
		{
			m_pCompMemories = nullptr;
			m_ReflectionMap.clear();
		}

	private:
		// クラス登録用 連想配列
		//  <クラス名,生成関数オブジェクト>で登録
		std::unordered_map<std::string, std::function<__ECSComponentBase*()>> m_ReflectionMap;

		ComponentMemoriesByID* m_pCompMemories;
	};

}
}
#endif