#ifndef __ECS_SYSTEM_H__
#define __ECS_SYSTEM_H__

#include "EzLib.h"
#include "ECSCommon.h"

namespace EzLib
{
namespace ZECS
{
	class ECSSystemBase : public std::enable_shared_from_this<ECSSystemBase>
	{
	public:
		enum
		{
			FLAG_OPTIONAL = 1
		};

	public:
		ECSSystemBase() : m_UseMultiThread(false), m_IsEnable(true)
		{
		}
		ECSSystemBase(ECSSystemBase&) = delete;

		virtual void UpdateComponents(float delta, UpdateCompParams components)
		{
		}
		virtual void LateUpdateComponents(float delta, UpdateCompParams components)
		{
		}

		virtual void DebugImGuiRender();

		const std::vector<uint32>& GetComponentTypes()const;

		const std::vector<uint32>& GetComponentFlags()const;
		
		bool IsActive()const;

		bool UseMultiThread()const;

		std::string GetSystemName()const;
		
	protected:
		template<class T>
		void AddComponentType(uint32 compFlag = 0)
		{
			static_assert(is_base_of<__ECSComponentBase, T>(), "this class is not component");
			m_UpdateCompIndexMap.emplace(T::ID, m_ComponentTypes.size());
			m_ComponentTypes.push_back(T::ID);
			m_ComponentFlags.push_back(compFlag);
		}

		template<class Component>
		Component* GetCompFromUpdateParam(UpdateCompParams comp)const;

	protected:
		std::map<uint32, uint32> m_UpdateCompIndexMap;
		bool m_UseMultiThread;
		std::string m_DebugSystemName;
		bool m_IsEnable;

	private:
		std::vector<uint32> m_ComponentTypes;
		std::vector<uint32> m_ComponentFlags;

	};

	class ECSSystemList
	{
	public:
		~ECSSystemList()
		{
			Release();
		}

		bool AddSystem(sptr<ECSSystemBase> system);

		template<typename T>
		sptr<T> AddSystem();
		
		bool RemoveSystem(sptr<ECSSystemBase> system);

		void Release()
		{
			m_Systems.clear();
		}

		size_t Size()
		{
			return m_Systems.size();
		}

		auto begin()
		{
			return m_Systems.begin();
		}

		auto end()
		{
			return m_Systems.end();
		}

		sptr<ECSSystemBase> operator[](uint32 index)
		{
			return m_Systems[index];
		}

	private:
		std::vector<sptr<ECSSystemBase>> m_Systems;

	};

}
}

namespace EzLib
{
namespace ZECS
{
#include "ECSSystem.inl"
}
}

#endif