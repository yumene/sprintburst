#include "ECSListener.h"

namespace EzLib
{
namespace ZECS
{
	ECSListener::ECSListener()
		: m_IsNotifyOnAllComponentOperations(false),
		  m_IsNotifyOnAllEntityOperations(false)
	{
	}

	ECSListener::~ECSListener()
	{
	}

	void ECSListener::OnMakeEntity(EntityHandle handle)
	{
	}
	
	void ECSListener::OnRemoveEntity(EntityHandle hadle)
	{
	}
	
	void ECSListener::OnAddComponent(EntityHandle handle, uint32 id)
	{
	}

	void ECSListener::OnRemoveComponent(EntityHandle handle, uint32 id)
	{
	}

	void ECSListener::SetNotificationSettings(bool notifyOnAllComponentOperations,
											  bool notifyOnAllEntityOperations)
	{
		m_IsNotifyOnAllComponentOperations = notifyOnAllComponentOperations;
		m_IsNotifyOnAllEntityOperations = notifyOnAllEntityOperations;
	}
	
	void ECSListener::AddComponentID(uint32 id)
	{
		m_ComponentIDs.push_back(id);
	}

	const std::vector<uint32>& ECSListener::GetComponentIDs()
	{
		return m_ComponentIDs;
	}

}
}