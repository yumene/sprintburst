#include "ZECS.h"

namespace EzLib
{
namespace ZECS
{
	EntityComponentSystem::EntityComponentSystem()
		: m_CompRefrection(&m_CompMemories),
		m_UseMultiThread(false)
	{
		SYSTEM_INFO info;
		GetSystemInfo(&info);
		m_NumCpu = (int)info.dwNumberOfProcessors;
		m_ThreadPool = std::make_unique<ZThreadPool>(m_NumCpu);

		m_Futures.resize(m_ThreadPool->GetNumThreads());
		m_CollectSystemParamFutures.resize(m_ThreadPool->GetNumThreads());
	}

	EntityComponentSystem::~EntityComponentSystem()
	{
		m_CompRefrection.Release();
		RemoveAllEntity();
		m_CompMemories.clear();

		__ECSComponentBase::Release();
	}

	void EntityComponentSystem::RemoveEntity(ECSEntity& entity)
	{
		std::unique_lock<std::mutex> ul(m_EntityMtx);
		_RemoveEntityComps(entity.GetEntityHandle());
		entity.SetEntityHandle(NULL_ENTITY_HANDLE);
	}

	void EntityComponentSystem::RemoveAllEntity()
	{
		std::unique_lock<std::mutex> ul(m_EntityMtx);
		for(uint32 i = m_EntitieCompsPairList.size()-1;m_EntitieCompsPairList.size() > 0; i = m_EntitieCompsPairList.size() - 1)
			_RemoveEntityComps((EntityHandle)m_EntitieCompsPairList[i]);
		
		m_EntitieCompsPairList.resize(0);
	}

	void EntityComponentSystem::UpdateSystems(ECSSystemList& systems,const float delta, bool notUseLateUpdate)
	{
		std::unique_lock<std::mutex> ul(m_ECSMtx);
		
		// systemごとのコンポーネントパッケージの収集(マルチスレッド)
		{
			ECSThreadData data;
			data.size = systems.Size();

			auto execThread = [this,&systems,&data](size_t index)
			{
				if (index >= data.size)
					return UpdateComponentPackageList();

				auto system = systems[index];
				return CollectComponentPackage(system);
			};

			auto& futures = m_CollectSystemParamFutures;
	
			if(futures.size() < data.size)
				futures.resize(data.size);

			for (size_t i = 0; i < data.size; i++)
			{
				if (systems[i]->IsActive() == false)
					continue;

				futures[i] = m_ThreadPool->AddTask([this,execThread,i]()
				{
					return execThread(i);
				});
			}

			for (size_t i = 0;i< data.size; i++)
			{
				if (systems[i]->IsActive() == false)
					continue;

				m_SystemsUpdateParams[systems[i]] = std::move(futures[i].get());
			}
		}
		
		for (uint32 updateCnt = 0; updateCnt < 2; updateCnt++)
		{
			if(updateCnt == 1 && notUseLateUpdate == true)
				return;

			for (uint32 updateIndex = 0; updateIndex < systems.Size(); updateIndex++)
			{
				if (systems[updateIndex]->IsActive() == false)
					continue;

				auto& compTypes = systems[updateIndex]->GetComponentTypes();
				
				// 要求されたコンポーネントが0個でも更新
				if(compTypes.size() == 0)
				{
					if (updateCnt == 0)
						systems[updateIndex]->UpdateComponents(delta, nullptr);
					else
						systems[updateIndex]->LateUpdateComponents(delta, nullptr);
					continue;
				}

				bool isLateUpdate = (updateCnt == 1);
				auto& pacageList = m_SystemsUpdateParams[systems[updateIndex]];
				_UpdateSystem(systems[updateIndex], delta, pacageList, isLateUpdate);
			
			}	//	for (uint32 updateIndex = 0; updateIndex < systems.Size(); updateIndex++)
		
		} // for (uint32 updateCnt = 0; updateCnt < 2; updateCnt++)

		for (auto& it : m_SystemsUpdateParams)
			it.second.clear();

	}

	void EntityComponentSystem::EnableUseMultiThread()
	{
		std::unique_lock<std::mutex> ul(m_ECSMtx);
		m_UseMultiThread = true;
	}

	void EntityComponentSystem::DisableUseMultiThread()
	{
		std::unique_lock<std::mutex> ul(m_ECSMtx);
		m_UseMultiThread = false;
	}

	void EntityComponentSystem::_RemoveEntityComps(const EntityHandle handle)
	{
		if (handle == NULL_ENTITY_HANDLE)
			return;
		
		auto& entityComps = HandleToEntityComps(handle);

		// リスナーに通知
		for (auto& listener : m_ECSListeners)
		{
			bool isValid = true;
			
			if (listener->NotifyOnAllEntityOperations())
			{
				listener->OnRemoveEntity(handle);
				continue;
			}

			for (auto componentID : listener->GetComponentIDs())
			{
				bool hasComponent = false;
				for (uint32 i = 0; i < entityComps.size(); i++)
				{
					if (listener->GetComponentIDs()[i] != entityComps[i])
						continue;
					
					hasComponent = true;
					break;
				}

				if (hasComponent == false)
				{
					isValid = false;
					break;
				}
			}
			if (isValid)
				listener->OnRemoveEntity(handle);
		}

		for(auto comp : entityComps)
			DeleteComponent(comp.first, comp.second);

		uint32 destIndex = HandleToEntityIndex(handle);
		uint32 srcIndex = m_EntitieCompsPairList.size() - 1;
		delete m_EntitieCompsPairList[destIndex];
		
		m_EntitieCompsPairList[destIndex] = m_EntitieCompsPairList[srcIndex];
		m_EntitieCompsPairList.pop_back();

		if(destIndex < m_EntitieCompsPairList.size())
			m_EntitieCompsPairList[destIndex]->first = destIndex;
		
	}

	void EntityComponentSystem::DeleteComponent(uint32 componentID,uint32 index)
	{
		ComponentMemory& compArray = m_CompMemories[componentID];
		ECSComponentFreeFunction freeFunc = __ECSComponentBase::GetTypeFreeFunction(componentID);
		size_t typeSize = __ECSComponentBase::GetTypeSize(componentID);
		uint32 srcIndex = compArray.size() - typeSize;

		auto* destComponent = (__ECSComponentBase*)(&compArray[index]);
		auto* srcComponent = (__ECSComponentBase*)(&compArray[srcIndex]);
		freeFunc(destComponent);
		
 		if(index == srcIndex)
		{
			compArray.resize(srcIndex);
			return;
		}
		memcpy(destComponent, srcComponent, typeSize);

		// 一応登録されているかチェック
		srcComponent->m_MemoryIndex = index;

		auto& srcComponents = HandleToEntityComps(srcComponent->m_Entity->GetEntityHandle());
		
		auto& it = srcComponents.find(componentID);

		if (it != srcComponents.end() && (*it).second == srcIndex)
			srcComponents[componentID] = index;

		compArray.resize(srcIndex);
	}

	EntityComponentSystem::UpdateComponentPackageList EntityComponentSystem::CollectComponentPackage(sptr<ECSSystemBase> system)
	{
		UpdateComponentPackageList packageList;
		auto compTypes = system->GetComponentTypes();

		if (compTypes.size() == 0)
			return std::move(packageList);
		// 単一のコンポーネントなら
		else if (compTypes.size() == 1)
		{
			size_t typeSize = __ECSComponentBase::GetTypeSize(compTypes[0]);
			ComponentMemory& compMemArray = m_CompMemories[compTypes[0]];
			size_t numComps = compMemArray.size() / typeSize;
			packageList.resize(numComps);
			for (uint32 i = 0; i < compMemArray.size(); i += typeSize)
			{
				size_t entityIndex = i / typeSize;
				__ECSComponentBase* comp = (__ECSComponentBase*)&compMemArray[i];
				packageList[entityIndex].push_back(comp);
			}
			return std::move(packageList);
		}
		// 複数のコンポーネントなら
		else if(compTypes.size() > 1)
		{
			// systemのコンポーネントフラグ取得
			const std::vector<uint32>& compFlags = system->GetComponentFlags();

			ComponentArray compMemArray(compTypes.size());
			for (uint32 i = 0; i < compTypes.size(); i++)
				compMemArray[i] = &m_CompMemories[compTypes[i]];

			// 要求された中で一番数の少ないコンポーネントタイプのインデックスを取得
			uint32 minSizeIndex = FindLeastCommonComponent(compTypes, compFlags);

			size_t typeSize = __ECSComponentBase::GetTypeSize(compTypes[minSizeIndex]);
			ComponentMemory& compArray = *compMemArray[minSizeIndex]; // 一番少ないコンポーネントの配列
			size_t numConpnents = compArray.size() / typeSize;
			packageList.resize(numConpnents);

			for (size_t i = 0; i < numConpnents; i++)
				packageList[i].resize(compTypes.size());

			for (uint32 i = 0; i < compArray.size(); i += typeSize)
			{
				size_t entityIndex = i / typeSize;
				packageList[entityIndex][minSizeIndex] = (__ECSComponentBase*)(&(compArray[i]));

				// コンポーネントからEntity取得
				auto& entityComponents = HandleToEntityComps(packageList[entityIndex][minSizeIndex]->m_Entity->GetEntityHandle());

				// 取得したEntityから残りのコンポーネント取得
				bool isFindComponents = true;
				for (uint32 j = 0; j < compTypes.size(); j++)
				{
					if (j == minSizeIndex)
						continue;

					packageList[entityIndex][j] = _GetComponent(entityComponents, *compMemArray[j], compTypes[j]);
					if (packageList[entityIndex][j] == nullptr && (compFlags[j] & ECSSystemBase::FLAG_OPTIONAL) == 0)
					{
						isFindComponents = false;
						break;
					}
				}

				if (isFindComponents == false)
					continue;

			} // for (uint32 i = 0; i < compArray.size(); i += typeSize)

		} // else if(compTypes.size() > 1)

		return std::move(packageList);
	}

	void EntityComponentSystem::_UpdateSystem(sptr<ECSSystemBase> updateSystem, float delta, UpdateComponentPackageList& pacageList, bool isLateUpdate)
	{
		// ECS自体、またはシステムのマルチスレッドでの更新が有効でなければシングルスレッドで更新
		if (m_UseMultiThread == false || updateSystem->UseMultiThread() == false)
		{
			for (auto& compPackage : pacageList)
			{
				if (isLateUpdate == false)
					updateSystem->UpdateComponents(delta, &compPackage[0]);
				else
					updateSystem->LateUpdateComponents(delta, &compPackage[0]);
			}

			return;
		}

		// ECS自体とシステムのマルチスレッドでの更新が有効であればマルチスレッドで更新(テスト段階)
		ECSThreadData data;
		data.size = pacageList.size();
		data.nowIndex = 0;
		
		auto execThread = [this, &data, &pacageList, isLateUpdate, updateSystem, delta]()
		{
			while (true)
			{
				size_t index = data.nowIndex.fetch_add(1);
				
				if (index >= data.size)
					break;

				if (isLateUpdate == false)
					updateSystem->UpdateComponents(delta, &pacageList[index][0]);
				else
					updateSystem->LateUpdateComponents(delta, &pacageList[index][0]);
			}
		};

		// 並列処理
		{
			auto numThreads = m_ThreadPool->GetNumThreads();
			std::vector<std::future<void>> futures;

			for (size_t i = 0; i < numThreads; i++)
				m_Futures[i] = m_ThreadPool->AddTask(execThread);

			for (size_t i = 0; i < numThreads; i++)
				m_Futures[i].wait();
		}

	}

	uint32 EntityComponentSystem::FindLeastCommonComponent(const std::vector<uint32>& compTypes, const std::vector<uint32>& compFlags)
	{
		// 指定されたコンポーネント一覧の中で一番保持している数が少ないコンポーネントのIDを取得
		
		uint32 minSize = (uint32)-1; // オーバーフロー -> uint32の最大値
		uint32 minIndex = (uint32)-1; // オーバーフロー -> uint32の最大値

		for(uint32 i = 0;i<compTypes.size();i++)
		{
			if ((compFlags[i]& ECSSystemBase::FLAG_OPTIONAL) != 0)
				continue;

			size_t typeSize = __ECSComponentBase::GetTypeSize(compTypes[i]);
			uint32 size = m_CompMemories[compTypes[i]].size()/ typeSize;
			if (size > minSize)
				continue;
			minSize = size;
			minIndex = i;
		}

		return minIndex;
	}

}
}