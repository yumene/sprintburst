#ifndef __DEBUG_WINDOW_H__
#define __DEBUG_WINDOW_H__

#include <time.h>
#include <windows.h>
#include <map>
#include <string>
#include <queue>
#include <thread>
#include <mutex>
#include "Utils/DebugLog.h"

using namespace EzLib;
using namespace std;

#ifdef SHOW_DEBUG_WINDOW

class DebugWindow
{
public:
	static const int SCROLL_WINDOW_MAX = 6;
	static const int STATIC_MESSAGE_MAX = 100;

	struct DebugMessage
	{
		enum FLAGS
		{
			STATIC = 0,
			SCROLL,
			CLEAR_SCROLL,
		};
		FLAGS m_Flag;
		int m_Index;
		string m_Message;
		static const size_t NUM_FLAGS = 3;
	};

public:
	bool Init(HWND hWnd, int scrollWindowNum, void(*commandProc)(string));
	
	void Open()
	{
		if (m_Initialized == false)
			return;
		m_IsOpen = true;
	}

	void Close()
	{
		m_IsOpen = false;
	}

	void ToggleWindow()
	{
		m_IsOpen ^= true;
	}

	void Release();

	void Update();

	void Draw(sptr<ZTexture> renderTarget = sptr<ZTexture>());

	void AddCommandQueue(const string& cmd)
	{
		std::unique_lock<std::mutex> ul(m_CommandMutex);
		m_CommandQueue.push(cmd);
	}

	void AddImGuiFunction(const std::function<void()>& func)
	{
		std::unique_lock<std::mutex> ul(m_ImGuiMutex);
		m_ImGuiFunctions.push_back(func);
	}

	template<typename... Args>
	void SetStaticMessage(int index, const char* format, Args... args)
	{
		if (m_IsOpen == false)
			return;
		if (index <= 0 || index >= STATIC_MESSAGE_MAX)
			return;
		
		index--;

		char tmpMsg[1024];
		sprintf_s(tmpMsg, 1022, format, args...);

		DebugMessage message;
		message.m_Flag = DebugMessage::STATIC;
		message.m_Index = index;
		message.m_Message = tmpMsg;

		std::unique_lock<std::mutex> ul(m_MsgMutex);
		m_MessageBuffer[DebugMessage::STATIC].push_back(message);
	}

	template<typename... Args>
	void SetScrollMessage(int no, const char* format, Args... args)
	{
		if (m_IsOpen == false)
			return;
		if (no < 0 || no >= m_NumScrollWindow)
			return;

		char tmpMsg[1024];
		sprintf_s(tmpMsg, 1022, format, args...);
		std::string msg = tmpMsg;
		int pos = msg.find_last_of('\n');
		if (pos == -1)
			msg += '\n';

		DebugMessage message;
		message.m_Flag = DebugMessage::SCROLL;
		message.m_Index = no;
		message.m_Message = std::move(msg);

		std::unique_lock<std::mutex> ul(m_MsgMutex);
		m_MessageBuffer[DebugMessage::SCROLL].push_back(message);
	}

	void SetScrollMessage(int no, const string& msg)
	{
		if (m_IsOpen == false)
			return;
		if (no < 0 || no >= m_NumScrollWindow)
			return;

		DebugMessage message;
		message.m_Flag = DebugMessage::SCROLL;
		message.m_Index = no;
		message.m_Message = std::move(msg);
		int pos = message.m_Message.find_last_of('\n');
		if (pos == -1)
			message.m_Message += '\n';

		std::unique_lock<std::mutex> ul(m_MsgMutex);
		m_MessageBuffer[DebugMessage::SCROLL].push_back(message);
	}

	void ClearStaticMessage()
	{
		if (m_IsOpen == false)
			return;

		for (int i = 0; i<STATIC_MESSAGE_MAX; i++)
		{
			DebugMessage message;
			message.m_Flag = DebugMessage::STATIC;
			message.m_Index = i;
			message.m_Message = "";

			std::unique_lock<std::mutex> ul(m_MsgMutex);
			m_MessageBuffer[DebugMessage::STATIC].push_back(message);
		}

	}

	void ClearScrollMessage(int no)
	{
		if (m_IsOpen == false)
			return;

		if (no < 0 || no >= m_NumScrollWindow)
			return;
		
		DebugMessage message;
		message.m_Flag = DebugMessage::CLEAR_SCROLL;
		message.m_Index = no;
		message.m_Message = "";
		
		std::unique_lock<std::mutex> ul(m_MsgMutex);
		m_MessageBuffer[DebugMessage::SCROLL].push_back(message);
	}

	void TimeBegin(const string& str)
	{
		m_TimeMutex.lock();
		auto p = m_TimeMap.find(str);

		if (p != m_TimeMap.end())
			p->second.m_StartTime = timeGetTime();
		else
		{
			TimeData time;
			time.m_StartTime = timeGetTime();
			m_TimeMap.insert(pair<string, TimeData>(str, time));
		}
	}

	DWORD TimeEnd(const string& str, int staticLineNO)
	{
		DWORD endTime = timeGetTime();
		DWORD startTime;
		DWORD durationTime;
		string messag;

		bool isFind = false;
		{
			std::unique_lock<std::mutex> ul(m_TimeMutex);
			auto p = m_TimeMap.find(str);
			if (p != m_TimeMap.end())
			{
				p->second.m_EndTime = endTime;
				startTime = p->second.m_StartTime;
				messag = p->first;
				m_TimeMap.erase(p);
				isFind = true;
			}
		}

		durationTime = endTime - startTime;

		if (isFind)
		{
			if (staticLineNO >= 0)
				SetStaticMessage(staticLineNO, "%s:%03d min", str.c_str(), durationTime);
		}

		return durationTime;
	}

private:
	void GUIUpdate();
	void GUIDraw();

#pragma region Singleton
public:
	static DebugWindow& GetInstance()
	{
		static DebugWindow instance;
		return instance;
	}

private:
	DebugWindow() : m_IsOpen(false), m_Initialized(false)
	{
	}

#pragma endregion

private:
	size_t NumUpdateMessagePerFrame;
	size_t MaxScrollWindowMessage;

	std::queue<string> m_CommandQueue;
	std::function<void(string)> m_CommandProc;
	
	std::string m_LineIndexNumbers[STATIC_MESSAGE_MAX];
	std::vector<std::vector<string>> m_ScrollMsgBufferList;
	std::string m_StaticMessages[STATIC_MESSAGE_MAX];
	int m_NumScrollWindow;

	std::array<std::vector<DebugMessage>, DebugMessage::NUM_FLAGS-1> m_MessageBuffer;

	std::vector<std::function<void()>> m_ImGuiFunctions;

	bool m_IsOpen;
	bool m_Initialized;

	struct TimeData
	{
		DWORD m_StartTime, m_EndTime;
	};
	std::map<string, TimeData> m_TimeMap;
	std::mutex m_TimeMutex;
	std::mutex m_MsgMutex;
	std::mutex m_CommandMutex;
	std::mutex m_ImGuiMutex;
};

#endif

#ifdef SHOW_DEBUG_WINDOW

#define DEBUG_WINDOW DebugWindow::GetInstance()
#define DW_STATIC DEBUG_WINDOW.SetStaticMessage
#define DW_SCROLL DEBUG_WINDOW.SetScrollMessage
#define DW_IMGUI_FUNC DEBUG_WINDOW.AddImGuiFunction
#define DW_STATIC_CREAR DEBUG_WINDOW.ClearStaticMessage
#define DW_SCROLL_CREAR DEBUG_WINDOW.ClearScrollMessage
#define DW_TIME_BEGIN DEBUG_WINDOW.TimeBegin
#define DW_TIME_End DEBUG_WINDOW.TimeEnd

#else
#define DEBUG_WINDOW
#define DW_STATIC(x)
#define DW_SCROLL(x)
#define DW_STATIC_CREAR(x)
#define DW_SCROLL_CREAR(x)
#define DW_TIME_BEGIN(x)
#define DW_TIME_End(x)

#endif


#endif