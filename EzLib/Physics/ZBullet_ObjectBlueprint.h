#ifndef __ZBULLET_OBJECT_BLUEPRINT_H__
#define __ZBULLET_OBJECT_BLUEPRINT_H__

namespace EzLib
{
	struct ZBP_RigidBody
	{
	public:
		// 0... 球
		// 1... 箱
		// 2... カプセル
		enum class shape : uint8
		{
			Sphere,
			Box,
			Capsule
		};
	
	public:
		uint8 Group;
		uint16 UnCollisionGroup;
	
		shape Shape;
		ZVec3 ShapeSize;
		ZVec3 Translate;
		ZVec3 Rotate; // ラジアン表現
	
		float Mass;
		float TranslateDimmer;
		float RotateDimmer;
		float Repulsion;
		float Friction;
	};
	
	struct ZBP_Joint
	{
	public:
		// 0... バネ付き6DoF
		// 1... 6DoF
		// 2... P2P
		// 3... ConeTwist
		// 4... Slider
		// 5... Hinge
		enum class JointType : uint8
		{
			SpringDoF6,
			DoF6,
			P2P,
			ConeTwist,
			Slider,
			Hinge
		};
	
	public:
		JointType Type;
		ZVec3 Translate;
		ZVec3 Rotate;
		ZVec3 TranslateLowerLimit;		// 移動制限-下限
		ZVec3 TranslateUpperLimit;		// 移動制限-上限
		ZVec3 RotateLowerLimit;			// 回転制限-下限 -> ラジアン角
		ZVec3 RotateUpperLimit;			// 回転制限-上限 -> ラジアン角
		ZVec3 SpringTranslateFactor;	// バネ定数-移動
		ZVec3 SpringRotateFactor;		// バネ定数-回転
		
		// ヒンジジョイント用
		ZVec3 HingeAxisA;
		ZVec3 HingeAxisB;
	
	};
}

#endif