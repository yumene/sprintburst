//===============================================================
//  @file ZVertexShader.h
//   頂点シェーダクラス
// 
//  @author 鎌田
//===============================================================
#ifndef ZVertexShader_h
#define ZVertexShader_h

namespace EzLib
{

	//=======================================================================
	//   頂点シェーダクラス
	// 
	//  [主な機能]								\n
	// 	・HLSLファイルから頂点シェーダ作成		\n
	// 	・頂点レイアウト作成					\n
	// 	・それらをデバイスにセットする機能		\n
	// 
	//  @ingroup Graphics_Shader
	//=======================================================================
	class ZVertexShader
	{
	public:

		//--------------------------------------------------------------------------
		//   コンパイル済みシェーダファイル(.cso)を読み込み、シェーダオブジェクトを作成する
		//   csoFileName		… CSOファイルパス
		//   vertexTypeData	… 頂点レイアウト用のデータ　頂点シェーダの入力にあった形式を渡すこと
		//  @return 成功:true
		//--------------------------------------------------------------------------
		bool LoadShader(const std::string& csoFileName, const ZVertexTypeData& vertexTypeData);

		//--------------------------------------------------------------------------
		//   HLSLソースファイル(.hlsl)を読み込み、頂点シェーダ・頂点レイアウトを作成する。
		//   hlslFileName		… HLSLファイルパス
		//   VsFuncName		… HLSL内の頂点シェーダ関数名
		//   shaderModel		… 使用するシェーダモデルを指定 例)"vs_5_0"
		//   shaderFlag		… 追加のシェーダコンパイルフラグ
		//   vertexTypeData	… 頂点レイアウト用のデータ　頂点シェーダの入力にあった形式を渡すこと
		//  @return 成功:true
		//--------------------------------------------------------------------------
		bool LoadShaderFromFile(const std::string& hlslFileName, const std::string& VsFuncName, const std::string& shaderModel, UINT shaderFlag, const ZVertexTypeData& vertexTypeData);

		//--------------------------------------------------------------------------
		//   この頂点シェーダ用の頂点レイアウトを作成する。Create()した後に呼ぶこと。
		//   vertexTypeData	… 頂点レイアウト用のデータ　頂点シェーダの入力にあった形式を渡すこと
		//  @return 成功：作成されたID3D11InputLayoutのアドレス ※使わなくなったらRelease()を呼ぶこと
		//--------------------------------------------------------------------------
		ID3D11InputLayout* CreateVertexLayout(const ZVertexTypeData& vertexTypeData);

		//--------------------------------------------------------------------------
		//   コンパイル済みシェーダをファイルに保存する
		//  Create()時に指定した、[hlslFileName]_[VsFuncName].cso のファイル名で保存される
		//   filename		… 保存するファイルパス
		//  @return 成功:true
		//--------------------------------------------------------------------------
		bool SaveToFile();

		//--------------------------------------------------------------------------
		//   解放
		//--------------------------------------------------------------------------
		void Release();

		//--------------------------------------------------------------------------
		//   シェーダをデバイスにセット(頂点レイアウトもセット)
		//   vertexLayout		… 頂点レイアウト　NULLだとInit()時に指定したものが使用される
		//--------------------------------------------------------------------------
		void SetShader(ID3D11InputLayout* vertexLayout = nullptr);

		//--------------------------------------------------------------------------
		//   頂点シェーダを無効にする
		//  SetShader()で最後設定すると有効になる
		//--------------------------------------------------------------------------
		static void DisableShader();

		//
		ZVertexShader();
		~ZVertexShader()
		{
			Release();
		}

	private:
		ID3D11VertexShader* m_VS;				// 頂点シェーダー
		ID3D11InputLayout*	m_VLayout;			// 頂点インプットレイアウト

		ID3DBlob*			m_pCompiledShader;	// コンパイル済みシェーダデータ

		std::string			m_hlslFileName;		// HLSLファイル名
		std::string			m_csoFileName;		// コンパイル済みシェーダ(CSO)時のファイル名

	private:
		// コピー禁止用
		ZVertexShader(const ZVertexShader& src) {}
		void operator=(const ZVertexShader& src) {}
	};



}

#endif
