//===============================================================
//  @file ZVerteType.h
//   頂点形式データ集
// 
//  @author 鎌田
//===============================================================
#ifndef ZVertexType_h
#define ZVertexType_h

namespace EzLib
{

	//  @addtogroup Graphics_Model
	//  @{

	//===============================================
	//   頂点レイアウト用データ
	//  １つの頂点データの詳細を入れる
	//===============================================
	struct ZVertexTypeData
	{
		int								ByteSize;		// １頂点のバイト数

		int								Num;			// 頂点レイアウトの要素数
		const D3D11_INPUT_ELEMENT_DESC*	pLayout;		// 頂点レイアウトデータ(何バイト目に座標があって、何バイト目に法線があって…っというデータ)

		//   指定セマンティクス存在チェック
		//   semanticName … 検索したセマンティクス名
		//  @return	存在しない場合はnullptr　存在する場合はそのデータのアドレスが返る
		const D3D11_INPUT_ELEMENT_DESC* FindSemantics(const std::string& semanticName) const
		{
			for (int i = 0; i < Num; i++)
			{
				if (semanticName == pLayout[i].SemanticName)
				{
					return &pLayout[i];
				}
			}
			return nullptr;

		}
	};

	//===============================================
	// 頂点データ
	//===============================================

	//   頂点データ(座標のみ)
	//  これが基本の頂点データクラスになる
	struct ZVertex_Pos
	{
		ZVec3 Pos;	// 位置

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			// 頂点のレイアウト
			static const D3D11_INPUT_ELEMENT_DESC layout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};
			// データ
			static const ZVertexTypeData vtd = {
				sizeof(ZVertex_Pos),		// 型
				1,							// レイアウトの個数
				layout						// レイアウト
			};
			return vtd;
		}

	};


	//   頂点データ(座標,色)
	struct ZVertex_Pos_Color : public ZVertex_Pos
	{
		//	ZVec3 Pos;		// 位置
		ZVec4 Color;	// 色

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			// 頂点のレイアウト
			static const D3D11_INPUT_ELEMENT_DESC layout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
			};
			// データ
			static const ZVertexTypeData vtd = {
				sizeof(ZVertex_Pos_Color),	// 型
				2,							// レイアウトの個数
				layout						// レイアウト
			};
			return vtd;
		}

		ZVertex_Pos_Color() {}

		ZVertex_Pos_Color(const ZVec3& pos, const ZVec4& color)
		{
			Pos = pos;
			Color = color;
		}

	};


	//   頂点データ(座標,UV)
	struct ZVertex_Pos_UV : public ZVertex_Pos
	{
		//	ZVec3 Pos;		// 位置
		ZVec2 UV;		// UV

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			static const D3D11_INPUT_ELEMENT_DESC layout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,	0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,		0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
			};
			// データ
			static const ZVertexTypeData vtd = {
				sizeof(ZVertex_Pos_UV),
				2,
				layout
			};
			return vtd;
		}

		ZVertex_Pos_UV() {}

		ZVertex_Pos_UV(const ZVec3& pos, const ZVec2& uv)
		{
			Pos = pos;
			UV = uv;
		}
	};

	//   頂点データ(座標,UV,Color)
	struct ZVertex_Pos_UV_Color : public ZVertex_Pos
	{
		//	ZVec3 Pos;		// 位置
		ZVec2 UV;		// UV
		ZVec4 Color;	// 色

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			static const D3D11_INPUT_ELEMENT_DESC layout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,	0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,		0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "COLOR",	  0, DXGI_FORMAT_R32G32B32A32_FLOAT,0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
			};
			// データ
			static const ZVertexTypeData vtd = {
				sizeof(ZVertex_Pos_UV_Color),
				3,
				layout
			};
			return vtd;
		}

		ZVertex_Pos_UV_Color() {}

		ZVertex_Pos_UV_Color(const ZVec3& pos, const ZVec2& uv, const ZVec4& color)
		{
			Pos = pos;
			UV = uv;
			Color = color;
		}
	};


	//   頂点データ(座標,UV,法線)
	struct ZVertex_Pos_UV_Normal : public ZVertex_Pos
	{
		//	ZVec3 Pos;			// 位置
		ZVec2 UV;			// UV
		ZVec3 Normal;		// 法線

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			static const D3D11_INPUT_ELEMENT_DESC layout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,		0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,			0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,		0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};
			// データ
			static const ZVertexTypeData vtd = {
				sizeof(ZVertex_Pos_UV_Normal),
				3,
				layout
			};
			return vtd;
		}

		ZVertex_Pos_UV_Normal() {}

		ZVertex_Pos_UV_Normal(const ZVec3& pos, const ZVec2& uv, const ZVec3& normal)
		{
			Pos = pos;
			UV = uv;
			Normal = normal;
		}
	};


	//   頂点データ(座標,UV,法線マップ用法線)
	//  ZGameModelクラスで読み込んだモデル(Static mesh)は、この形式です
	struct ZVertex_Pos_UV_TBN : public ZVertex_Pos
	{
		//	ZVec3 Pos;			// 位置
		ZVec2 UV;			// UV
		ZVec3 Tangent;		// 接線
		ZVec3 Binormal;	// 従法線
		ZVec3 Normal;		// 法線

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			static const D3D11_INPUT_ELEMENT_DESC layout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,		0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,			0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TANGENT",   0, DXGI_FORMAT_R32G32B32_FLOAT,		0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "BINORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,		0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,		0, 44, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};
			// データ
			static const ZVertexTypeData vtd = {
				sizeof(ZVertex_Pos_UV_TBN),
				5,
				layout
			};
			return vtd;
		}
	};

	//   インスタンシング描画用頂点データ(座標,UV,法線マップ用法線)
	struct ZVertex_Pos_UV_TBN_Instancing : public ZVertex_Pos
	{
		//	ZVec3 Pos;			// 位置
		ZVec2 UV;			// UV
		ZVec3 Tangent;		// 接線
		ZVec3 Binormal;	// 従法線
		ZVec3 Normal;		// 法線
		
		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			static const D3D11_INPUT_ELEMENT_DESC layout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,	0,  0, D3D11_INPUT_PER_VERTEX_DATA,		0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,		0, 12, D3D11_INPUT_PER_VERTEX_DATA,		0 },
				{ "TANGENT",  0, DXGI_FORMAT_R32G32B32_FLOAT,	0, 20, D3D11_INPUT_PER_VERTEX_DATA,		0 },
				{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT,	0, 32, D3D11_INPUT_PER_VERTEX_DATA,		0 },
				{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,	0, 44, D3D11_INPUT_PER_VERTEX_DATA,		0 },
			
				{ "MATRIX",   0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1,  0, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
				{ "MATRIX",   1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
				{ "MATRIX",   2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
				{ "MATRIX",   3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			};

			// データ
			static const ZVertexTypeData vtd = {
				sizeof(ZVertex_Pos_UV_TBN_Instancing),
				9,
				layout
			};
			return vtd;
		}
	};

	//   頂点データ(座標,UV,法線マップ用法線,頂点ブレンド情報)
	//  ZGameModelクラスで読み込んだモデル(Skin mesh)は、この形式です
	struct ZVertex_Pos_UV_TBN_Skin : public ZVertex_Pos_UV_TBN
	{
		//	ZVec3 Pos;			// 位置
		//	ZVec2 UV;			// UV
		//	ZVec3 Tangent;		// 接線
		//	ZVec3 Binormal;	// 従法線
		//	ZVec3 Normal;		// 法線

		uint8_t				BlendWeight[4];		// ブレンドの重み
		unsigned short		BlendIndices[4];	// ボーン番号

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			static const D3D11_INPUT_ELEMENT_DESC layout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,		0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,			0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TANGENT",   0, DXGI_FORMAT_R32G32B32_FLOAT,		0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "BINORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,		0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,		0, 44, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "BLENDWEIGHT", 0, DXGI_FORMAT_R8G8B8A8_UNORM,		0, 56, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "BLENDINDICES", 0, DXGI_FORMAT_R16G16B16A16_UINT,	0, 60, D3D11_INPUT_PER_VERTEX_DATA, 0 },

				//			{ "BLENDWEIGHT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 56, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				//			{ "BLENDINDICES", 0, DXGI_FORMAT_R16G16B16A16_UINT,	0, 72, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};
			// データ
			static const ZVertexTypeData vtd = {
				sizeof(ZVertex_Pos_UV_TBN_Skin),
				7,
				layout
			};
			return vtd;
		}

	};

	//  @}

}
#endif
