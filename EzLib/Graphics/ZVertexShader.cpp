#include "EzLib.h"
#include "ZVertexShader.h"

using namespace EzLib;

bool ZVertexShader::LoadShader(const std::string& csoFileName, const ZVertexTypeData& vertexTypeData)
{
	Release();

	m_csoFileName = csoFileName;

	std::ifstream ifs(csoFileName, std::ios::binary);
	if (!ifs.fail())
	{
		// ファイルサイズ
		ifs.seekg(0, std::ios::end);
		int len = (int)ifs.tellg();
		ifs.seekg(0, std::ios::beg);

		// Blob(バッファ)作成
		D3DCreateBlob(len, &m_pCompiledShader);
		// 読み込み
		ifs.read((char*)m_pCompiledShader->GetBufferPointer(), len);
	}
	else
	{
		MessageBox(0, "指定したcsoファイルが存在しません", csoFileName.c_str(), MB_OK);
		Release();
		return false;
	}

	//==========================================================================
	// ブロブから頂点シェーダー作成
	//==========================================================================
	if (FAILED(ZDx.GetDev()->CreateVertexShader(m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize(), nullptr, &m_VS)))
	{
		MessageBox(0, "頂点シェーダー作成失敗", csoFileName.c_str(), MB_OK);
		Release();
		return false;
	}

	//==========================================================================
	// 頂点インプットレイアウトを作成
	//==========================================================================
	m_VLayout = CreateVertexLayout(vertexTypeData);
	if (m_VLayout == nullptr)
	{
		MessageBox(0, "頂点インプットレイアウト作成失敗", csoFileName.c_str(), MB_OK);
		Release();
		return false;
	}

	return false;
}

bool ZVertexShader::LoadShaderFromFile(const std::string& hlslFileName, const std::string& VsFuncName, const std::string& shaderModel, UINT shaderFlag, const ZVertexTypeData& vertexTypeData)
{
	Release();

	// 最適化
	#ifdef OPTIMIZESHADER
	shaderFlag |= D3DCOMPILE_OPTIMIZATION_LEVEL3;	// 最大の最適化 リリース用
	#else
	shaderFlag |= D3DCOMPILE_OPTIMIZATION_LEVEL0 | D3DCOMPILE_DEBUG;	// 最低の最適化 開発用
	#endif

	ID3DBlob* pErrors = nullptr;

	m_hlslFileName = hlslFileName;

	// csoファイル名生成( [hlslFileName]_[VsFuncName].cso )
	// 例) aaa.hlslを指定した場合で頂点シェーダ関数名が"VS"の時、aaa_VS.cso
	char szDir[_MAX_PATH];
	char szFname[_MAX_FNAME];
	char szExt[_MAX_EXT];
	_splitpath_s(hlslFileName.c_str(), nullptr, 0, szDir, _MAX_PATH, szFname, _MAX_FNAME, szExt, _MAX_EXT);

	m_csoFileName = szDir;
	m_csoFileName += szFname;
	m_csoFileName += "_";
	m_csoFileName += VsFuncName;
	m_csoFileName += ".cso";


	//==========================================================================
	// HLSLから頂点シェーダをコンパイルし読み込む
	//==========================================================================
	if (m_pCompiledShader == nullptr)
	{
		if (FAILED(D3DCompileFromFile(ConvertStringToWString(hlslFileName).c_str(), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, VsFuncName.c_str(), shaderModel.c_str(), shaderFlag, 0, &m_pCompiledShader, &pErrors)))
		{
			if (pErrors)
			{
				MessageBox(0, (char*)pErrors->GetBufferPointer(), "HLSLコンパイル失敗", MB_OK);
			}
			else
			{
				MessageBox(0, "HLSLファイルが存在しない", hlslFileName.c_str(), MB_OK);
			}
			Release();
			return false;
		}
		Safe_Release(pErrors);
	}

	//==========================================================================
	// ブロブから頂点シェーダー作成
	//==========================================================================
	if (FAILED(ZDx.GetDev()->CreateVertexShader(m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize(), nullptr, &m_VS)))
	{
		MessageBox(0, "頂点シェーダー作成失敗", hlslFileName.c_str(), MB_OK);
		Release();
		return false;
	}

	//==========================================================================
	// 頂点インプットレイアウトを作成
	//==========================================================================
	m_VLayout = CreateVertexLayout(vertexTypeData);
	if (m_VLayout == nullptr)
	{
		MessageBox(0, "頂点インプットレイアウト作成失敗", hlslFileName.c_str(), MB_OK);
		Release();
		return false;
	}

	return true;
}

ID3D11InputLayout* ZVertexShader::CreateVertexLayout(const ZVertexTypeData& vertexTypeData)
{
	ID3D11InputLayout* layout = nullptr;

	//==========================================================================
	// 頂点インプットレイアウトを作成
	//==========================================================================
	if (FAILED(ZDx.GetDev()->CreateInputLayout(vertexTypeData.pLayout,
		vertexTypeData.Num,
		m_pCompiledShader->GetBufferPointer(),
		m_pCompiledShader->GetBufferSize(),
		&layout)))
	{
		return nullptr;
	}

	return layout;
}

bool ZVertexShader::SaveToFile()
{
	if (m_pCompiledShader == nullptr)return false;

	//==========================================================================
	// コンパイル済みのシェーダを保存する
	//==========================================================================
	std::ofstream ofs(m_csoFileName, std::ios::binary);
	ofs.write((char*)m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize());

	return true;
}


void ZVertexShader::Release()
{
	Safe_Release(m_pCompiledShader);
	Safe_Release(m_VS);
	Safe_Release(m_VLayout);

	m_hlslFileName = "";
	m_csoFileName = "";
}

void ZVertexShader::SetShader(ID3D11InputLayout* vertexLayout)
{
	if (vertexLayout == nullptr)vertexLayout = m_VLayout;

	// 頂点入力レイアウト設定
	ZDx.GetDevContext()->IASetInputLayout(vertexLayout);
	// 頂点シェーダ設定
	ZDx.GetDevContext()->VSSetShader(m_VS, nullptr, 0);
}

void ZVertexShader::DisableShader()
{
	// 頂点シェーダ解除
	ZDx.GetDevContext()->VSSetShader(nullptr, 0, 0);
}

ZVertexShader::ZVertexShader() : m_VS(0), m_VLayout(0), m_pCompiledShader(0)
{
}