//===============================================================
//  @file ZBoneController.h
//   モデルデータのボーンを操作するクラス
// 
//  @author 鎌田
//===============================================================
#ifndef ZBoneController_h
#define ZBoneController_h

namespace EzLib
{
	class ZIKSolver;

	class ZPhysicsShape_Base;
	class ZPhysicsRigidBody;
	class ZPhysicsJoint_Base;
	class ZPhysicsWorld;

	//===========================================================================================
	// 
	//   モデルデータのボーンを操作するクラス
	// 
	//  ZGameModelクラスを参照し、その情報を元にボーンを動作させるクラス				\n
	//  [主な機能]																		\n
	//  ・渡されたZGameModelと同じボーン構造を作成										\n
	//  ・アニメーションを行いたい場合は、ZAnimatorクラスを別途使用する				\n
	//  ・全ボーンのLocal行列をTrans行列を元に計算										\n
	//  ・シェーダで使用するボーンの変換行列用の定数バッファの作成・書き込み			\n
	// 
	//  @ingroup Graphics_Model_Important
	//===========================================================================================
	class ZBoneController
	{
	private:
		struct cbBones;
	public:
		class BoneNode;
		class ZBCRigidBody;
		
		struct ZPhysicsObjectSet
		{
			std::vector<sptr<ZBCRigidBody>> RigidBodys;
			std::vector<sptr<ZPhysicsJoint_Base>> Joints;
		};

		//=================================================================================
		// 情報取得
		//=================================================================================

		//   登録されているゲームモデルクラス取得
		sptr<ZGameModel>					GetGameModel() { return m_pGameModel; }

		//   ルートボーン取得　ボーンがない場合はnullptr
		sptr<BoneNode>						GetRootBone() { return m_BoneTree.size() == 0 ? nullptr : m_BoneTree[0]; }

		//   ボーンテーブル取得
		std::vector<sptr<BoneNode>>&		GetBoneTree() { return m_BoneTree; }

		//	ソート済みボーンテーブル取得
		std::vector<sptr<BoneNode>>&		GetSortedBones() { return m_SortedBones; }

		//   アニメーションが存在するか？
		bool								IsAnimationexist() { return m_pGameModel->IsAnimationexist(); }

		//   物理演算オブジェクトセットのリストを取得
		std::vector<ZPhysicsObjectSet>&		GetPhysicsObjectSetList() { return m_PhysicsObjectSetList; }

		//=================================================================================
		// 設定・解放
		//=================================================================================

		//------------------------------------------------------------------------
		//   モデルを設定する
		// 
		//  ZGameModelを元にデータを構築する
		// 
		//   pGameMesh	… モデルデータであるZGameModel。
		//   enableBones	… ボーン情報も使用する。スタティックメッシュの場合は必要ないので、少しだけ処理・容量がマシになる。
		// 
		//------------------------------------------------------------------------
		bool SetModel(sptr<ZGameModel> pGameMesh, bool enableBones = true);

		//------------------------------------------------------------------------
		//   メッシュを読み込む
		// 
		//   実際の読み込みはZGameModelクラスで行っている		\n
		//   この関数はResourceStorageを使用しモデルを読み込み(ZGameModel)、SetModel()を読んでるだけ
		// 
		//  	filename	… ファイル名
		//------------------------------------------------------------------------
		bool LoadMesh(const std::string& filename);

		//------------------------------------------------------------------------
		//   参照行列設定
		//------------------------------------------------------------------------
		void SetRefMatrix(sptr<ZMatrix> mat);

		//------------------------------------------------------------------------
		//   参照行列初期化(nullptr代入)
		//------------------------------------------------------------------------
		void ResetRefMatrix();

		//------------------------------------------------------------------------
		//   解放
		//------------------------------------------------------------------------
		void Release();

		//------------------------------------------------------------------------
		//   このモデル用にアニメータを初期化する
		//  ※Rootボーンに関しては、複数トラックブレンドが無効になります
		//  	animator	… 初期化するアニメータ
		//------------------------------------------------------------------------
		void InitAnimator(ZAnimator& animator);

		//------------------------------------------------------
		// モデルの物理オブジェクトを物理ワールドへ追加
		//------------------------------------------------------
		void AddAllPhysicsObjToPhysicsWorld(ZPhysicsWorld& world);

		//=================================================================================
		// ボーン行列演算
		//=================================================================================

		//------------------------------------------------------------------------
		//   m_Matをベースに全てのボーンのLocalMatを更新する(計算にはTransMatを使用)。
		//  内部でrecCalcBoneMatrix関数を呼んでいるだけ
		//  	baseMat	… 処理の基になる行列。この行列を先頭に全ボーンを計算していく。
		//------------------------------------------------------------------------
		void CalcBoneMatrix(bool afterPhysics = false);

		void UpdatePhysics();

		//   parentMatをベースに指定ボーン以下のLocalMatを更新する(計算にはTransMatを使用)。
		static void recCalcBoneMatrix(BoneNode& node, const ZMatrix& parentMat)
		{
			// 計算
			// ワールド行列作成
			ZMatrix::Multiply(node.LocalMat, node.TransMat, parentMat);

			// 子再帰
			for (UINT i = 0; i < node.Child.size(); i++)
				recCalcBoneMatrix(*node.Child[i].get(), node.LocalMat);
		}

		//------------------------------------------------------------------------
		//   全ボーンを、描画で使用するボーン用定数バッファに書き込む
		//------------------------------------------------------------------------
		void UpdateBoneConstantBuffer();

		// ボーン定数バッファ取得
		ZConstantBuffer<cbBones>& GetBoneConstantBuffer()
		{
			return m_cb_Bones;
		}

		//------------------------------------------------------------------------
		//   全ボーンのTransMatをデフォルトに戻す
		//------------------------------------------------------------------------
		void ResetDefaultTransMat();

		//=================================================================================
		//=================================================================================

		//------------------------------------------------------------------------
		//   ボーンテーブルから指定した名前のボーンの情報を取得 名前が存在しない場合はnullptrが返る
		//  	BoneName	… 検索したいボーン名
		//  @return 存在する場合:ボーンデータのアドレス　存在しない場合:nullptr
		//------------------------------------------------------------------------
		sptr<BoneNode> SearchBone(const std::string& BoneName)
		{
			for (UINT i = 0; i < m_BoneTree.size(); i++)
			{
				if (m_BoneTree[i]->pSrcBoneNode->BoneName == BoneName)
				{
					return m_BoneTree[i];
				}
			}
			return nullptr;
		}

		//------------------------------------------------------------------------
		//   ボーンテーブルから指定した名前のボーンの番号を取得 名前が存在しない場合は-1が返る
		//  	BoneName	… 検索したいボーン名
		//  @return 存在する場合:ボーン番号　存在しない場合:-1
		//------------------------------------------------------------------------
		int SearchBoneIndex(const std::string& BoneName)
		{
			for (UINT i = 0; i < m_BoneTree.size(); i++)
			{
				if (m_BoneTree[i]->pSrcBoneNode->BoneName == BoneName)
				{
					return i;
				}
			}
			return -1;
		}

		void EnableIKBone(const std::string& IKBoneName, bool flg);

		bool IsEnableIK(const std::string& IKBoneName);

		// 
		ZBoneController()
		{
		}
		// 
		ZBoneController(sptr<ZGameModel> gameModel, bool enableBones)
		{
			SetModel(gameModel, enableBones);
		}

		//
		~ZBoneController()
		{
			Release();
		}

	private:
		void CreateBoneTreeFromModel();
		
		bool CreatePhysicsObjectsFromModel();

	private:
		//---------------------------------------------------------------------------------
		//   元となるZGameModelのポインタ
		//---------------------------------------------------------------------------------
		sptr<ZGameModel>						m_pGameModel = nullptr;

		//---------------------------------------------------------------------------------
		//   ボーンツリー＆ボーン配列
		//  [0]がRoot ツリー構造であるが、1次元配列としてもアクセス可能。
		//---------------------------------------------------------------------------------
		std::vector<sptr<BoneNode>>				m_BoneTree;

		// ソート済みボーン配列
		std::vector<sptr<BoneNode>>				m_SortedBones;
		
		// 物理更新前に計算するボーン
		std::vector<sptr<BoneNode>>				m_DeformBeforePhysicsBones;

		// 物理更新後に計算するボーン
		std::vector<sptr<BoneNode>>				m_DeformAfterPhysicsBones;

		// IK
		std::vector<uptr<ZIKSolver>>			m_IKs;

		// 行列参照
		wptr<ZMatrix>							m_RefMat;

		// 物理オブジェクト(剛体,ジョイント)リスト
		std::vector<ZPhysicsObjectSet>	 m_PhysicsObjectSetList;

		// 定数バッファ用
		struct cbBones
		{
			ZMatrix	mWArray[1024];	// ボーン行列配列
		};
		ZConstantBuffer<cbBones>				m_cb_Bones;

	public:


		//=====================================================================
		//   ZBoneController用ボーンノード
		//  @ingroup Graphics_Model_Important
		//=====================================================================
		class BoneNode
		{
		public:
			sptr<const ZGameModel::BoneNode>	pSrcBoneNode;	// 元データ側(ZGameModeのBoneNode)のボーンへの参照

			wptr<BoneNode>				Mother;			// 親ボーンへのアドレス
			std::vector<sptr<BoneNode>>	Child;			// 子ボーン配列

			int							Level;			// 階層 Rootが0

			ZMatrix					TransMat;		// 変換行列(Animation関数で更新される)　※親ボーンからの相対的な行列

			ZMatrix					LocalMat;		// ローカル行列(CalcBoneMatrix関数で更新される)　※原点からの相対的な行列

			// ※ボーンの最終的なワールド行列は、「LocalMat* キャラのワールド行列」となります。

			//-----------------------------------------------------------
			//   ボーンIndex取得
			//  @return ボーンIndex  失敗時は-1
			//-----------------------------------------------------------
			int GetBoneIndex() const
			{
				if (pSrcBoneNode == nullptr)return -1;
				return pSrcBoneNode->OffsetID;
			}

			// 
			BoneNode() : Level(0), pSrcBoneNode(0)
			{
			}
		};


	private:
		// コピー禁止用
		ZBoneController(const ZBoneController& src) {}
		void operator=(const ZBoneController& src) {}
	};

	bool CreateRigidBody(ZBoneController::ZBCRigidBody** out, ZGM_RigidBodyData& rbData,sptr<ZGameModel>& model,ZBoneController& bc);
	
	class ZIKSolver
	{
	private:
		using BoneNode = ZBoneController::BoneNode;

		enum class SolveAxis
		{
			X,
			Y,
			Z
		};

		struct IKChain
		{
			sptr<BoneNode> Node;
			bool EnableAxisLimit;
			ZVec3 LimitMax;
			ZVec3 LimitMin;
		};

	public:
		ZIKSolver(ZBoneController& bc);
		~ZIKSolver()
		{
			Release();
		}

		void Release();

		void AddIKChain(sptr<BoneNode> node, bool isKnee = false);
						
		void AddIKChain(sptr<BoneNode> node, bool axisLimit, const ZVec3& limitMin, const ZVec3& limitMax);

		void Solve();

		void SetIKNode(sptr<BoneNode> node)
		{
			m_IKNode = node;
		}
		sptr<BoneNode> GetIKNode()const
		{
			return m_IKNode;
		}

		void SetTargetNode(sptr<BoneNode> node)
		{
			m_IKTarget = node;
		}
		sptr<BoneNode> GetTargetNode()const
		{
			return m_IKTarget;
		}

		std::string GetName()const
		{
			if (m_IKNode != nullptr)
				return m_IKNode->pSrcBoneNode->BoneName;
			else
				return "";
		}

		void SetIterateCount(uint32 count)
		{
			m_IterateCnt = count;
		}
		void SetLimitAngle(float ang)
		{
			m_LimitAng = ang;
		}
		void SetEnable(bool flg)
		{
			m_IsEnable = flg;
		}
		bool GetEnabled()const
		{
			return m_IsEnable;
		}

		void SaveBaseAnim()
		{
			m_IsBaseAnimEnable = m_IsEnable;
		}
		void LoadBaseAnim()
		{
			m_IsEnable = m_IsBaseAnimEnable;
		}
		void ClearBaseAnim()
		{
			m_IsBaseAnimEnable = true;
		}
		bool GetBaseAnimEnabled()const
		{
			return m_IsBaseAnimEnable;
		}

	private:
		void UpdateBone(BoneNode& bone);
		
		void AddIKChain(IKChain&& chain);
		void _Solve(uint32 iteration);
		
	private:
		ZBoneController& m_BC;
		std::vector<IKChain> m_Chains;
		sptr<BoneNode> m_IKNode;
		sptr<BoneNode> m_IKTarget;
		uint32 m_IterateCnt;
		float m_LimitAng; // ループ計算時の1回あたりの制限角度
		bool m_IsEnable;
		bool m_IsBaseAnimEnable;

	};


}
#endif
