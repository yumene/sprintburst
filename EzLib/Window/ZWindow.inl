inline uint32 ZWindow::GetWidth()const
{
	return m_Properties.Width;
}

inline uint32 ZWindow::GetHeight()const
{
	return m_Properties.Height;
}

inline HWND ZWindow::GetWindowHandle()const
{
	return m_WndHandle;
}

inline void ZWindow::SetWindowActive(bool flg)
{
	m_IsWindowActive = flg;
}

inline bool ZWindow::IsWindowActive() const
{
	return m_IsWindowActive;
}

inline bool ZWindow::IsInitialized() const
{
	return m_Initialized;
}
