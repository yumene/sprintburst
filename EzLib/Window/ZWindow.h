#ifndef __ZWINDOW_H__
#define __ZWINDOW_H__

#include "Input\ZInput.h"

// ウィンドウメッセージ処理用のコールバック関数
static LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

namespace EzLib
{
	struct ZWindowProperties
	{
		HINSTANCE hInst;
		uint32 Width;
		uint32 Height;
		bool UseFullScreen;
		bool UseVSync;
	};

	class ZWindow
	{
	public:
		ZWindow(const char* wndTitle,const ZWindowProperties& properties);
		~ZWindow();

		void Update();
		bool IsClosed() const;

		void SetTitle(const char* wndTitle);

		void SetClientSize(uint32 width,uint32 height);
		
		uint32 GetWidth()const;
		uint32 GetHeight()const;
		HWND GetWindowHandle()const;

		void SetWindowActive(bool flg);
		bool IsWindowActive()const;

		bool IsInitialized()const;

		static void RegisterWindowClass(void* handle, ZWindow* window);
		static ZWindow* GetWindowClass(void* handle);

	private:
		bool Init();

	private:
		HINSTANCE			m_hInst;
		HWND				m_WndHandle;
		std::string			m_WndTitle;
		ZWindowProperties	m_Properties;
		bool				m_Closed;
		bool				m_IsWindowActive;
		bool				m_Initialized;

		static std::unordered_map<void*, ZWindow*> m_sHandles;

	};

}

namespace EzLib
{
#include "ZWindow.inl"
}

#endif