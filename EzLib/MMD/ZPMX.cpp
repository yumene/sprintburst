#include "ZPMX.h"

#include <vector>
#include <string>
#include "Utils/ZFile.h"

#include "EzLib.h"

namespace EzLib
{
namespace
{
	template<typename T>
	bool Read(T* val, ZFile& file)
	{
		return file.Read(val);
	}

	template<typename T>
	bool Read(T* valArray, size_t size, ZFile& file)
	{
		return file.Read(valArray, size);
	}

	bool ReadStr(ZPMX* pmx, std::string* str, ZFile& file)
	{
		uint32 bufSize;
		if (Read(&bufSize, file) == false)
			return false;

		if (bufSize > 0)
		{
			if (pmx->Header.Encode == 0)
			{
				std::u16string utf16Str(bufSize / 2, u'\0');
				if (file.Read(&utf16Str[0], utf16Str.size()) == false)
					return false;
				*str = Utf16ToMulti(utf16Str);

			}
			else if (pmx->Header.Encode == 1)
			{
				// UTF-8
				std::string utf8Str(bufSize, '\0');
				file.Read(&utf8Str[0], bufSize);

				*str = Utf8ToMulti(utf8Str);
			}
		}

		return !file.IsBad();

	}

	bool ReadIndex(int32* index, uint8 indexSize, ZFile& file)
	{
		switch (indexSize)
		{
			case 1:
			{
				uint8 idx;
				Read(&idx, file);
				if (idx != 0xFF)
					*index = (int32)idx;
				else
					*index = -1;
			}
			break;

			case 2:
			{
				uint16 idx;
				Read(&idx, file);
				if (idx != 0xFFFF)
					*index = (int32)idx;
				else
					*index = -1;
			}
			break;

			case 4:
			{
				uint32 idx;
				Read(&idx, file);
				*index = (int32)idx;
			}
			break;

			default:
				return false;
		}
	
		return !file.IsBad();
	
	}

	bool ReadHeader(ZPMX* pmx, ZFile& file)
	{
		auto& header = pmx->Header;
		Read(&header.Magic, file);
		Read(&header.Version, file);
		Read(&header.DataSize, file);
		Read(&header.Encode, file);
		Read(&header.AddUVNum, file);
		Read(&header.VertexIndexSize, file);
		Read(&header.TextureIndexSize, file);
		Read(&header.MaterialIndexSize, file);
		Read(&header.BoneIndexSize, file);
		Read(&header.MorphIndexSize, file);
		Read(&header.RigidBodyIndexSize, file);

		return !file.IsBad();
	}

	bool ReadInfo(ZPMX* pmx, ZFile& file)
	{
		auto& info = pmx->Info;
		ReadStr(pmx, &info.ModelName, file);
		ReadStr(pmx, &info.EnglishModelName, file);
		ReadStr(pmx, &info.Comment, file);
		ReadStr(pmx, &info.EnglishComment, file);

		return !file.IsBad();
	}

	bool ReadVertex(ZPMX* pmx, ZFile& file)
	{
		int32 vertexCnt;
		if (Read(&vertexCnt, file) == false)
			return false;

		auto& vertices = pmx->Vertices;
		vertices.resize(vertexCnt);
		for (auto& vertex : vertices)
		{
			Read(&vertex.Position, file);
			Read(&vertex.Normal, file);
			Read(&vertex.UV, file);

			for (uint8 i = 0; i < pmx->Header.AddUVNum; i++)
				Read(&vertex.AddUV[i], file);

			Read(&vertex.WeightType, file);

			switch (vertex.WeightType)
			{
				case ZPMXVertexWeight::BDEF1:
					ReadIndex(&vertex.BoneIndices[0], pmx->Header.BoneIndexSize, file);
				break;

				case ZPMXVertexWeight::BDEF2:
				{
					ReadIndex(&vertex.BoneIndices[0], pmx->Header.BoneIndexSize, file);
					ReadIndex(&vertex.BoneIndices[1], pmx->Header.BoneIndexSize, file);
					Read(&vertex.BoneWeights[0],file);
				}
				break;

				case ZPMXVertexWeight::BDEF4:
				case ZPMXVertexWeight::QDEF:
				{
					for(int i = 0;i<4;i++)
						ReadIndex(&vertex.BoneIndices[i], pmx->Header.BoneIndexSize, file);
					for (int i = 0; i < 4; i++)
						Read(&vertex.BoneWeights[i], file);
				}
				break;

				case ZPMXVertexWeight::SDEF:
				{
					ReadIndex(&vertex.BoneIndices[0], pmx->Header.BoneIndexSize, file);
					ReadIndex(&vertex.BoneIndices[1], pmx->Header.BoneIndexSize, file);
					Read(&vertex.BoneWeights[0], file);
					Read(&vertex.SdefC, file);
					Read(&vertex.SdefR0, file);
					Read(&vertex.SdefR1, file);
				}
				break;

				default:
					return false;
			}

			Read(&vertex.EdgeMag, file);
		}

		return !file.IsBad();

	}

	bool ReadFace(ZPMX* pmx, ZFile& file)
	{
		int32 faceCnt = 0;
		if (Read(&faceCnt, file) == false)
			return false;

		faceCnt /= 3;

		pmx->Faces.resize(faceCnt);
		
		switch (pmx->Header.VertexIndexSize)
		{
			case 1:
			{
				std::vector<uint8> vertices(faceCnt * 3);
				Read(vertices.data(), vertices.size(), file);
				for (int32 faceIdx = 0; faceIdx < faceCnt; faceIdx++)
				{
					for(int i = 0;i<3;i++)
						pmx->Faces[faceIdx].Vertices[i] = vertices[faceIdx * 3 + i];

				}
			}
			break;

			case 2:
			{
				std::vector<uint16> vertices(faceCnt * 3);
				Read(vertices.data(), vertices.size(), file);
				for (int32 faceIdx = 0; faceIdx < faceCnt; faceIdx++)
				{
					for (int i = 0; i < 3; i++)
						pmx->Faces[faceIdx].Vertices[i] = vertices[faceIdx * 3 + i];
				}
			}
			break;

			case 4:
			{
				std::vector<uint32> vertices(faceCnt * 3);
				Read(vertices.data(), vertices.size(), file);
				for (int32 faceIdx = 0; faceIdx < faceCnt; faceIdx++)
				{
					for (int i = 0; i < 3; i++)
						pmx->Faces[faceIdx].Vertices[i] = vertices[faceIdx * 3 + i];
				}
			}
			break;

			default:
				return false;
		}

		return !file.IsBad();
	}

	bool ReadTexture(ZPMX* pmx, ZFile& file)
	{
		int32 texCnt = 0;
		if (Read(&texCnt, file) == false)
			return false;

		pmx->Textures.resize(texCnt);
		for (auto& tex : pmx->Textures)
			ReadStr(pmx, &tex.TextureName, file);

		return !file.IsBad();
	}

	bool ReadMaterial(ZPMX* pmx, ZFile& file)
	{
		int32 matCnt = 0;
		if (Read(&matCnt, file) == false)
			return false;

		pmx->Materials.resize(matCnt);

		for (auto& mat : pmx->Materials)
		{
			ReadStr(pmx, &mat.Name, file);
			ReadStr(pmx, &mat.EnglishName, file);

			Read(&mat.Diffuse, file);
			Read(&mat.Specular, file);
			Read(&mat.SpePower, file);
			Read(&mat.Ambient, file);

			Read(&mat.DrawMode, file);
			Read(&mat.EdgeColor, file);
			Read(&mat.EdgeSize, file);

			ReadIndex(&mat.TextureIndex, pmx->Header.TextureIndexSize, file);
			ReadIndex(&mat.SphereTextureIndex, pmx->Header.TextureIndexSize, file);
			Read(&mat.SphereMode, file);

			Read(&mat.ToonMode, file);
			if (mat.ToonMode == ZPMXToonMode::Separate)
				ReadIndex(&mat.ToonTextureIndex, pmx->Header.TextureIndexSize, file);
			else if (mat.ToonMode == ZPMXToonMode::Common)
			{
				uint8 toonIndex;
				Read(&toonIndex, file);
				mat.ToonTextureIndex = (int32)toonIndex;
			}
			else
				return false;

			ReadStr(pmx, &mat.Memo, file);
			Read(&mat.NumFaceVertices, file);
		}
		
		return !file.IsBad();
	}

	bool ReadBone(ZPMX* pmx, ZFile& file)
	{
		int32 boneCnt;
		if (Read(&boneCnt, file) == false)
			return false;

		pmx->Bones.resize(boneCnt);

		for (auto& bone : pmx->Bones)
		{
			ReadStr(pmx, &bone.Name, file);
			ReadStr(pmx, &bone.EnglishName, file);

			Read(&bone.Position, file);
			ReadIndex(&bone.ParentBoneIndex, pmx->Header.BoneIndexSize, file);
			Read(&bone.DeformDepth, file);

			Read(&bone.BoneFlag, file);
			if (((uint16)bone.BoneFlag & (uint16)ZPMXBoneFlags::TargetShadowMode) == 0)
				Read(&bone.PositionOffset, file);
			else
				ReadIndex(&bone.LinkBoneIndex, pmx->Header.BoneIndexSize, file);

			if (((uint16)bone.BoneFlag & (uint16)ZPMXBoneFlags::AppendRotate) ||
				((uint16)bone.BoneFlag & (uint16)ZPMXBoneFlags::AppendTranslate))
			{
				ReadIndex(&bone.AppendBoneIndex, pmx->Header.BoneIndexSize, file);
				Read(&bone.AppendWeight, file);
			}

			if ((uint16)bone.BoneFlag & (uint16)ZPMXBoneFlags::FixedAxis)
				Read(&bone.FixedAxis, file);

			if ((uint16)bone.BoneFlag & (uint16)ZPMXBoneFlags::LocalAxis)
			{
				Read(&bone.LocalXAxis, file);
				Read(&bone.LocalZAxis, file);
			}

			if ((uint16)bone.BoneFlag & (uint16)ZPMXBoneFlags::DeformOuterParent)
				Read(&bone.KeyValue, file);

			if ((uint16)bone.BoneFlag & (uint16)ZPMXBoneFlags::IK)
			{
				ReadIndex(&bone.IKTargetBoneIndex, pmx->Header.BoneIndexSize, file);
				Read(&bone.IKIterationCount, file);
				Read(&bone.IKLimit, file);

				int32 linkCnt;
				if (Read(&linkCnt, file) == false)
					return false;
				
				bone.IKLinks.resize(linkCnt);

				for (auto& IKLink : bone.IKLinks)
				{
					ReadIndex(&IKLink.IKBoneIndex, pmx->Header.BoneIndexSize, file);
					Read(&IKLink.EnableLimit, file);

					if (IKLink.EnableLimit != 0)
					{
						Read(&IKLink.LimitMin, file);
						Read(&IKLink.LimitMax, file);
					}

				}

			}

		}

		return !file.IsBad();

	}

	bool ReadMorph(ZPMX* pmx, ZFile& file)
	{
		int32 morphCnt;
		if (Read(&morphCnt, file) == false)
			return false;
		pmx->Morphs.resize(morphCnt);

		for (auto& morph : pmx->Morphs)
		{
			ReadStr(pmx, &morph.Name, file);
			ReadStr(pmx, &morph.EnglishName, file);

			Read(&morph.ControlPanel, file);
			Read(&morph.MorphType,file);

			int32 dataCnt;
			if (Read(&dataCnt, file) == false)
				return false;

			if (morph.MorphType == ZPMXMorphType::Position)
			{
				morph.PositionMorphs.resize(dataCnt);
				for (auto& data : morph.PositionMorphs)
				{
					ReadIndex(&data.VertexIndex, pmx->Header.VertexIndexSize, file);
					Read(&data.Position, file);
				}
			}
			else if(morph.MorphType == ZPMXMorphType::UV ||
					morph.MorphType == ZPMXMorphType::AddUV1 ||
					morph.MorphType == ZPMXMorphType::AddUV2 ||
					morph.MorphType == ZPMXMorphType::AddUV3 ||
					morph.MorphType == ZPMXMorphType::AddUV4)
			{
				morph.UvMorphs.resize(dataCnt);
				for (auto& data : morph.UvMorphs)
				{
					ReadIndex(&data.VertexIndex, pmx->Header.VertexIndexSize, file);
					Read(&data.UV, file);
				}

			}
			else if (morph.MorphType == ZPMXMorphType::Bone)
			{
				morph.BoneMorphs.resize(dataCnt);
				for (auto& data : morph.BoneMorphs)
				{
					ReadIndex(&data.BoneIndex, pmx->Header.BoneIndexSize, file);
					Read(&data.Position, file);
					Read(&data.Quaternion, file);
				}

			}
			else if (morph.MorphType == ZPMXMorphType::Material)
			{
				morph.MaterialMorphs.resize(dataCnt);
				for (auto& data : morph.MaterialMorphs)
				{
					ReadIndex(&data.MaterialIndex, pmx->Header.MaterialIndexSize, file);
					Read(&data.OPType, file);
					Read(&data.Diffuse, file);
					Read(&data.Specular, file);
					Read(&data.SpePower,file);
					Read(&data.Ambient, file);
					Read(&data.EdgeColor, file);
					Read(&data.EdgeSize, file);
					Read(&data.TextureFactor, file);
					Read(&data.SphereTextureFactor, file);
					Read(&data.ToonTextureFactor, file);
				}
			}
			else if (morph.MorphType == ZPMXMorphType::Group)
			{
				morph.GroupMorphs.resize(dataCnt);
				for (auto& data : morph.GroupMorphs)
				{
					ReadIndex(&data.MorphIndex, pmx->Header.MorphIndexSize, file);
					Read(&data.Weight, file);
				}
			}
			else if (morph.MorphType == ZPMXMorphType::Flip)
			{
				morph.FlipMorphs.resize(dataCnt);
				for (auto& data : morph.FlipMorphs)
				{
					ReadIndex(&data.MorphIndex, pmx->Header.MorphIndexSize, file);
					Read(&data.Weight, file);
				}
			}
			else if (morph.MorphType == ZPMXMorphType::Impulse)
			{
				morph.ImpulseMorphs.resize(dataCnt);
				for (auto&data : morph.ImpulseMorphs)
				{
					ReadIndex(&data.RigidBodyIndex, pmx->Header.RigidBodyIndexSize, file);
					Read(&data.LocalFlag, file);
					Read(&data.TranslateVelocity, file);
					Read(&data.RotateTorque, file);
				}
			}
			else
			{
				std::string errorMsg = "Unsupported MorphType : " + std::to_string((int)morph.MorphType);
				MessageBox(nullptr, "Unsupported MorphType ", "Error", MB_OK);
			}
		}

		return !file.IsBad();

	}

	bool ReadDisplayFrame(ZPMX* pmx, ZFile& file)
	{
		int32 displayFrameCnt;
		if (Read(&displayFrameCnt, file) == false)
			return false;

		pmx->DisplayFrames.resize(displayFrameCnt);
		for (auto& displayFrame : pmx->DisplayFrames)
		{
			ReadStr(pmx, &displayFrame.Name, file);
			ReadStr(pmx, &displayFrame.EnglishName, file);

			Read(&displayFrame.Flag, file);
			int32 targetCnt;
			if (Read(&targetCnt, file) == false)
				return false;

			displayFrame.Targets.resize(targetCnt);

			for (auto& target : displayFrame.Targets)
			{
				Read(&target.Type, file);
				if (target.Type == ZPMXDisplayFrame::TargetType::BoneIndex)
					ReadIndex(&target.Index, pmx->Header.BoneIndexSize, file);
				else if (target.Type == ZPMXDisplayFrame::TargetType::MorphIndex)
					ReadIndex(&target.Index, pmx->Header.MorphIndexSize, file);
				else
					return false;
			}

		}
		
		return !file.IsBad();
	}

	bool ReadRigidBody(ZPMX* pmx, ZFile& file)
	{
		int32 rbCnt;
		if (Read(&rbCnt, file) == false)
			return false;

		pmx->PhysicsDataSet.RigidBodyTbl.resize(rbCnt);
		for (auto& rb : pmx->PhysicsDataSet.RigidBodyTbl)
		{
			ReadStr(pmx, &rb.Name, file);
			ReadStr(pmx, &rb.EnglishName, file);

			ReadIndex(&rb.BoneIndex, pmx->Header.BoneIndexSize, file);
			Read(&rb.Group, file);
			Read(&rb.UnCollisionGroup, file);

			Read(&rb.Shape, file);
			Read(&rb.ShapeSize, file);

			Read(&rb.Translate, file);
			Read(&rb.Rotate, file);

			Read(&rb.Mass, file);
			Read(&rb.TranslateDimmer, file);
			Read(&rb.RotateDimmer, file);
			Read(&rb.Repulsion, file);
			Read(&rb.Friction, file);

			Read(&rb.OP, file);
		}

		return !file.IsBad();

	}

	bool ReadJoint(ZPMX* pmx, ZFile& file)
	{
		int32 jointCnt;
		if (Read(&jointCnt, file) == false)
			return false;

		pmx->PhysicsDataSet.JointTbl.resize(jointCnt);
		for (auto& joint : pmx->PhysicsDataSet.JointTbl)
		{
			ReadStr(pmx, &joint.Name, file);
			ReadStr(pmx, &joint.EnglishName, file);
			
			Read(&joint.Type, file);
			ReadIndex(&joint.RigidBodyAIndex, pmx->Header.RigidBodyIndexSize, file);
			ReadIndex(&joint.RigidBodyBIndex, pmx->Header.RigidBodyIndexSize, file);

			Read(&joint.Translate, file);
			Read(&joint.Rotate, file);
			
			Read(&joint.TranslateLowerLimit, file);
			Read(&joint.TranslateUpperLimit, file);
			Read(&joint.RotateLowerLimit, file);
			Read(&joint.RotateUpperLimit, file);

			Read(&joint.SpringTranslateFactor, file);
			Read(&joint.SpringRotateFactor, file);
		}

		return !file.IsBad();

	}

	bool ReadSoftBody(ZPMX* pmx, ZFile& file)
	{
		int32 sbCnt;
		if (Read(&sbCnt, file) == false)
			return false;

		pmx->PhysicsDataSet.SoftBodyTbl.resize(sbCnt);

		for (auto& sb : pmx->PhysicsDataSet.SoftBodyTbl)
		{
			ReadStr(pmx, &sb.Name, file);
			ReadStr(pmx, &sb.EnglishName, file);

			Read(&sb.Type, file);
			ReadIndex(&sb.MaterialIndex, pmx->Header.MaterialIndexSize, file);

			Read(&sb.Group, file);
			Read(&sb.CollisionGroup, file);

			Read(&sb.Flag, file);

			Read(&sb.BLinkLength, file);
			Read(&sb.NumClusters, file);

			Read(&sb.TotalMass, file);
			Read(&sb.CollisionMargin, file);

			Read(&sb.AeroModel, file);

			Read(&sb.VCF, file);
			Read(&sb.DP, file);
			Read(&sb.DG, file);
			Read(&sb.LF, file);
			Read(&sb.PR, file);
			Read(&sb.VC, file);
			Read(&sb.DF, file);
			Read(&sb.MT, file);
			Read(&sb.CHR, file);
			Read(&sb.KHR, file);
			Read(&sb.SHR, file);
			Read(&sb.AHR, file);

			Read(&sb.SRHR_CL, file);
			Read(&sb.SKHR_CL, file);
			Read(&sb.SSHR_CL, file);
			Read(&sb.SR_SPLT_CL, file);
			Read(&sb.SK_SPLT_CL, file);
			Read(&sb.SS_SPLT_CL, file);

			Read(&sb.V_IT, file);
			Read(&sb.P_IT, file);
			Read(&sb.D_IT, file);
			Read(&sb.C_IT, file);

			Read(&sb.LST, file);
			Read(&sb.AST, file);
			Read(&sb.VST, file);

			int32 arCnt;
			if (Read(&arCnt, file) == false)
				return false;

			sb.AnchorRigidBodies.resize(arCnt);
			for (auto& ar : sb.AnchorRigidBodies)
			{
				ReadIndex(&ar.RigidBodyIndex, pmx->Header.RigidBodyIndexSize, file);
				ReadIndex(&ar.VertexIndex, pmx->Header.VertexIndexSize, file);
				Read(&ar.NearMode, file);
			}

			int32 pvCnt;
			if (Read(&pvCnt, file) == false)
				return false;

			sb.PinVertexIndices.resize(pvCnt);
			for (auto& pv : sb.PinVertexIndices)
				ReadIndex(&pv, pmx->Header.VertexIndexSize, file);

		}

		return !file.IsBad();
	}

	bool ReadPMXFile(ZPMX* pmx, ZFile& file)
	{
		if (ReadHeader(pmx, file) == false)
		{
			MessageBox(nullptr, "Read Header Fail", "PMX File Load Error" , MB_OK);
			return false;
		}

		if (ReadInfo(pmx, file) == false)
		{
			MessageBox(nullptr, "Read Info Fail", "PMX File Load Error", MB_OK);
			return false;
		}

		if (ReadVertex(pmx, file) == false)
		{
			MessageBox(nullptr, "Read Vertex Fail", "PMX File Load Error", MB_OK);
			return false;
		}

		if (ReadFace(pmx, file) == false)
		{
			MessageBox(nullptr, "Read Face Fail", "PMX File Load Error", MB_OK);
			return false;
		}

		if (ReadTexture(pmx, file) == false)
		{
			MessageBox(nullptr, "Read Texture Fail", "PMX File Load Error", MB_OK);
			return false;
		}

		if (ReadMaterial(pmx, file) == false)
		{
			MessageBox(nullptr, "Read Material Fail", "PMX File Load Error", MB_OK);
			return false;
		}

		if (ReadBone(pmx, file) == false)
		{
			MessageBox(nullptr, "Read Bone Fail", "PMX File Load Error", MB_OK);
			return false;
		}

		if (ReadMorph(pmx, file) == false)
		{
			MessageBox(nullptr, "Read Morph Fail", "PMX File Load Error", MB_OK);
			return false;
		}
		
		if (ReadDisplayFrame(pmx, file) == false)
		{
			MessageBox(nullptr, "Read DisplayFrame Fail", "PMX File Load Error", MB_OK);
			return false;
		}

		if (ReadRigidBody(pmx, file) == false)
		{
			MessageBox(nullptr, "Read RigidBody Fail", "PMX File Load Error", MB_OK);
			return false;
		}

		if (ReadJoint(pmx, file) == false)
		{
			MessageBox(nullptr, "Read Joint Fail", "PMX File Load Error", MB_OK);
			return false;
		}

		if (file.Tell() < file.GetSize())
		{
			if (ReadSoftBody(pmx, file) == false)
			{
				MessageBox(nullptr, "Read SoftBody Fail", "PMX File Load Error", MB_OK);
				return false;
			}
		}

		return true;
	}

}

	bool ReadPMXFile(ZPMX* pmx, const char* fileName)
	{
		ZFile file;
		if (file.Open(fileName,ZFile::OPEN_MODE::READ) == false)
		{
			MessageBox(nullptr, "PMX FIle Open Fail", "Error", MB_OK);
			return false;
		}

		if (ReadPMXFile(pmx, file) == false)
		{
			MessageBox(nullptr, "PMX FIle Read Fail", "Error", MB_OK);
			return false;
		}
		
		return true;
	}

}
