#ifndef __ZPMX_H__
#define __ZPMX_H__

#include <cstdint>
#include <string>
#include <vector>

#include "SystemDefines.h"
#include "ZMMDFileStr.h"
#include "Math/ZMath.h"

namespace EzLib
{
	template<size_t size>
	using ZPMXString = ZMMDFileStr<size>;

	struct ZPMXHeader
	{
		ZPMXString<4> Magic;
		float Version;
		uint8 DataSize;
		uint8 Encode; // 0... UTF16 1... UTF8
		uint8 AddUVNum;
		uint8 VertexIndexSize;
		uint8 TextureIndexSize;
		uint8 MaterialIndexSize;
		uint8 BoneIndexSize;
		uint8 MorphIndexSize;
		uint8 RigidBodyIndexSize;
	};

	struct ZPMXInfo
	{
		std::string ModelName;
		std::string EnglishModelName;
		std::string Comment;
		std::string EnglishComment;
	};

	// BDEF1
	//  BoneIndices[0]
	// BDEF2
	//  BoneIndices[0~1]
	//  BoneWeight[0]
	// BDEF4
	//  BoneIndices[0~3]
	//  BoneWeights[0~3]
	// SDEF
	//  BoneIndices[0~1]
	//  BoneWeights[0]
	//  SdefC
	//  SdefR0
	//  SdefR1
	// QDEF
	//  BoneIndices[0~3]
	//  BoneWeights[0~3]
	enum class ZPMXVertexWeight : uint8
	{
		BDEF1,
		BDEF2,
		BDEF4,
		SDEF,
		QDEF
	};

	struct ZPMXVertex
	{
		ZVec3 Position;
		ZVec3 Normal;
		ZVec2 UV;
		ZVec4 AddUV[4];
		ZPMXVertexWeight WeightType; // 0... BDEF1  1... BDEF2  2... BDEF4  3... SDEF  4... QDEF
		int32 BoneIndices[4];
		float BoneWeights[4];
		ZVec3 SdefC;
		ZVec3 SdefR0;
		ZVec3 SdefR1;
		float EdgeMag;
	};

	struct ZPMXFace
	{
		uint32 Vertices[3];
	};

	struct ZPMXTexture
	{
		std::string TextureName;
	};

	// 0x01... 両面描画
	// 0x02... 地面影
	// 0x04... セルフシャドウマップへの描画
	// 0x08... セルフシャドウ描画
	// 0x10... エッジ描画
	// 0x20... 頂点カラー(2.1拡張)
	// 0x40... Point描画(2.1拡張)
	// 0x80... Line描画
	enum class ZPMXDrawModeFlags : uint8
	{
		BothFlag = 0x01,
		GroundShadow = 0x02,
		CastSelfShadow = 0x04,
		RecieveSelfShadow = 0x08,
		DrawEdge = 0x10,
		VertexColor = 0x20,
		DrawPoint = 0x40,
		DrawLine = 0x80
	};

	// 0... 無効
	// 1... 乗算
	// 2... 加算
	// 3... サブテクスチャ(追加UV1のx,yを参照し通常テクスチャ描画を行う)
	enum class ZPMXSphereMode : uint8
	{
		None,
		Mul,
		Add,
		SubTexture
	};

	enum class ZPMXToonMode : uint8
	{
		Separate,	// 0... 個別Toon
		Common		// 1... 共有Toon[0~9] toon01.bmp ~ toon10.bmp
	};

	struct ZPMXMaterial
	{
		std::string Name;
		std::string EnglishName;
		ZVec4 Diffuse;
		ZVec3 Specular;
		float SpePower;
		ZVec3 Ambient;
		ZPMXDrawModeFlags DrawMode;
		ZVec4 EdgeColor;
		float EdgeSize;
		int32 TextureIndex;
		int32 SphereTextureIndex;
		ZPMXSphereMode SphereMode;
		ZPMXToonMode ToonMode;
		int32 ToonTextureIndex;
		std::string Memo;
		int32 NumFaceVertices;
	};

	// 0x0001... 接続先(PMD子ボーン指定)表示方法 0. 座標オフセットで指定 1. ボーンで指定
	//
	// 0x0002... 回転可能
	// 0x0004... 移動可能
	// 0x0008... 表示
	// 0x0010... 操作可
	//
	// 0x0020... IK
	//
	// 0x0080... ローカル付与 | 付与対象 0. ユーザー変形値/IKリンク/多重付与 1. 親のローカル変化量
	// 0x0100... 回転付与
	// 0x0200... 移動付与
	//
	// 0x0400... 軸固定
	// 0x0800... ローカル軸
	//
	// 0x1000... 物理後変形
	// 0x2000... 外部親変形
	enum class ZPMXBoneFlags : uint16
	{
		TargetShadowMode = 0x0001,
		AllowRotate = 0x0002,
		AllowTranslate = 0x0004,
		Visible = 0x0008,
		AllowControl = 0x0010,
		IK = 0x0020,
		AppendLocal = 0x0080,
		AppendRotate = 0x0100,
		AppendTranslate = 0x0200,
		FixedAxis = 0x0400,
		LocalAxis = 0x0800,
		DeformAfterPhysics = 0x1000,
		DeformOuterParent = 0x2000
	};

	struct ZPMXIKLink
	{
		int32 IKBoneIndex;
		unsigned char EnableLimit;

		// EnableLimit == 1のとき
		// ラジアン表現
		ZVec3 LimitMin;
		ZVec3 LimitMax;
	};

	struct ZPMXBone
	{
		std::string Name;
		std::string EnglishName;
		ZVec3 Position;
		int32 ParentBoneIndex;
		int32 DeformDepth;
		ZPMXBoneFlags BoneFlag;
		ZVec3 PositionOffset; // 接続先 == 0
		int32 LinkBoneIndex;  // 接続先 == 1
		int32 AppendBoneIndex;
		float AppendWeight;
		// 軸固定が有効なときのみ
		ZVec3 FixedAxis;
		// ローカル軸が有効なときのみ
		ZVec3 LocalXAxis;
		ZVec3 LocalZAxis;
		// 外部親変形が有効なときのみ
		int32 KeyValue;
		// IKが有効なときのみ
		int32 IKTargetBoneIndex;
		int32 IKIterationCount;
		float IKLimit; // ラジアン表現

		std::vector<ZPMXIKLink> IKLinks;
	};

	//  0... グループ
	//  1... 頂点
	//  2... ボーン
	//  3... UV
	//  4... 追加UV1
	//  5... 追加UV2
	//  6... 追加UV3
	//  7... 追加UV4
	//  8... 材質
	//  9... フリップ(2.1拡張)
	// 10... インパルス(2.1拡張)
	enum class ZPMXMorphType : uint8
	{
		Group,
		Position,
		Bone,
		UV,
		AddUV1,
		AddUV2,
		AddUV3,
		AddUV4,
		Material,
		Flip,
		Impulse
	};

	struct ZPMXMorph
	{
	public:
		struct PositionMorph
		{
			int32 VertexIndex;
			ZVec3 Position;
		};

		struct UVMorph
		{
			int32 VertexIndex;
			ZVec4 UV;
		};

		struct BoneMorph
		{
			int32 BoneIndex;
			ZVec3 Position;
			ZQuat Quaternion;
		};

		struct MaterialMorph
		{
		public:
			enum class OpType : uint8
			{
				Mul,
				Add
			};

		public:
			int32 MaterialIndex;
			OpType OPType;
			ZVec4 Diffuse;
			ZVec3 Specular;
			float SpePower;
			ZVec3 Ambient;
			ZVec4 EdgeColor;
			float EdgeSize;
			ZVec4 TextureFactor;
			ZVec4 SphereTextureFactor;
			ZVec4 ToonTextureFactor;

		};

		struct GroupMorph
		{
			int32 MorphIndex;
			float Weight;
		};

		struct FlipMorph
		{
			int32 MorphIndex;
			float Weight;
		};

		struct ImpulseMorph
		{
			int32 RigidBodyIndex;
			uint8 LocalFlag;	// 0... OFF  1... ON
			ZVec3 TranslateVelocity;
			ZVec3 RotateTorque;
		};

	public:
		std::string Name;
		std::string EnglishName;
		uint8 ControlPanel;
		ZPMXMorphType MorphType;
		std::vector<PositionMorph> PositionMorphs;
		std::vector<UVMorph> UvMorphs;
		std::vector<BoneMorph> BoneMorphs;
		std::vector<MaterialMorph> MaterialMorphs;
		std::vector<GroupMorph> GroupMorphs;
		std::vector<FlipMorph> FlipMorphs;
		std::vector<ImpulseMorph> ImpulseMorphs;

	};

	struct ZPMXDisplayFrame
	{
	public:
		enum class TargetType :uint8
		{
			BoneIndex,
			MorphIndex
		};

		struct Target
		{
			TargetType Type;
			int32 Index;
		};

		enum class FrameType :uint8
		{
			DefaultFrame, // 0... 通常枠
			SpecialFrame  // 1... 特殊枠
		};

	public:
		std::string Name;
		std::string EnglishName;

		FrameType Flag;
		std::vector<Target> Targets;

	};

	struct ZPMXRigidBody
	{
	public:
		// 0... 球
		// 1... 箱
		// 2... カプセル
		enum class shape : uint8
		{
			Sphere,
			Box,
			Capsule
		};

		// 0... ボーン追従(Static)
		// 1... 物理演算(Dynamic)
		// 2... 物理演算 + Bone位置合わせ
		enum class Operation : uint8
		{
			Static,
			Dynamic,
			DynamicAndBoneMerge
		};


	public:
		std::string Name;
		std::string EnglishName;

		int32 BoneIndex;
		uint8 Group;
		uint16 UnCollisionGroup;
		
		shape Shape;
		ZVec3 ShapeSize;
		ZVec3 Translate;
		ZVec3 Rotate; // ラジアン表現

		float Mass;
		float TranslateDimmer;
		float RotateDimmer;
		float Repulsion;
		float Friction;

		Operation OP;
	};

	struct ZPMXJoint
	{
	public:
		// 0... バネ付き6DoF
		// 1... 6DoF
		// 2... P2P
		// 3... ConeTwist
		// 4... Slider
		// 5... Hinge
		enum class JointType : uint8
		{
			SpringDoF6,
			DoF6,
			P2P,
			ConeTwist,
			Slider,
			Hinge
		};


	public:
		std::string Name;
		std::string EnglishName;

		JointType Type;
		int32 RigidBodyAIndex;
		int32 RigidBodyBIndex;
		ZVec3 Translate;
		ZVec3 Rotate;
		ZVec3 TranslateLowerLimit;
		ZVec3 TranslateUpperLimit;
		ZVec3 RotateLowerLimit;
		ZVec3 RotateUpperLimit;
		ZVec3 SpringTranslateFactor;
		ZVec3 SpringRotateFactor;

	};

	struct ZPMXSoftBody
	{
	public:

		// 0... TriMesh
		// 1... Rope
		enum class SoftBodyType : uint8
		{
			TriMesh,
			Rope
		};

		// 0x01... B-Link作成
		// 0x02... クラスタ作成
		// 0x04... リンク交雑
		enum class SoftBodyMask : uint8
		{
			BLink = 0x01,
			Cluster = 0x02,
			HybridLink = 0x04
		};

		// 1... V_TwoSided
		// 2... V_OneSided
		// 3... F_TwoSided
		// 4... F_OneSided
		enum class AeroModel : int32
		{
			kAeroModelV_TwoSided,
			kAeroModelV_OneSided,
			kAeroModelF_TwoSided,
			kAeroModelF_OneSided
		};
		
		struct AnchorRigidBody
		{
			int32 RigidBodyIndex;
			int32 VertexIndex;
			uint8 NearMode; // 0... OFF  1... ON
		};

	public:
		std::string Name;
		std::string EnglishName;
		SoftBodyType Type;
		int32 MaterialIndex;
		uint8 Group;
		uint16 CollisionGroup;
		SoftBodyMask Flag;
		int32 BLinkLength;
		int32 NumClusters;
		float TotalMass;
		float CollisionMargin;
		int32 AeroModel;

		/* config */
		float VCF;
		float DP;
		float DG;
		float LF;
		float PR;
		float VC;
		float DF;
		float MT;
		float CHR;
		float KHR;
		float SHR;
		float AHR;

		/* cluster */
		float SRHR_CL;
		float SKHR_CL;
		float SSHR_CL;
		float SR_SPLT_CL;
		float SK_SPLT_CL;
		float SS_SPLT_CL;

		/* iteration */
		int32 V_IT;
		int32 P_IT;
		int32 D_IT;
		int32 C_IT;

		/* material */
		float LST;
		float AST;
		float VST;

		std::vector<AnchorRigidBody> AnchorRigidBodies;
		std::vector<int32> PinVertexIndices;
	};

	struct ZPMXPhysicsDataSet
	{
		std::vector<ZPMXRigidBody> RigidBodyTbl;
		std::vector<ZPMXJoint> JointTbl;
		std::vector<ZPMXSoftBody> SoftBodyTbl;
	};

	struct ZPMX
	{
		ZPMXHeader Header;
		ZPMXInfo Info;
		
		std::vector<ZPMXVertex> Vertices;
		std::vector<ZPMXFace> Faces;
		std::vector<ZPMXTexture> Textures;
		std::vector<ZPMXMaterial> Materials;
		std::vector<ZPMXBone> Bones;
		std::vector<ZPMXMorph> Morphs;
		std::vector<ZPMXDisplayFrame> DisplayFrames;
		ZPMXPhysicsDataSet PhysicsDataSet;
	};

	bool ReadPMXFile(ZPMX* pmx, const char* fileName);

}

#endif