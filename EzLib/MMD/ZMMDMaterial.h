#ifndef __ZMMD_MATERIAL_H__
#define __ZMMD_MATERIAL_H__

#include <string>
#include <cstdint>
#include "SystemDefines.h"
#include "Math/ZMath.h"

namespace EzLib
{
	struct ZMMDMaterial
	{
	public:
		enum class SPHERE_TEX_MODE
		{
			NONE,
			MUL,
			ADD
		};

	public:
		ZMMDMaterial();

		ZVec3 Diffuse;
		float Alpha;
		ZVec3 Specular;
		float SpePow;
		ZVec3 Ambient;
		uint8 EdgeFlag;
		float EdgeSize;
		ZVec4 EdgeColor;
		std::string Texture;
		std::string SpTexture;
		SPHERE_TEX_MODE SpTextureMode;
		std::string ToonTexture;
		ZVec4 TextureMulFactor;
		ZVec4 SpTextureMulFactor;
		ZVec4 ToonTextureMulFactor;
		ZVec4 TextureAddFactor;
		ZVec4 SpTextureAddFactor;
		ZVec4 ToonTextureAddFactor;
		bool BothFace;
		bool GroundShadow;
		bool ShadowCaster;
		bool ShadowReceiver;
	};


}
#endif